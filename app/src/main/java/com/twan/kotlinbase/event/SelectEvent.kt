package com.twan.kotlinbase.event

import com.twan.kotlinbase.bean.Mycoupon

//EventBus events
data class TakePhotoEvent(var path:String="")
data class TichengEvent(var rate:String="无")
data class HotRefresh(var param:String="")
data class RefreshService(var param: String="")
data class SelectCouponEvent(var param: Mycoupon)