package com.twan.kotlinbase.factory;

import android.content.Context;

import rx.Subscriber;

/**
 * Created by paopao on 2018/2/5.
 */

public class BaseSubscriber<T> extends Subscriber<T> {

    protected Context context_;
    public BaseSubscriber() {
    }


    public BaseSubscriber(Context context) {
        this.context_ = context;
    }

    @Override
    public void onCompleted() {

    }


    @Override
    public void onError(final Throwable e) {
        ApiErrorHelper.handleCommonError(context_, e);
//        if (e instanceof HttpException) {
//            // We had non-2XX http error
////            Toast.makeText(mContext, mContext.getString(R.string.server_internal_error), Toast.LENGTH_SHORT).show();
//        } else if (e instanceof IOException) {
//            // A network or conversion error happened
////            Toast.makeText(mContext, mContext.getString(R.string.cannot_connected_server), Toast.LENGTH_SHORT).show();
//        } else if (e instanceof ApiException) {
//            ApiException exception = (ApiException) e;
//
//                Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
//
//        }
    }

    @Override
    public void onNext(T t) {

    }



}
