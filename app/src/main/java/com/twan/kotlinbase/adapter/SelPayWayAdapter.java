package com.twan.kotlinbase.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.bean.SelPayWayBean;

import org.jetbrains.annotations.NotNull;

/**
 * @anthor zhoujr
 * @time 2021/4/15 15:16
 * @describe
 */
public class SelPayWayAdapter extends BaseQuickAdapter<SelPayWayBean, BaseViewHolder> {
    public SelPayWayAdapter() {
        super(R.layout.view_selpayway);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder helper, SelPayWayBean item) {
        helper.setImageResource(R.id.item_Img,item.getRes());
        helper.setText(R.id.item_title,item.getTitle());
        if(item.isSel()){
            helper.setImageResource(R.id.item_isSel,R.mipmap.icon_sel);
        }else{
            helper.setImageResource(R.id.item_isSel,R.mipmap.icon_unsel);
        }
    }
}
