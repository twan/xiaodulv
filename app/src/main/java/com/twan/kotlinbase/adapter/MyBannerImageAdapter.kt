package com.twan.kotlinbase.adapter

import android.content.Context
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.twan.kotlinbase.bean.BannerBean
import com.twan.kotlinbase.util.GlideUtils
import com.youth.banner.adapter.BannerAdapter
import com.youth.banner.util.BannerUtils

class MyBannerImageAdapter(var mContext:Context, data: List<BannerBean>) : BannerAdapter<BannerBean, MyBannerImageAdapter.ImageHolder>(data) {


    override fun onCreateHolder(parent: ViewGroup?, viewType: Int): ImageHolder {
        val imageView = ImageView(parent!!.context)
        val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        imageView.layoutParams = params
        imageView.scaleType = ImageView.ScaleType.CENTER_CROP
        return ImageHolder(imageView)
    }

    override fun onBindView(holder: ImageHolder, data: BannerBean, position: Int, size: Int) {
        GlideUtils.load(mContext,holder.imageView,data.imageUrl)
    }


    class ImageHolder(view: View) : RecyclerView.ViewHolder(view) {
        var imageView: ImageView = view as ImageView
    }

}

