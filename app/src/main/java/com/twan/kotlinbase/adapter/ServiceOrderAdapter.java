package com.twan.kotlinbase.adapter;

import android.graphics.Color;
import android.widget.ImageView;

import com.blankj.utilcode.util.SpanUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.app.EnumOrderStatus;
import com.twan.kotlinbase.app.EnumServiceOrderStatus;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.widgets.RoundImageView;
import com.twan.kotlinbase.widgets.RoundImageView5;

import org.jetbrains.annotations.NotNull;

/**
 * @anthor zhoujr
 * @time 2021/4/17 14:12
 * @describe
 */
public class ServiceOrderAdapter extends BaseQuickAdapter<OrderBean.ResultBean.ContentBean, BaseViewHolder> {
    ItemClick itemClick;
    public ServiceOrderAdapter() {
        super(R.layout.item_serviceorder);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        RoundImageView iv_head = helper.getView(R.id.iv_head);
        RoundImageView5 iv_avater = helper.getView(R.id.iv_avater);
        Glide.with(getContext()).load(item.getSellerAvatarUrl()).into(iv_avater);
        Glide.with(getContext()).load(item.getBuyerAvatarUrl()).into(iv_head);
        helper.setText(R.id.itemShopName,item.getBuyerNick())
                .setText(R.id.itemTitle,item.getTitle())
                .setText(R.id.itemRealPay,new SpanUtils()
                        .append("实付款¥")
                        .setForegroundColor(Color.parseColor("#F99419"))
                        .setFontSize(14,true)
                        .append(item.getTotalFee()+"")
                        .setForegroundColor(Color.parseColor("#F99419"))
                        .setFontSize(14,true)
                        .create())
                .setText(R.id.tv_amt,new SpanUtils()
                        .append("¥")
                        .setForegroundColor(Color.parseColor("#F99419"))
                        .setFontSize(12,true)
                        .append(item.getTotalFee()+"")
                        .setForegroundColor(Color.parseColor("#F99419"))
                        .setFontSize(15,true)
                        .create());

        String type = item.getServiceTypeValue();
        if(type.equals("actor")){
            //演员
            helper.setGone(R.id.itemDesc,true)
                    .setGone(R.id.itemCount,true);
        }else if(type.equals("model")){
            //模特
            helper.setGone(R.id.itemDesc,true)
                    .setGone(R.id.itemCount,true);
        }else if(type.equals("shopLive")){
            //店铺直播
            helper.setGone(R.id.itemDesc,false)
                    .setGone(R.id.itemCount,false);
            helper.setText(R.id.itemDesc,item.getSpecificationLive().getTimeLengthLabel()+item.getSpecificationLive().getTimeLengthValue())
                    .setText(R.id.itemCount,item.getSpecificationLive().getBuyNumber()+"");
        }else if(type.equals("shortVideoMake")){
            //短视频制作
            helper.setGone(R.id.itemDesc,false)
                    .setGone(R.id.itemCount,false);
            helper.setText(R.id.itemDesc,item.getSpecificationVideo().getServiceSku().getAttrSymbolLabel()+item.getSpecificationVideo().getServiceSku().getAttrSymbolPath())
                    .setText(R.id.itemCount,item.getSpecificationVideo().getBuyNumber()+"");
        }else if(type.equals("shortVideoPromote")){
            //短视频推广
            helper.setGone(R.id.itemDesc,false)
                    .setGone(R.id.itemCount,false);
            helper.setText(R.id.itemDesc,item.getSpecificationPopularize().getLatestCompletionDay()+"")
                    .setText(R.id.itemCount,item.getSpecificationPopularize().getBuyNumber()+"");
        }

        helper.setGone(R.id.itemConfimReceive,true);
        helper.getView(R.id.itemConfimReceive).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.confimReceived(helper.getAdapterPosition());
            }
        });
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.clickDetail(helper.getAdapterPosition());
            }
        });

        String status = "";
        if(item.getStatus() == Integer.parseInt(EnumOrderStatus.PENDING_PAYMENT.getId())){
            //待付款
            status = "待付款";
        }else if(item.getStatus() == Integer.parseInt(EnumOrderStatus.TO_BE_SHIPPED.getId())){
            //待发货
            status = "待发货";
        }else if(item.getStatus() == Integer.parseInt(EnumOrderStatus.TO_BE_EVALUATED.getId())){
            //待评价
            status = "待评价";
        }else if(item.getStatus() == Integer.parseInt(EnumOrderStatus.COMPLETED.getId())){
            //已完成
            status = "已完成";
        }else if(item.getStatus() == Integer.parseInt(EnumOrderStatus.CANCELED.getId())){
            //已取消
            status = "已取消";
        }else if(item.getStatus() == Integer.parseInt(EnumOrderStatus.TO_BE_RECEIVED.getId())){
            //待收货
            status = "待收货";
            helper.setGone(R.id.itemConfimReceive,false);
        }else if(item.getStatus() == Integer.parseInt(EnumOrderStatus.IN_THE_PROCESS.getId())){
            //进行中
            status = "服务中";
        }else if(item.getStatus() == Integer.parseInt(EnumOrderStatus.REFUNDED.getId())){
            //已退款
            status = "已退款";
        }else if(item.getStatus() == Integer.parseInt(EnumOrderStatus.TIMEOUT.getId())){
            //订单超时
            status = "订单超时";
        }
        helper.setText(R.id.item_State,status);
    }



    public interface ItemClick{
        void clickDetail(int position);
        void confimReceived(int position);
    }


    public void setItemClick(ItemClick itemClick){
        this.itemClick = itemClick;
    }
}
