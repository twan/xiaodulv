package com.twan.kotlinbase.adapter;

import android.graphics.Color;
import android.widget.Button;

import com.blankj.utilcode.util.SpanUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseDelegateMultiAdapter;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.delegate.BaseMultiTypeDelegate;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.app.ConstantsKt;
import com.twan.kotlinbase.app.EnumOrderStatus;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.widgets.RoundImageView;
import com.twan.kotlinbase.widgets.RoundImageView5;

import org.jetbrains.annotations.NotNull;

import java.util.List;


/**
 * @anthor zhoujr
 * @time 2021/4/13 15:04
 * @describe
 */
public class NewOrderAdapter extends BaseDelegateMultiAdapter<OrderBean.ResultBean.ContentBean, BaseViewHolder> {
    ItemClick itemClick;
    RequestOptions options;
    public NewOrderAdapter() {
        super();
        options = new RequestOptions();
        options.error(R.mipmap.test);
        options.placeholder(R.mipmap.test);

        setMultiTypeDelegate(new BaseMultiTypeDelegate<OrderBean.ResultBean.ContentBean>() {
            @Override
            public int getItemType(@NotNull List<? extends OrderBean.ResultBean.ContentBean> data, int position)  {
                return data.get(position).getStatus();
            }
        });

        getMultiTypeDelegate()
                .addItemType(Integer.parseInt(EnumOrderStatus.PENDING_PAYMENT.getId()),R.layout.item_orderwaitpay)
                .addItemType(Integer.parseInt(EnumOrderStatus.TO_BE_SHIPPED.getId()),R.layout.item_orderwaitsend)
                .addItemType(Integer.parseInt(EnumOrderStatus.TO_BE_EVALUATED.getId()),R.layout.item_orderwaitevaluate)
                .addItemType(Integer.parseInt(EnumOrderStatus.COMPLETED.getId()),R.layout.item_orderfinish)
                .addItemType(Integer.parseInt(EnumOrderStatus.CANCELED.getId()),R.layout.item_ordercancel)
                .addItemType(Integer.parseInt(EnumOrderStatus.TO_BE_RECEIVED.getId()),R.layout.item_orderreceive)
                .addItemType(Integer.parseInt(EnumOrderStatus.IN_THE_PROCESS.getId()),R.layout.item_orderfinish)
                .addItemType(Integer.parseInt(EnumOrderStatus.REFUNDED.getId()),R.layout.item_orderfinish)
                .addItemType(Integer.parseInt(EnumOrderStatus.TIMEOUT.getId()),R.layout.item_orderfinish);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.PENDING_PAYMENT.getId())){
            //待付款
            setWaitpayData(helper,item);
        }else if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.TO_BE_SHIPPED.getId())){
            //待发货
            setWaitSendData(helper,item);
        }else if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.TO_BE_EVALUATED.getId())){
            //待评价
            setEvaluateData(helper,item);
        }else if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.COMPLETED.getId())){
            //已完成
            setFinishData(helper,item);
        }else if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.CANCELED.getId())){
            //已取消
            setCancelData(helper,item);
        }else if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.TO_BE_RECEIVED.getId())){
            //待收货
            setReceiveData(helper,item);
        }else if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.IN_THE_PROCESS.getId())){
            //进行中
            setInProcess(helper,item);
        }else if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.REFUNDED.getId())){
            //已退款
            setRefundedData(helper,item);
        }else if(helper.getItemViewType() == Integer.parseInt(EnumOrderStatus.TIMEOUT.getId())){
            //订单超时
            setTimeOutData(helper,item);
        }
    }

    private void setEvaluateData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.getView(R.id.itemEvaluate).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.evaluateOrder(helper.getAdapterPosition());
            }
        });
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void setTimeOutData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.setText(R.id.item_State,"订单超时");
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void setRefundedData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.setText(R.id.item_State,"退款");
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void setInProcess(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.setText(R.id.item_State,"进行中");
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void setReceiveData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        helper.getView(R.id.itemConfimReceive).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.receiveOrder(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void setCancelData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.getView(R.id.itemDelete).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.deleteOrder(helper.getAdapterPosition());
            }
        });
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void setFinishData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.setText(R.id.item_State,"已完成");
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void setWaitSendData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        helper.getView(R.id.itemSend).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.sendOrder(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void setWaitpayData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        helper.getView(R.id.itemLl).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.goDetail(helper.getAdapterPosition());
            }
        });
        helper.getView(R.id.btn_cancel).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.cancelOrder(helper.getAdapterPosition());
            }
        });
        helper.getView(R.id.btn_pay).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.payOrder(helper.getAdapterPosition());
            }
        });
        addDefalutData(helper,item);
    }

    private void addDefalutData(BaseViewHolder helper, OrderBean.ResultBean.ContentBean item) {
        RoundImageView iv_head = helper.getView(R.id.iv_head);
        RoundImageView5 iv_avater = helper.getView(R.id.iv_avater);
        Glide.with(getContext()).load(item.getPicUrl()).apply(options).into(iv_avater);
        Glide.with(getContext()).load(item.getSellerAvatarUrl()).apply(options).into(iv_head);
        String type = item.getServiceTypeValue();
        if(type.equals("actor")){
            //演员
            helper.setGone(R.id.itemDesc,true)
                    .setGone(R.id.itemCount,true);
        }else if(type.equals("model")){
            //模特
            helper.setGone(R.id.itemDesc,true)
                    .setGone(R.id.itemCount,true);
        }else if(type.equals("shopLive")){
            //店铺直播
            helper.setGone(R.id.itemDesc,false)
                    .setGone(R.id.itemCount,false);
            helper.setText(R.id.itemDesc,item.getSpecificationLive().getTimeLengthLabel()+item.getSpecificationLive().getTimeLengthValue())
                    .setText(R.id.itemCount,item.getSpecificationLive().getBuyNumber()+"");
        }else if(type.equals("shortVideoMake")){
            //短视频制作
            helper.setGone(R.id.itemDesc,false)
                    .setGone(R.id.itemCount,false);
            helper.setText(R.id.itemDesc,item.getSpecificationVideo().getServiceSku().getAttrSymbolLabel()+item.getSpecificationVideo().getServiceSku().getAttrSymbolPath())
                    .setText(R.id.itemCount,item.getSpecificationVideo().getBuyNumber()+"");
        }else if(type.equals("shortVideoPromote")){
            //短视频推广
            helper.setGone(R.id.itemDesc,false)
                    .setGone(R.id.itemCount,false);
            helper.setText(R.id.itemDesc,item.getSpecificationPopularize().getLatestCompletionDay()+"")
                    .setText(R.id.itemCount,item.getSpecificationPopularize().getBuyNumber()+"");
        }

        helper.setText(R.id.itemTitle,item.getTitle())
                .setText(R.id.itemShopName,item.getSellerNick())
                .setText(R.id.tv_amt,new SpanUtils()
                        .append("¥")
                        .setForegroundColor(Color.parseColor("#F99419"))
                        .setFontSize(12,true)
                        .append(item.getTotalFee()+"")
                        .setForegroundColor(Color.parseColor("#F99419"))
                        .setFontSize(15,true)
                        .create());
    }




    public interface ItemClick{
        void cancelOrder(int position);
        void payOrder(int position);
        void sendOrder(int position);
        void evaluateOrder(int position);
        void deleteOrder(int position);
        void goDetail(int position);
        void receiveOrder(int position);
    }

    public void setItemClick(ItemClick itemClick){
        this.itemClick = itemClick;
    }
}
