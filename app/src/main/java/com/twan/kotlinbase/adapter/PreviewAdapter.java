package com.twan.kotlinbase.adapter;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.next.easynavigation.adapter.ViewPagerAdapter;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.widgets.CustomProgress;

import java.util.ArrayList;

/**
 * @anthor zhoujr
 * @time 2021/4/17 9:58
 * @describe
 */
public class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.ViewPagerViewHolder> {
    ItemClick itemClick;
    ArrayList<Integer> url;
    public PreviewAdapter(ArrayList<Integer> url){
        this.url = url;
    }

    @NonNull
    @Override
    public ViewPagerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewPagerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_preview, parent,false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewPagerViewHolder holder, int position) {
//        holder.mTvTitle.setText("item " + position);
        holder.item_img.setBackgroundResource(url.get(position));
        holder.item_img.setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.saveImg(position);
            }
        });
    }

    public interface  ItemClick{
        void saveImg(int position);
    }

    public void setItemClick(ItemClick itemClick){
        this.itemClick = itemClick;
    }

    @Override
    public int getItemCount() {
        return url.size();
    }

    class ViewPagerViewHolder extends RecyclerView.ViewHolder {
        ImageView item_img;
        public ViewPagerViewHolder(@NonNull View itemView) {
            super(itemView);
            item_img = itemView.findViewById(R.id.item_img);
        }
    }
}
