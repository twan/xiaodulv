package com.twan.kotlinbase.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.twan.kotlinbase.fragment.NewOrderFragment;
import com.twan.kotlinbase.fragment.ServiceOrderFragment;

import java.util.ArrayList;

/**
 * @anthor zhoujr
 * @time 2021/4/13 10:16
 * @describe
 */
public class ServiceOrderFMAdapter extends FragmentPagerAdapter {
    ArrayList<ServiceOrderFragment> fragments;

    public ServiceOrderFMAdapter(@NonNull FragmentManager fm, ArrayList<ServiceOrderFragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
