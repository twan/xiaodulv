package com.twan.kotlinbase.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.twan.kotlinbase.R;

import org.jetbrains.annotations.NotNull;

/**
 * @anthor zhoujr
 * @time 2021/4/17 9:24
 * @describe
 */
public class AuditedAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public AuditedAdapter() {
        super(R.layout.item_audited);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, String s) {

    }
}
