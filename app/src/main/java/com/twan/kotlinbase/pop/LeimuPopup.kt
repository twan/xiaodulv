package com.twan.kotlinbase.pop

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lxj.xpopup.impl.PartShadowPopupView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.bean.SpiBean
import com.twan.kotlinbase.util.*
import kotlinx.android.synthetic.main.pop_partshdow_city_select.view.*
import kotlinx.android.synthetic.main.pop_select.view.*

//类目选择
class LeimuPopup(var tagView:View, context: Context, var onselect: ((pos:Int, SpiBean)->Unit)?=null) : PartShadowPopupView(context) {
    var mContext = context
    override fun getImplLayoutId(): Int {
        return R.layout.pop_select
    }

    override fun onCreate() {
        super.onCreate()
        generData()
        if (datas.size>5) ViewUtils.setLLheight(ll_container)
        rv_list.adapter= object : BaseQuickAdapter<SpiBean, BaseViewHolder>(R.layout.item_my_spi,datas) {
            override fun convert(holder: BaseViewHolder, model: SpiBean) {
                var iv_check=holder.getView<ImageView>(R.id.iv_check)
                var tv_desc=holder.getView<TextView>(R.id.tv_desc)
                holder!!.setText(R.id.tv_desc, model.toString())
                if (tagView.tag !=null && tagView.tag == holder.position){
                    iv_check.visibility=View.VISIBLE
                    tv_desc.setTextColor(mContext.resources.getColor(R.color.text_orange))
                } else {
                    iv_check.visibility=View.GONE
                }
            }
        }.also {
            it.setOnItemClickListener { adapter, view, position ->
                tagView.tag = position
                onselect?.run {
                    (onselect!!)(position,datas[position])
                }
                this@LeimuPopup.dismiss()
            }
        }
    }

    var datas = mutableListOf<SpiBean>()
    fun generData(){
        datas.add(SpiBean("服装鞋包"))
        datas.add(SpiBean("服饰配件"))
        datas.add(SpiBean("流行男鞋"))
        datas.add(SpiBean("男装"))
        datas.add(SpiBean("女士内衣"))
        datas.add(SpiBean("女鞋"))
        datas.add(SpiBean("女装"))
        datas.add(SpiBean("箱包皮具"))
        datas.add(SpiBean("手机数码"))
        datas.add(SpiBean("美妆饰品"))
        datas.add(SpiBean("母婴用品"))
        datas.add(SpiBean("家居建材"))
        datas.add(SpiBean("床上用品"))
        datas.add(SpiBean("百货食品"))
        datas.add(SpiBean("运动户外"))
    }

}