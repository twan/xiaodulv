package com.twan.kotlinbase.pop;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.util.SpanUtils;
import com.twan.kotlinbase.R;

import razerdp.basepopup.BasePopupWindow;

/**
 * @anthor zhoujr
 * @time 2021/4/15 16:28
 * @describe
 */
public class ConfimSimplePop extends BasePopupWindow {
    TextView popServiceName,popServiceNo;
    ItemClick itemClick;


    public ConfimSimplePop(Context context) {
        super(context);
        setPopupGravity(Gravity.CENTER);
        bindView();
    }

    private void bindView() {
        popServiceName = findViewById(R.id.popServiceName);
        popServiceNo = findViewById(R.id.popServiceNo);
        popServiceName.setText(new SpanUtils()
                .append("快递公司：")
                .setFontSize(13,true)
                .setForegroundColor(Color.parseColor("#999999"))
                .append("申通快递")
                .setFontSize(13,true)
                .setForegroundColor(Color.parseColor("#333333"))
                .create()
        );
        popServiceNo.setText(new SpanUtils()
                .append("快递单号：")
                .setFontSize(13,true)
                .setForegroundColor(Color.parseColor("#999999"))
                .append("123 321 123 321 123")
                .setFontSize(13,true)
                .setForegroundColor(Color.parseColor("#333333"))
                .create()
        );
        findViewById(R.id.popServiceReBack).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.reWrite();
                dismiss();
            }
        });
        findViewById(R.id.popServiceConfim).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.confimSubmit();
                dismiss();
            }
        });
    }

    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_confimsimple);
    }


    public interface ItemClick{
        void reWrite();
        void confimSubmit();
    }

    public void setItemClick(ItemClick itemClick){
        this.itemClick = itemClick;
    }
}
