package com.twan.kotlinbase.pop

import android.content.Context
import butterknife.ButterKnife
import butterknife.OnClick
import com.lxj.xpopup.core.CenterPopupView
import com.twan.kotlinbase.R
import kotlinx.android.synthetic.main.pop_my_center_demo.view.*

/**
 * 调用方式: XPopup.Builder(this).asCustom(MyCenterPopup(this)).show()
 */
class MyCenterPopup(context: Context,var title:String,var success:()->Unit) : CenterPopupView(context) {
    private val content: String? = null

    override fun getImplLayoutId(): Int {
        return R.layout.pop_my_center_demo
    }

    override fun onCreate() {
        super.onCreate()
        ButterKnife.bind(this)
        tv_title.text=title
    }

    @OnClick(R.id.btn_agree)
    fun agress(){
        success()
        this.dismiss()
    }
    @OnClick(R.id.btn_cancle)
    fun canl(){
        this.dismiss()
    }
}