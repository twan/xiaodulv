package com.twan.kotlinbase.pop

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lxj.xpopup.impl.PartShadowPopupView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.util.*
import kotlinx.android.synthetic.main.pop_partshdow_city_select.view.*
import kotlinx.android.synthetic.main.pop_select.view.*

//才艺达人
class SelectDictPopup(var tagView:View, context: Context,dictKey:String, var onselect: ((pos:Int, Dict)->Unit)?=null) : PartShadowPopupView(context) {
    var mContext = context
    override fun getImplLayoutId(): Int {
        return R.layout.pop_select
    }

    override fun onCreate() {
        super.onCreate()
        if (datas.size>5) ViewUtils.setLLheight(ll_container)
        rv_list.adapter= object : BaseQuickAdapter<Dict, BaseViewHolder>(R.layout.item_my_spi,datas) {
            override fun convert(holder: BaseViewHolder, model: Dict) {
                var iv_check=holder.getView<ImageView>(R.id.iv_check)
                var tv_desc=holder.getView<TextView>(R.id.tv_desc)
                holder!!.setText(R.id.tv_desc, model.toString())
                if (tagView.tag !=null && tagView.tag == holder.position){
                    iv_check.visibility=View.VISIBLE
                    tv_desc.setTextColor(mContext.resources.getColor(R.color.text_orange))
                } else {
                    iv_check.visibility=View.GONE
                }
            }
        }.also {
            it.setOnItemClickListener { adapter, view, position ->
                tagView.tag = position
                onselect?.run {
                    (onselect!!)(position,datas[position])
                }
                this@SelectDictPopup.dismiss()
            }
        }
    }

    var datas = DictUtils.getDictCache(dictKey)!!.toMutableList()
}