package com.twan.kotlinbase.pop;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.twan.kotlinbase.R;

import razerdp.basepopup.BasePopupWindow;

/**
 * @anthor zhoujr
 * @time 2021/4/15 15:52
 * @describe
 */
public class PressSimplePop extends BasePopupWindow {
    TextView presssimpleConpanyName;
    ImageView presssimpleImg;
    EditText presssimpleNoEt;
    ItemClick itemClick;


    public PressSimplePop(Context context) {
        super(context);
        setPopupGravity(Gravity.BOTTOM);
        setAdjustInputMethod(true);
        bindView();
    }

    private void bindView() {
        presssimpleConpanyName = findViewById(R.id.presssimpleConpanyName);
        presssimpleImg = findViewById(R.id.presssimpleImg);
        presssimpleNoEt = findViewById(R.id.presssimpleNoEt);
        findViewById(R.id.popClose).setOnClickListener(view -> {
            dismiss();
        });
        findViewById(R.id.presssimpleBtn).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.confimSubmit();
                dismiss();
            }
        });
        findViewById(R.id.presssimpleRe).setOnClickListener(view -> {
            if(itemClick != null){
                itemClick.selServiceCompany();
            }
        });
    }

    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_presssimple);
    }


    public interface ItemClick{
        void selServiceCompany();
        void confimSubmit();
    }


    public void setItemClick(ItemClick itemClick){
        this.itemClick = itemClick;
    }



}
