package com.twan.kotlinbase.pop

import android.app.Activity
import android.widget.LinearLayout
import com.blankj.utilcode.util.SpanUtils
import com.just.agentweb.AgentWeb
import com.just.agentweb.DefaultWebClient
import com.lxj.xpopup.core.CenterPopupView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.App
import com.twan.kotlinbase.ui.WebActivity
import com.twan.kotlinbase.util.WindowWH
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_web.*
import kotlinx.android.synthetic.main.pop_statement.view.*

class StatementPopup(context: Activity, onsuccess:()->Unit) : CenterPopupView(context) {
    private val title: String? = null
    private val mContext: Activity = context
    private var callback = onsuccess

    override fun getImplLayoutId(): Int {
        return R.layout.pop_statement
    }

    override fun onCreate() {
        super.onCreate()
        SpanUtils.with(tv_xieyi).append("查看完整版")
            .append("《服务协议》").setClickSpan(resources.getColor(R.color.text_orange),false){
                Router.newIntent(mContext)
                    .putString("url","https://api.xiaodulv6.com/protocol/p1")
                    .putString("title","服务协议")
                    .to(WebActivity::class.java).launch()
            }
            .append("及")
            .append("《隐私政策》").setClickSpan(resources.getColor(R.color.text_orange),false){
                Router.newIntent(mContext)
                    .putString("url","https://api.xiaodulv6.com/protocol/p2")
                    .putString("title","隐私政策")
                    .to(WebActivity::class.java).launch()
            }.create()
        btn_cancle.setOnClickListener{
            this.dismiss()
            App.exitApp()
            mContext.moveTaskToBack(true)
        }
        btn_agree.setOnClickListener{
            callback()
            this.dismiss()
        }
    }
}