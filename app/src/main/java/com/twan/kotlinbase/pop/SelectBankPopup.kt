package com.twan.kotlinbase.pop

import android.app.Activity
import butterknife.ButterKnife
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.core.BottomPopupView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.bean.BankItem
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.AddzfbActivity
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_account_manage.*
import kotlinx.android.synthetic.main.pop_select_bank.view.*
import org.greenrobot.eventbus.EventBus
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class SelectBankPopup(context: Activity, var onselect:(item: BankItem)->Unit) : BottomPopupView(context) {
    private var mContext = context
    override fun getImplLayoutId(): Int {
        return R.layout.pop_select_bank
    }

    override fun onCreate() {
        super.onCreate()
        ButterKnife.bind(this)
        btn_add.setOnClickListener {
            this.dismiss()
            Router.newIntent(mContext).to(AddzfbActivity::class.java).launch()
        }
        initRv()

    }

    fun initRv(){
        rv_care.adapter=object :BaseQuickAdapter<BankItem,BaseViewHolder>(R.layout.item_select_bank){
            override fun convert(holder: BaseViewHolder, item: BankItem) {
                holder.setText(R.id.tv_mobile,if (item.accountType =="1") "支付宝账户(${item.cardNumber})" else "银行卡账户(${item.cardNumber})")
            }

        }.also {
            getCare(it)
            it.setOnItemClickListener { adapter, view, position ->
                onselect(it.data[position])
                this@SelectBankPopup.dismiss()
            }
        }
    }

    fun getCare(adapter: BaseQuickAdapter<BankItem,BaseViewHolder>){
        RxHttpScope().launch {
            var req=RxHttp.get("bankCard/findByAccountId").add("userId",MyUtils.getUserid()).toResponse<List<BankItem>>().await()
            adapter.setList(req)
        }
    }
}