package com.twan.kotlinbase.pop

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.blankj.utilcode.util.FileUtils
import com.blankj.utilcode.util.LogUtils
import com.lxj.xpopup.core.BottomPopupView
import com.twan.kotlinbase.BuildConfig
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.bean.ServiceBean
import com.twan.kotlinbase.bean.ServiceDetail
import com.twan.kotlinbase.bean.ShopLiveFormat
import com.twan.kotlinbase.ui.CalendarActivity
import com.twan.kotlinbase.util.DictUtils
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.RxPermissionsUtil
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlinx.android.synthetic.main.pop_my_bottom_demo.view.*
import kotlinx.android.synthetic.main.pop_my_bottom_guige.view.*
import org.greenrobot.eventbus.EventBus

/**
 * 调用方式: XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
 */
class GuigeShopLivePopup(context: Activity,var need:Need, var serviceBean:ServiceBean, var serviceDetail: ServiceDetail) : BottomPopupView(context) {
    private var mContext = context
    lateinit var mShichang:Dict
    override fun getImplLayoutId(): Int {
        return R.layout.pop_my_bottom_guige
    }

    override fun onCreate() {
        super.onCreate()
        ButterKnife.bind(this)
        ll_shop_live.visibility= View.VISIBLE
        tv_amt.text="¥${serviceDetail.baseItemDTO.price}"
        tv_name.text="¥${serviceDetail.baseItemDTO.title}"
        GlideUtils.load(mContext,iv_avater,serviceDetail.baseItemDTO.imageUrl)
        initRadioButton(rg_shichang)
    }

    fun initRadioButton(radioGroup: RadioGroup){
        var times =DictUtils.getDictCache(DictUtils.shop_live_time_length)
        radioGroup.weightSum=times!!.size.toFloat()
        times.forEachIndexed { index, dict ->
            var rbView = mContext.layoutInflater.inflate(R.layout.radio_button,radioGroup,false) as RadioButton
            rbView.text=dict.label
            rbView.id=dict.id
            rbView.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    mShichang=dict
                    updateTotalPrice()
                }
            }
            if (index==0) rbView.isChecked=true
            radioGroup.addView(rbView)
        }
    }

    fun updateTotalPrice(){
        var num =edt_num.text.toString().toInt()
        var totalPrice=serviceDetail.baseItemDTO.price.toFloat().times(mShichang.value.toInt()).times(num)
        tv_amt.text="¥${totalPrice}"
        serviceBean.totalAmount=totalPrice.toString()
    }

    @OnClick(R.id.iv_close)
    fun close(){
        this.dismiss()
    }

    @OnClick(R.id.tv_minus)
    fun minus(){
        if (edt_num.text.toString() =="1") return
        edt_num.text = "${edt_num.text.toString().toInt()-1}"
        updateTotalPrice()
    }

    @OnClick(R.id.tv_add)
    fun add(){
        edt_num.text = "${edt_num.text.toString().toInt()+1}"
        updateTotalPrice()
    }

    @OnClick(R.id.btn_ok)
    fun confirm(){
        updateTotalPrice()
        var shopLiveFormat= ShopLiveFormat(edt_num.text.toString(),timeLengthLabel=mShichang.label,timeLengthValue = mShichang.id)
        Router.newIntent(mContext)
                .putSerializable("need",need)
                .putSerializable("shopLiveFormat",shopLiveFormat)
                .putSerializable("serviceBean",serviceBean)
                .to(CalendarActivity::class.java).launch()
        this.dismiss()
    }


}