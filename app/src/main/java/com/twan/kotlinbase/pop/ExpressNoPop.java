package com.twan.kotlinbase.pop;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.lxj.xpopup.core.BasePopupView;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.util.NotEmpty;

import razerdp.basepopup.BasePopup;
import razerdp.basepopup.BasePopupWindow;

/**
 * @anthor zhoujr
 * @time 2021/4/14 13:59
 * @describe
 */
public class ExpressNoPop extends BasePopupWindow {
    ConfimPress confimPress;
    public ExpressNoPop(@NonNull Context context) {
        super(context);
        setPopupGravity(Gravity.CENTER);
        bindView();
    }

    private void bindView() {
        EditText popExpressNo = findViewById(R.id.popExpressNo);
        Button popBtn = findViewById(R.id.popBtn);
        popBtn.setOnClickListener(view -> {
            if(NotEmpty.isempty(popExpressNo.getText().toString().trim(),"请输入单号"))
                return;
            if(confimPress !=null){
                confimPress.confim(popExpressNo.getText().toString().trim());
                dismiss();
            }
        });
    }

    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_expressno);
    }

    public interface  ConfimPress{
        void confim(String expressno);
    }

    public void setConfimPress(ConfimPress confimPress){
        this.confimPress = confimPress;
    }
}
