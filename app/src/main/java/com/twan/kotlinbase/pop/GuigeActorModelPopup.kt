package com.twan.kotlinbase.pop

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.blankj.utilcode.util.FileUtils
import com.blankj.utilcode.util.LogUtils
import com.lxj.xpopup.core.BottomPopupView
import com.twan.kotlinbase.BuildConfig
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.bean.ServiceBean
import com.twan.kotlinbase.bean.ServiceDetail
import com.twan.kotlinbase.ui.CalendarActivity
import com.twan.kotlinbase.ui.CommitOrderActivity
import com.twan.kotlinbase.util.DictUtils
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.RxPermissionsUtil
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlinx.android.synthetic.main.pop_my_bottom_demo.view.*
import kotlinx.android.synthetic.main.pop_my_bottom_guige.view.*
import org.greenrobot.eventbus.EventBus

/**
 * 调用方式: XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
 *
 * 模特演员 数量
 */
class GuigeActorModelPopup(context: Activity, var need: Need, var serviceBean: ServiceBean, var serviceDetail: ServiceDetail) : BottomPopupView(context) {
    private var mContext = context
    override fun getImplLayoutId(): Int {
        return R.layout.pop_my_bottom_guige
    }

    override fun onCreate() {
        super.onCreate()
        ButterKnife.bind(this)
        tv_amt.text="¥${serviceDetail.baseItemDTO.price}"
        tv_name.text="¥${serviceDetail.baseItemDTO.title}"
        GlideUtils.load(mContext,iv_avater,serviceDetail.baseItemDTO.imageUrl)
    }

    fun updateTotalPrice(){
        var num =edt_num.text.toString().toInt()
        var totalPrice=serviceDetail.baseItemDTO.price.toInt().times(num)
        tv_amt.text="¥${totalPrice}"
        serviceBean.totalAmount=totalPrice.toString()
    }

    @OnClick(R.id.iv_close)
    fun close(){
        this.dismiss()
    }

    @OnClick(R.id.tv_minus)
    fun minus(){
        if (edt_num.text.toString() =="1") return
        edt_num.text = "${edt_num.text.toString().toInt()-1}"
        updateTotalPrice()
    }

    @OnClick(R.id.tv_add)
    fun add(){
        edt_num.text = "${edt_num.text.toString().toInt()+1}"
        updateTotalPrice()
    }

    @OnClick(R.id.btn_ok)
    fun confirm(){
        updateTotalPrice()
        Router.newIntent(mContext)
            .putSerializable("need",need)
            .putInt("buyNumber",edt_num.text.toString().toInt())
            .putSerializable("serviceBean",serviceBean)
            .to(CommitOrderActivity::class.java).launch()
        this.dismiss()
    }


}