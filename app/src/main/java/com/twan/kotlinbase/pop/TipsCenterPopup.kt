package com.twan.kotlinbase.pop

import android.content.Context
import butterknife.ButterKnife
import butterknife.OnClick
import com.lxj.xpopup.core.CenterPopupView
import com.twan.kotlinbase.R
import kotlinx.android.synthetic.main.pop_tips.view.*

/**
 * 调用方式: XPopup.Builder(this).asCustom(MyCenterPopup(this)).show()
 * 消息提示
 */
class TipsCenterPopup(context: Context, val tips:String, var success:()->Unit) : CenterPopupView(context) {
    private val content: Context = context

    override fun getImplLayoutId(): Int {
        return R.layout.pop_tips
    }

    override fun onCreate() {
        super.onCreate()
        ButterKnife.bind(this)
        tv_tips.text = tips
    }

    @OnClick(R.id.btn_cancel)
    fun cancle1(){
        this.dismiss()
    }

    @OnClick(R.id.btn_confirm)
    fun agree(){
        success()
        this.dismiss()
    }
}