package com.twan.kotlinbase.pop

import android.content.Context
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.lxj.xpopup.core.DrawerPopupView
import com.twan.kotlinbase.R
import kotlinx.android.synthetic.main.pop_my_drawer_demo.view.*

/**
 * Description: 自定义抽屉弹窗
 * Create by dance, at 2018/12/20
 */
class MyDrawerPopupView(context: Context,var done:()->Unit) : DrawerPopupView(context) {
    var text: TextView? = null
    override fun getImplLayoutId(): Int {
        return R.layout.pop_my_drawer_demo
    }

    override fun onCreate() {
        super.onCreate()
        ButterKnife.bind(this)
    }

    @OnClick(R.id.tv_done)
    fun done(){
        done()
        this.dismiss()
    }

    @OnClick(R.id.tv_reset)
    fun reset(){
        edt_min.setText("")
        edt_max.setText("")
    }

    @OnClick(R.id.tv_500)
    fun click500(){
        edt_min.setText("0")
        edt_max.setText("500")
    }
    @OnClick(R.id.tv_1000)
    fun click1000(){
        edt_min.setText("500")
        edt_max.setText("1000")
    }
    @OnClick(R.id.tv_2000)
    fun click2000(){
        edt_min.setText("1000")
        edt_max.setText("2000")
    }

}