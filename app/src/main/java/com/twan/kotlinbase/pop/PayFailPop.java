package com.twan.kotlinbase.pop;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.twan.kotlinbase.R;

import razerdp.basepopup.BasePopupWindow;

/**
 * @anthor zhoujr
 * @time 2021/4/15 15:37
 * @describe
 */
public class PayFailPop extends BasePopupWindow {
    RePay rePay;


    public PayFailPop(Context context) {
        super(context);
        setPopupGravity(Gravity.CENTER);
        bindView();
    }

    private void bindView() {
        findViewById(R.id.popRePay).setOnClickListener(view -> {
            if(rePay != null){
                rePay.ConfimRePay();
                dismiss();
            }
        });
    }

    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_payfail);
    }

    public interface RePay{
        void ConfimRePay();
    }

    public void setRePay(RePay rePay){
        this.rePay = rePay;
    }
}
