package com.twan.kotlinbase.pop

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.blankj.utilcode.util.FileUtils
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.core.BottomPopupView
import com.twan.kotlinbase.BuildConfig
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.Role
import com.twan.kotlinbase.bean.SettleStatus
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.LoginActivity
import com.twan.kotlinbase.ui.SendActivity
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.RxPermissionsUtil
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlinx.android.synthetic.main.pop_my_bottom_demo.view.*
import org.greenrobot.eventbus.EventBus
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

/**
 * 调用方式: XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
 */
class PublishPopup(context: Activity) : BottomPopupView(context) {
    private var mContext = context
    override fun getImplLayoutId(): Int {
        return R.layout.pop_publish
    }

    override fun onCreate() {
        super.onCreate()
        ButterKnife.bind(this)


    }

    @OnClick(R.id.cancle)
    fun calcle(){
        this.dismiss()
    }

    @OnClick(R.id.ll_send_need)
    fun sendNeed(){
        if(MyUtils.getRole()==Role.SERVICE_PROVIDER) {
            ToastUtils.showShort("请切换到用户操作")
            return
        }
        if(!MyUtils.isLogin()){
            Router.newIntent(mContext).to(LoginActivity::class.java).launch()
            return
        }
        Router.newIntent(mContext).putInt("data",1).to(SendActivity::class.java).launch()
        this.dismiss()
    }
    @OnClick(R.id.ll_send_service)
    fun sendService(){
//        if(MyUtils.getRole()==Role.USER) {
//            ToastUtils.showShort("请切换到服务商操作")
//            return
//        }
        if(!MyUtils.isLogin()){
            Router.newIntent(mContext).to(LoginActivity::class.java).launch()
            return
        }

        //Router.newIntent(mContext).putInt("data",2).to(SendActivity::class.java).launch()
        RxHttpScope().launch {
            //验证是否可申请,个人认证、企业认证，只要其中一个认证了就能发布服务
            //0认证中 1认证成功 -1认证失败
            var req2= RxHttp.get("talent/validateRepeat").add("userId",MyUtils.getUserid()).toResponse<SettleStatus>().await()
            if (req2.companySettle =="1" || req2.personalSettle=="1"){
                Router.newIntent(mContext).putInt("data",2).to(SendActivity::class.java).launch()
                this@PublishPopup.dismiss()
            } else {
                ToastUtils.showShort("认证完成后才能发布服务哦")
            }
        }

    }


}