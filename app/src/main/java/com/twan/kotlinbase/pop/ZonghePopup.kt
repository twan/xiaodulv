package com.twan.kotlinbase.pop

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lxj.xpopup.impl.PartShadowPopupView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.bean.SpiBean
import com.twan.kotlinbase.util.*
import kotlinx.android.synthetic.main.pop_partshdow_city_select.view.*
import kotlinx.android.synthetic.main.pop_select.view.*

/**
 * Description:  - 自定义局部阴影弹窗
 *
 * 调用方式:
    XPopup.Builder(mActivity)
        .atView(toolbar)
        .setPopupCallback(object : SimpleCallback() {
            override fun onShow(popupView: BasePopupView) {

            }

            override fun onDismiss(popupView: BasePopupView) {
            }
    }).asCustom(FileSortPopupView(mActivity!!)).show()

 */
class ZonghePopup(var tagView:View, context: Context, var onselect: ((pos:Int, SpiBean)->Unit)?=null) : PartShadowPopupView(context) {
    var mContext = context
    override fun getImplLayoutId(): Int {
        return R.layout.pop_select
    }

    override fun onCreate() {
        super.onCreate()
        generData()
        rv_list.adapter= object : BaseQuickAdapter<SpiBean, BaseViewHolder>(R.layout.item_my_spi,datas) {
            override fun convert(holder: BaseViewHolder, model: SpiBean) {
                var iv_check=holder.getView<ImageView>(R.id.iv_check)
                var tv_desc=holder.getView<TextView>(R.id.tv_desc)
                holder!!.setText(R.id.tv_desc, model.toString())
                if (tagView.tag !=null && tagView.tag == holder.position){
                    iv_check.visibility=View.VISIBLE
                    tv_desc.setTextColor(mContext.resources.getColor(R.color.text_orange))
                } else {
                    iv_check.visibility=View.GONE
                }
            }
        }.also {
            it.setOnItemClickListener { adapter, view, position ->
                tagView.tag = position
                onselect?.run {
                    (onselect!!)(position,datas[position])
                }
                this@ZonghePopup.dismiss()
            }
        }
    }

    var datas = mutableListOf<SpiBean>()
    fun generData(){
        datas.add(SpiBean("综合"))
        datas.add(SpiBean("评分升序"))
        datas.add(SpiBean("评分降序"))
        datas.add(SpiBean("价格升序"))
        datas.add(SpiBean("价格降序"))
    }

}