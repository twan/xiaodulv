package com.twan.kotlinbase.pop

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lxj.xpopup.impl.PartShadowPopupView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.util.Province
import kotlinx.android.synthetic.main.pop_partshdow_city_select.view.*

/**
 * Description:  - 自定义局部阴影弹窗
 *
 * 调用方式:
    XPopup.Builder(mActivity)
        .atView(toolbar)
        .setPopupCallback(object : SimpleCallback() {
            override fun onShow(popupView: BasePopupView) {

            }

            override fun onDismiss(popupView: BasePopupView) {
            }
    }).asCustom(FileSortPopupView(mActivity!!)).show()

 */
class SelectCityPopup(context: Context,var done:(district:String)->Unit) : PartShadowPopupView(context) {
    var mContext = context
    override fun getImplLayoutId(): Int {
        return R.layout.pop_partshdow_city_select
    }

    data class LastPosition(
        var item:String,
        var view: TextView
    )

    override fun onCreate() {
        super.onCreate()
        var provider = CityDataProvider(mContext)
        val mSelectedOptions = mutableMapOf<String, String>()   // 已选值
        val provinceData = provider.stageData(STAGE_KEY_PROVINCE,mSelectedOptions)
        var cityData = provider.stageData(STAGE_KEY_CITY,mSelectedOptions)
        var districtData = provider.stageData(STAGE_KEY_DISTRICT,mSelectedOptions)

        var mCityAdapter :BaseQuickAdapter<String,BaseViewHolder>?=null
        var mDistrictAdapter :BaseQuickAdapter<String,BaseViewHolder>?=null

        var lastProvicePosition:LastPosition?=null
        var lastCityPosition:LastPosition?=null
        var lastDistrictPosition:LastPosition?=null

        rv_province.addItemDecoration(DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL))
        rv_province.adapter=object :BaseQuickAdapter<String,BaseViewHolder>(R.layout.item_city,provinceData!!.toMutableList()){
            override fun convert(holder: BaseViewHolder, item: String) {
                var item_text= holder.getView<TextView>(R.id.tv_desc)
                item_text.text=item

                item_text.setOnClickListener {
                    if ((lastProvicePosition!=null && lastProvicePosition!!.item != item) ||lastProvicePosition==null) {
                        lastProvicePosition?.view?.setTextColor(resources.getColor(R.color.text_black))
                        lastCityPosition?.view?.setTextColor(resources.getColor(R.color.text_black))
                        lastDistrictPosition?.view?.setTextColor(resources.getColor(R.color.text_black))

                        lastProvicePosition = LastPosition(item, item_text)
                        item_text.setTextColor(resources.getColor(R.color.text_red))
                        mSelectedOptions[STAGE_KEY_PROVINCE] = item
                        cityData = provider.stageData(STAGE_KEY_CITY, mSelectedOptions)
                        mCityAdapter?.setList(cityData)
                        mDistrictAdapter?.setList(emptyList())
                    }
                }
            }
        }

        rv_city.adapter=object :BaseQuickAdapter<String,BaseViewHolder>(R.layout.item_city){
            override fun convert(holder: BaseViewHolder, item: String) {
                var item_text= holder.getView<TextView>(R.id.tv_desc)
                item_text.text=item

                item_text.setOnClickListener {
                    if ((lastCityPosition!=null && lastCityPosition!!.item != item) || lastCityPosition==null){
                        lastCityPosition?.view?.setTextColor(resources.getColor(R.color.text_black))
                        lastDistrictPosition?.view?.setTextColor(resources.getColor(R.color.text_black))

                        lastCityPosition=LastPosition(item,item_text)
                        item_text.setTextColor(resources.getColor(R.color.text_red))
                        mSelectedOptions[STAGE_KEY_CITY]=item
                        districtData = provider.stageData(STAGE_KEY_DISTRICT,mSelectedOptions)
                        mDistrictAdapter?.setList(districtData)

                        if (item =="全国"){
                            done(item)
                            this@SelectCityPopup.dismiss()
                        }
                    }
                }
            }
        }.also {
            mCityAdapter=it
        }

        rv_district.adapter=object :BaseQuickAdapter<String,BaseViewHolder>(R.layout.item_city){
            override fun convert(holder: BaseViewHolder, item: String) {
                var item_text= holder.getView<TextView>(R.id.tv_desc)
                item_text.text=item
                item_text.setOnClickListener {
                    if ((lastDistrictPosition!=null && lastDistrictPosition!!.item != item) || lastDistrictPosition==null) {
                        lastDistrictPosition?.view?.setTextColor(resources.getColor(R.color.text_black))

                        lastDistrictPosition = LastPosition(item, item_text)
                        item_text.setTextColor(resources.getColor(R.color.text_red))

                        done(item)
                        this@SelectCityPopup.dismiss()
                    }
                }
            }
        }.also {
            mDistrictAdapter=it
        }


    }

}