package com.twan.kotlinbase.pop;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.lxj.xpopup.core.AttachPopupView;
import com.twan.kotlinbase.R;

import androidx.annotation.NonNull;

/**
 * Description:
 * Create by lxj, at 2019/3/13
 */
public class CustomAttachPopup2 extends AttachPopupView {
    public CustomAttachPopup2(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.pop_custom_attach;
    }

    @Override
    protected void onCreate() {
        super.onCreate();

    }

    //如果要自定义弹窗的背景，不要给布局设置背景图片，重写这个方法返回一个Drawable即可
    protected Drawable getPopupBackground() {
        return getResources().getDrawable(R.mipmap.shadow_bg);
    }
}
