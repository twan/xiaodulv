package com.twan.kotlinbase.pop;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.SpanUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.SelPayWayAdapter;
import com.twan.kotlinbase.bean.SelPayWayBean;

import java.util.ArrayList;

import razerdp.basepopup.BasePopupWindow;

/**
 * @anthor zhoujr
 * @time 2021/4/15 15:08
 * @describe
 */
public class SelPayWayPop extends BasePopupWindow {

    TextView popCost;
    SelPayWayAdapter selPayWayAdapter;
    int oldPosition = 0;
    ConfimPay confimPay;

    public SelPayWayPop(@NonNull Context context) {
        super(context);
        setPopupGravity(Gravity.BOTTOM);
        bindView();
    }

    private void bindView() {
        ImageView popClose = findViewById(R.id.popClose);
        popCost = findViewById(R.id.popCost);
        RecyclerView popRecy = findViewById(R.id.popRecy);
        Button popBtn = findViewById(R.id.popBtn);
        popCost.setText(new SpanUtils()
                .append("¥")
                .setFontSize(20,true)
                .setForegroundColor(Color.parseColor("#FF333333"))
                .append("699")
                .setFontSize(40,true)
                .setForegroundColor(Color.parseColor("#FF333333"))
                .create()
        );
        ArrayList<SelPayWayBean> selPayWayBeans = new ArrayList<>();
        selPayWayBeans.add(new SelPayWayBean(R.mipmap.icon_wechatpay,"微信支付",true));
        selPayWayBeans.add(new SelPayWayBean(R.mipmap.icon_alipay,"支付宝支付",false));
        selPayWayAdapter = new SelPayWayAdapter();
        popRecy.setLayoutManager(new LinearLayoutManager(getContext()));
        popRecy.setAdapter(selPayWayAdapter);
        selPayWayAdapter.setNewData(selPayWayBeans);
        selPayWayAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                selPayWayBeans.get(oldPosition).setSel(false);
                selPayWayBeans.get(position).setSel(true);
                selPayWayAdapter.notifyDataSetChanged();
                oldPosition = position;
            }
        });
        popClose.setOnClickListener(view -> dismiss());
        popBtn.setOnClickListener(view -> {
            if(confimPay != null){
                confimPay.ConfimSelPayWay(oldPosition);
                dismiss();
            }
        });
    }

    @Override
    public View onCreateContentView() {
        return createPopupById(R.layout.pop_selpayway);
    }

    public interface ConfimPay{
        void ConfimSelPayWay(int position);
    }

    public void setConfimPay(ConfimPay confimPay){
        this.confimPay = confimPay;
    }
}
