package com.twan.kotlinbase.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.FrameLayout;

import androidx.annotation.ColorRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.gyf.immersionbar.ImmersionBar;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.app.App;
import com.twan.kotlinbase.util.ToolbarHelper;
import com.twan.kotlinbase.widgets.CustomProgress;

import butterknife.ButterKnife;


public abstract class BaseActivity<V, T extends BasePresenter<V>> extends AppCompatActivity {

    protected T mPresenter;
   public static Bundle bundle;
    private FrameLayout mBaseRootView;
    private Toolbar mBaseToolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_abse);
        mBaseRootView = findViewById(R.id.contentView);
        mBaseToolbar = findViewById(R.id.mBaseTooBar);
        setSupportActionBar(mBaseToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mBaseToolbar.setNavigationOnClickListener(v -> onBackPressed());
        bundle=new Bundle();
        init();
        //判断是否使用MVP模式
        mPresenter = createPresenter();
        if (mPresenter != null) {
            mPresenter.attachView((V) this);//因为之后所有的子类都要实现对应的View接口
        }



        //子类不再需要设置布局ID，也不再需要使用ButterKnife.bind()
        setContentView(provideContentViewId());
        View mToolbar = mBaseRootView.findViewWithTag("toolbar");

        ImmersionBar.with(this)
                .statusBarColor(R.color.transparent)
                .statusBarDarkFont(true)
                .flymeOSStatusBarFontColor(R.color.black)
                .titleBar(mToolbar == null ? mBaseToolbar : mToolbar)
                .init();
        setGlobalLayoutListener();
        initView();
        initData();
        initListener();
    }


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View view = getLayoutInflater().inflate(layoutResID, mBaseRootView, false);
        mBaseRootView.addView(view, 0);
        ButterKnife.bind(this, view);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }

    //在setContentView()调用之前调用，可以设置WindowFeature(如：this.requestWindowFeature(Window.FEATURE_NO_TITLE);)
    public void init() {
    }

    public void initView() {
    }

    public void initData() {
    }

    public void initListener() {
    }

    //用于创建Presenter和判断是否使用MVP模式(由子类实现)
    protected abstract T createPresenter();

    //得到当前界面的布局文件id(由子类实现)
    protected abstract int provideContentViewId();

    /**
     * 是否让Toolbar有返回按钮(默认可以，一般一个应用中除了主界面，其他界面都是可以有返回按钮的)
     */
    protected boolean isToolbarCanBack() {
        return true;
    }

    /**
     * 显示等待提示框
     */
    public void showWaitingDialog(String tip) {
        hideWaitingDialog();
        CustomProgress.show(this,tip);
    }

    /**
     * 隐藏等待提示框
     */
    public void hideWaitingDialog() {
        CustomProgress.setdismiss();
    }


    protected void hideBaseToolBar() {
        mBaseToolbar.setVisibility(View.GONE);
    }


    protected  void setToolBarImgAndBg(int iconimg, int color){
        mBaseToolbar.setNavigationIcon(iconimg);
        mBaseToolbar.setBackgroundColor(color);
    }

    protected void setActivityTitle(String title) {
        setActivityTitle(title, R.color.c_333333);
    }

    protected void setActivityTitle(String title, @ColorRes int color) {
        ToolbarHelper.addMiddleTitle(this, title, ContextCompat.getColor(this, color), mBaseToolbar);
    }

    public void jumpToActivityForResult(Class activity, int code) {
        Intent intent = new Intent(this,activity);
        startActivityForResult(intent,code);
    }


    public void jumpToActivity(Intent intent) {
        startActivity(intent);
    }

    public void jumpToActivity(Class activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }



    public void jumpToActivityForBundle(Class activity, Bundle bundle) {
        Intent intent = new Intent(this,activity);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void jumpToActivityBundleForResult(Class activity, Bundle bundle, int code) {
        Intent intent = new Intent(this,activity);
        intent.putExtras(bundle);
        startActivityForResult(intent,code);
    }


    public void jumpToActivityAndClearTask(Class activity) {
        Intent intent = new Intent(this, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void jumpToActivityAndTaskForBundle(Class activity, Bundle bundle) {
        Intent intent = new Intent(this, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtras(bundle);
        startActivity(intent);
    }
    public void jumpToActivityAndNewTaskForBundle(Class activity, Bundle bundle) {
        Intent intent = new Intent(this, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    public void jumpToActivityAndNewTASK(Class activity) {
        Intent intent = new Intent(this, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void jumpToActivityAndClearTop(Class activity) {
        Intent intent = new Intent(this, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void jumpToActivityAndClearTopForBundle(Class activity, Bundle bundle) {
        Intent intent = new Intent(this, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void setGlobalLayoutListener() {
        final View layout = findViewById(Window.ID_ANDROID_CONTENT);
        ViewTreeObserver observer = layout.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                onGlobalLayoutCompleted();
            }
        });
    }

    protected void onGlobalLayoutCompleted() {

    }

}
