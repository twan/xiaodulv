package com.twan.kotlinbase.app

import android.Manifest
import java.io.Serializable

val testImage = "https://img.zcool.cn/community/013de756fb63036ac7257948747896.jpg"
val isSimulater=false
var permissionArr = arrayOf( //SD卡读写操作
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION)



//需求
enum class Role(){
    USER,//用户,默认
    SERVICE_PROVIDER//服务提供商
}

//需求
enum class Need(var desc:String, var title:String):Serializable{
//    shopLive("店铺直播"),//店铺直播
//    shortVideoMake("短视频制作"),//短视频制作
//    shortVideoPromote("短视频推广"),//短视频推广
//    actorModel("演员模特"),//演员模特
//    actor("演员"),//演员
//    model("模特"),//模特
    /** 演员 */
    ACTOR("actor","演员"),
    /** 模特 */
    MODEL("model","模特"),
    ACTOR_MODEL("actorModel","演员模特"),
    /** 店铺直播 */
    SHOP_LIVE("shopLive","店铺直播"),
    /** 短视频制作 */
    SHORT_VIDEO_MAKE("shortVideoMake","短视频制作"),
    /** 短视频推广 */
    SHORT_VIDEO_PROMOTE("shortVideoPromote","短视频推广");
}

fun getNeedByDesc(desc: String):Need{
    return when (desc){
        "actor" -> Need.ACTOR
        "model" -> Need.MODEL
        "actorModel" -> Need.ACTOR_MODEL
        "shopLive" -> Need.SHOP_LIVE
        "shortVideoMake" -> Need.SHORT_VIDEO_MAKE
        "shortVideoPromote" -> Need.SHORT_VIDEO_PROMOTE
        else -> Need.SHOP_LIVE
    }
}

enum class EnumOrderStatus(var id:String){
    /** 全部,自己加的 */
    ALL(""),
    /** 待付款 */
    PENDING_PAYMENT("0"),
    /** 待发货 */
    TO_BE_SHIPPED("1"),
    /** 待收货 */
    TO_BE_RECEIVED("2"),
    /** 进行中 */
    IN_THE_PROCESS("3"),
    /** 待评价 */
    TO_BE_EVALUATED("4"),
    /** 已完成 */
    COMPLETED("5"),
    /** 已取消 */
    CANCELED("6"),
    /** 已退款 */
    REFUNDED("7"),
    /** 交易关闭 */
    CLOSED("8"),
    /** 订单超时 */
    TIMEOUT("10"),
}

enum class EnumServiceOrderStatus(var id:String){
    /** 全部 */
    ALL(""),
    /** 待付款 */
    PENDING_PAYMENT("0"),
    /** 待寄样 */
    TO_BE_SENDING("1"),
    /** 服务中 */
    TO_BE_SERVICING("3"),
    /** 已完成 */
    COMPLETED("5"),
}

enum class EnumSaleStatus(var id:String){
    /** 全部,自己加的 */
    ALL(""),
    /** 上架 */
    SALE_UP("1"),
    /** 下架 */
    SALE_DOWN("-1"),
    /** 待发布 */
    SALE_PENDING("2"),
}