package com.twan.kotlinbase.api;


import com.twan.kotlinbase.bean.AliPayBean;
import com.twan.kotlinbase.bean.CancelOrderBean;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.bean.OrderDetailBean;
import com.twan.kotlinbase.bean.WeChatPayBean;

import java.util.Map;
import okhttp3.MultipartBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * @描述 server端api
 */

public interface MyApi {


    @GET("api/order/findAllByUserId")
    Observable<OrderBean> getOrderLst(@QueryMap Map<String, String> map);


    /**
     * 确认收货
     * @param map
     * @return
     */
    @GET("api/order/confirmReceipt")
    Observable<String> confimreceive(@QueryMap Map<String, String> map);

    /**
     * 确认收到样品
     * @param map
     * @return
     */
    @GET("api/order/confirmSample")
    Observable<String> confimreceiveSimple(@QueryMap Map<String, String> map);


    /**
     * 取消订单
     * @param map
     * @return
     */
    @GET("api/order/cancelOrder")
    Observable<CancelOrderBean> cancelOrder(@QueryMap Map<String, String> map);

    /**
     * 删除订单
     * @param map
     * @return
     */
    @GET("api/order/deleteById")
    Observable<CancelOrderBean> deleteOrder(@QueryMap Map<String, String> map);


    /**
     * 订单详情
     * @param map
     * @return
     */
    @GET("api/order/findDetailByOrderId")
    Observable<OrderDetailBean> getorderDetail(@QueryMap Map<String, String> map);

    /**
     * 支付宝支付
     * @param map
     * @return
     */
    @FormUrlEncoded
    @POST("api/aliPay/toPayAsMobile")
    Observable<AliPayBean> getalipaydata(@FieldMap Map<String, String> map);

    /**
     * 微信支付
     * @param map
     * @return
     */
    @FormUrlEncoded
    @POST("/api/wxPay/toPayAsMobile")
    Observable<WeChatPayBean> getwechatpaydata(@FieldMap Map<String, String> map);
}