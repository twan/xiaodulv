package com.twan.kotlinbase.api;



import com.twan.kotlinbase.api.base.BaseApiRetrofit;
import com.twan.kotlinbase.bean.AliPayBean;
import com.twan.kotlinbase.bean.CancelOrderBean;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.bean.OrderDetailBean;
import com.twan.kotlinbase.bean.WeChatPayBean;
import com.twan.kotlinbase.factory.CustomGsonConverterFactory;
import com.twan.kotlinbase.factory.StringConverterFactory;
import com.twan.kotlinbase.network.Api;
import com.twan.kotlinbase.util.Constant;
import com.twan.kotlinbase.util.MyUtils;

import java.util.HashMap;
import java.util.Map;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;

/**
 * @描述 使用Retrofit对网络请求进行配置
 */
public class ApiRetrofit extends BaseApiRetrofit {

    public MyApi mApi;
    public static ApiRetrofit mInstance;

    private ApiRetrofit() {
        super();

        //在构造方法中完成对Retrofit接口的初始化
        mApi = new Retrofit.Builder()
                .baseUrl(Constant.BASE_HTTP_URL)
                .client(getClient())
//                .addConverterFactory(GsonConverterFactory.create(buildGson()))
                .addConverterFactory(CustomGsonConverterFactory.create())
                .addConverterFactory(StringConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(MyApi.class);
    }


    public static ApiRetrofit getInstance() {
        if (mInstance == null) {
            synchronized (ApiRetrofit.class) {
                if (mInstance == null) {
                    mInstance = new ApiRetrofit();
                }
            }
        }
        return mInstance;
    }

    public Observable<OrderBean> getOrderLst(String status, String page,String from) {
        Map<String, String> map = new HashMap<>();
        addDefaultData(map);
        map.put("pageSize","20");
        map.put("status",status);
        map.put("currentPage",page);
        map.put(from,MyUtils.INSTANCE.getUserid());
        return mApi.getOrderLst(map);
    }

    public Observable<String> confimreceive(String orderId) {
        Map<String, String> map = new HashMap<>();
        addDefaultData(map);
        map.put("orderId",orderId);
        return mApi.confimreceive(map);
    }

    public Observable<String> confimreceiveSimple(String orderId) {
        Map<String, String> map = new HashMap<>();
        addDefaultData(map);
        map.put("orderId",orderId);
        return mApi.confimreceiveSimple(map);
    }

    public Observable<OrderDetailBean> getorderDetail(String orderId) {
        Map<String, String> map = new HashMap<>();
        addDefaultData(map);
        map.put("orderId",orderId);
        return mApi.getorderDetail(map);
    }

    public Observable<CancelOrderBean> cancelOrder(String orderId) {
        Map<String, String> map = new HashMap<>();
        addDefaultData(map);
        map.put("orderId",orderId);
        return mApi.cancelOrder(map);
    }

    public Observable<CancelOrderBean> deleteOrder(String orderId) {
        Map<String, String> map = new HashMap<>();
        addDefaultData(map);
        map.put("orderId",orderId);
        return mApi.deleteOrder(map);
    }

    public Observable<AliPayBean> getalipaydata(String orderId,String totalAmount) {
        Map<String, String> map = new HashMap<>();
        addDefaultData(map);
        map.put("orderId",orderId);
        map.put("totalAmount",totalAmount);
        return mApi.getalipaydata(map);
    }

    public Observable<WeChatPayBean> getwechatpaydata(String orderId, String totalAmount) {
        Map<String, String> map = new HashMap<>();
        addDefaultData(map);
        map.put("orderId",orderId);
        map.put("totalAmount",totalAmount);
        return mApi.getwechatpaydata(map);
    }

    private  void addDefaultData(Map<String, String> map) {
      map.put("userId",MyUtils.INSTANCE.getUserid());
    }


}
