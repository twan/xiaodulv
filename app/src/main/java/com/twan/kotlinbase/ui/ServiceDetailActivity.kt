package com.twan.kotlinbase.ui

import android.content.Intent
import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lxj.xpopup.XPopup
import com.tencent.imsdk.v2.V2TIMConversation
import com.tencent.qcloud.tim.demo.DemoApplication
import com.tencent.qcloud.tim.demo.chat.ChatActivity
import com.tencent.qcloud.tim.demo.utils.Constants
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.MyBannerImageAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.BannerBean
import com.twan.kotlinbase.bean.ServiceBean
import com.twan.kotlinbase.bean.ServiceDetail
import com.twan.kotlinbase.databinding.ActivityServiceDetailBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.GuigeActorModelPopup
import com.twan.kotlinbase.pop.GuigeShopLivePopup
import com.twan.kotlinbase.pop.GuigeVideoMakePopup
import com.twan.kotlinbase.pop.GuigeVideoPremotePopup
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.XpopupUtils
import com.twan.kotlinbase.widgets.router.Router
import com.youth.banner.Banner
import com.youth.banner.config.IndicatorConfig
import com.youth.banner.indicator.CircleIndicator
import kotlinx.android.synthetic.main.activity_service_detail.*
import kotlinx.android.synthetic.main.activity_service_detail.you_banner_pic
import kotlinx.android.synthetic.main.fragment_dresser_detail.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class ServiceDetailActivity : BaseDataBindingActivity<ActivityServiceDetailBinding>(){
    lateinit var serviceBean:ServiceBean
    lateinit var mNeed:Need
    var dataUrl="service/shopLive/detail"
    lateinit var mServiceDetail: ServiceDetail
    override fun getLayout(): Int {
        return R.layout.activity_service_detail
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="服务详情"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        serviceBean=intent.getSerializableExtra("ServiceBean") as ServiceBean
        mNeed=intent.getSerializableExtra("need") as Need
        initDataSource()
        getData()
    }

    fun initDataSource(){
        if (mNeed == Need.SHOP_LIVE){
            dataUrl="service/shopLive/detail"
        } else if (mNeed==Need.SHORT_VIDEO_MAKE){
            dataUrl="service/shortVideoMake/detail"
        } else if (mNeed==Need.SHORT_VIDEO_PROMOTE){
            dataUrl="service/shortVideoPromote/detail"
        } else {
            if (serviceBean.serviceTypeValue =="actor") {
                dataUrl = "service/actor/detail"
            } else {
                dataUrl = "service/model/detail"
            }
        }
    }

    @OnClick(R.id.tv_home)
    fun tohome(){
        Router.newIntent(this)
            .putString("id",mServiceDetail.talentDTO?.talentId)
            .putString("userId",mServiceDetail.talentDTO?.userId)
            .putString("nickName",mServiceDetail.talentDTO?.nickName)
            .to(DresserDetailActivity::class.java).launch()
    }

    fun getData(){
        RxHttpScope().launch {
            var req=RxHttp.get(dataUrl).add("serviceId",serviceBean.id).toResponse<ServiceDetail>().await()
            mServiceDetail=req
            handleData()
            mServiceDetail.need=mNeed
            mServiceDetail.isActor= (serviceBean.serviceTypeValue =="actor")
            mBinding.item=mServiceDetail
            showUI()
        }
    }

    //返回的数据的特殊处理
    fun handleData(){
        mServiceDetail.popularizeItemDTO?.run {
            var dto=this
            dto.perPrices = ""
            dto.perFans = ""
            dto.perAccounts = ""
            dto.platformList?.run {
                this.forEach {
//                    dto.perPrices += (it.platformAccount+" "+it.cooperationPrice+";")
//                    dto.perFans += (it.platformAccount+" "+it.fansNum+";")
//                    dto.perAccounts += (it.platformAccount+" "+it.platformAccount+";")
                    dto.perPrices += it.cooperationPrice+";"
                    dto.perFans += it.fansNum+";"
                    dto.perAccounts += it.platformAccount+";"
                }
            }
        }
    }

    fun showUI(){
        var urlsPic = mServiceDetail.baseItemDTO?.imageUrl?.split(",")
        var urlsDetail = mServiceDetail.detailImagesDTO?.imagesUrl
        if (urlsPic != null ) initPicBanner(urlsPic.toTypedArray(),you_banner_pic)
        initDetailRv(urlsDetail)
        GlideUtils.loadCicle(this,iv_talent,mServiceDetail.talentDTO?.avatarUlr)
    }
    //详情
    fun initDetailRv(datas:MutableList<String>){
        var realdatas = datas.filter { it.isNotEmpty() }
        rv_detail.adapter=object :BaseQuickAdapter<String,BaseViewHolder>(R.layout.item_service_detail_pic,realdatas.toMutableList()){
            override fun convert(holder: BaseViewHolder, item: String) {
                var ivpic = holder.getView<ImageView>(R.id.iv_pic)
                GlideUtils.load(this@ServiceDetailActivity,ivpic,item,R.mipmap.test)

                ivpic.setOnClickListener {
                    XpopupUtils.previewMultiImgs(this@ServiceDetailActivity,holder.adapterPosition,ivpic,realdatas as MutableList<Any>)
                }
            }
        }
    }

    fun initPicBanner(datas:Array<String>,banner: Banner<*,*>) {
        var req = mutableListOf<BannerBean>()
        if (datas.isNotEmpty()){
            datas.forEach {
                if (it.isNotEmpty()) req.add(BannerBean(imageUrl = it))
            }
        }
        var adapter = MyBannerImageAdapter(this!!, req)
        banner.let {
            it.setIndicatorGravity(IndicatorConfig.Direction.CENTER)
            it.indicator = CircleIndicator(this)
            it.adapter = adapter
        }
    }

    @OnClick(R.id.tv_fav)
    fun fav(){
        RxHttpScope().launch {
            RxHttp.postJson("favorites/save")
                .add("favoritesId",serviceBean.id)
                .add("userId", MyUtils.getUserid())
                .toResponse<Any>().await()
            ToastUtils.showShort("收藏成功")
        }
    }
    @OnClick(R.id.tv_call)
    fun call(){
        MyUtils.callPhone(this,"19157812217")
    }
    @OnClick(R.id.btn_chat)
    fun chat(){
        if (MyUtils.isLogin()){
            MyUtils.chat(this,mServiceDetail.talentDTO.userId,if (mServiceDetail.talentDTO.nickName.isNullOrEmpty()) "达人" else mServiceDetail.talentDTO.nickName)
        }else{
            Router.newIntent(this).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.rl_talent_detail,R.id.ll_talent)
    fun toTalent(){
        if (mServiceDetail.talentDTO == null){
            ToastUtils.showShort("达人数据异常")
            return
        }
        Router.newIntent(this).putString("id",mServiceDetail.talentDTO.talentId)
            .putString("userId",mServiceDetail.talentDTO.userId)
            .putString("nickName",mServiceDetail.talentDTO?.nickName)
            .to(DresserDetailActivity::class.java).launch()
    }

    @OnClick(R.id.btn_buy)
    fun buy(){
        if (!MyUtils.isLogin()){
            Router.newIntent(this).to(LoginActivity::class.java).launch()
            return
        }
        if (mNeed == Need.SHOP_LIVE){
//            XPopup.Builder(this).asCustom(GuigeVideoMakePopup(this,mServiceDetail)).show()
            XPopup.Builder(this).asCustom(GuigeShopLivePopup(this,mNeed,serviceBean,mServiceDetail)).show()
        } else if (mNeed == Need.SHORT_VIDEO_MAKE){
            XPopup.Builder(this).asCustom(GuigeVideoMakePopup(this,mNeed,serviceBean,mServiceDetail)).show()
        } else if (mNeed == Need.SHORT_VIDEO_PROMOTE){
            XPopup.Builder(this).asCustom(GuigeVideoPremotePopup(this,mNeed,serviceBean,mServiceDetail)).show()
        } else {
            XPopup.Builder(this).asCustom(GuigeActorModelPopup(this,mNeed,serviceBean,mServiceDetail)).show()
        }

    }

}