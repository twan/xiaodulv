package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.*
import com.twan.kotlinbase.databinding.*
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.wx.goodview.GoodView
import kotlinx.android.synthetic.main.activity_single_rv.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class DynamicManageActivity : BaseDataBindingActivity<ActivitySingleRvBinding>(){
    private lateinit var mAdpater: BaseQuickAdapter<Discovery, BaseDataBindingHolder<ItemDiscoveryBinding>>
    lateinit var goodView: GoodView
    override fun getLayout(): Int {
        return R.layout.activity_single_rv
    }

    override fun initEventAndData() {
        whiteToolbar("动态管理","发布")
        initRv()
    }

    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(this))
        refreshLayout.setRefreshFooter(ClassicsFooter(this))
        refreshLayout.setOnRefreshListener {
            currentPage=1
            reqData()
        }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setOnLoadMoreListener { reqData() }
        single_rv.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        single_rv.adapter=object :BaseQuickAdapter<Discovery,BaseDataBindingHolder<ItemDiscoveryBinding>>(R.layout.item_discovery){
            override fun convert(holder: BaseDataBindingHolder<ItemDiscoveryBinding>, item: Discovery) {
                holder.dataBinding!!.item=item
                GlideUtils.load(this@DynamicManageActivity,holder.dataBinding!!.ivAvater,item.avatarUrl)
                holder.dataBinding!!.tvShijian.text= RelativeDateFormat.format(DateUtils.convetDate2(item.releaseTime))
                holder.dataBinding!!.tvLike.setOnClickListener {
                    holder.dataBinding!!.tvLike.text=(item.likeCount+1).toString()
                    goodView.setText("+1")//goodView.setImage(getResources().getDrawable(R.mipmap.good_checked));
                    goodView.show(it)
                    dianzan(item.id)
                }
                //九宫格图片
                var rv_pic = holder.getView<RecyclerView>(R.id.rv_pic)
                rv_pic.layoutManager= GridLayoutManager(this@DynamicManageActivity,3)
                rv_pic.adapter = object : BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_pic, item.imagesUrl){
                    override fun convert(holder: BaseViewHolder, url: String) {
                        var iv_pic =holder.getView<ImageView>(R.id.iv_pic)
                        if (url.endsWith("mp4")){
                            GlideUtils.loadVideo0Frame(this@DynamicManageActivity,iv_pic!!,url)
                        } else {
                            GlideUtils.loadCorner(this@DynamicManageActivity, iv_pic, url.toString())
                        }
                        iv_pic.setOnClickListener {
                            XpopupUtils.previewMultiImgs(this@DynamicManageActivity,holder.adapterPosition,iv_pic,item.imagesUrl as MutableList<Any>)
                        }

                    }
                }
            }

        }.also {
            mAdpater=it
            it.setOnItemClickListener { adapter, view, position ->
                Router.newIntent(this).putSerializable("data",it.data[position]).to(DiscoveryDetailActivity::class.java).launch()
            }
            reqData()
        }
    }

    fun dianzan(momentsId:String){
        RxHttpScope().launch {
            RxHttp.postJson("moment/like").add("likedPostId",MyUtils.getUserid()).add("momentsId",momentsId).toResponse<Any>().await()
        }
    }

    var currentPage=1
    fun reqData(){
        RxHttpScope(this,refreshLayout).launch {
            var req = RxHttp.get("moment/getByCondition")
                    .add("pageSize",20)
                    .add("currentPage",currentPage++)
                    .toResponse<PageData<Discovery>>().await()
            if (currentPage==2) mAdpater.setList(req.content)
            else  mAdpater.addData(req.content)
        }
    }

    override fun rightClick() {
        if (MyUtils.isLogin()) {
            Router.newIntent(this).to(SendDynamicActivity::class.java).launch()
        } else {
            Router.newIntent(this).to(LoginActivity::class.java).launch()
        }
    }
}