package com.twan.kotlinbase.ui;

import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.OrderAdapter;
import com.twan.kotlinbase.app.EnumOrderStatus;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.fragment.NewOrderFragment;
import com.twan.kotlinbase.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @anthor zhoujr
 * @time 2021/4/13 10:00
 * @describe
 */
public class NewOrderActivity extends BaseActivity {
    ArrayList<NewOrderFragment> orderFragments;
    @BindView(R.id.newOrderTab)
    SlidingTabLayout newOrderTab;
    @BindView(R.id.newOrderVP)
    ViewPager newOrderVP;
    int index;

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("我的订单");
        setToolBarImgAndBg(R.mipmap.ic_arrow_back_white, getResources().getColor(R.color.head_bg));
        index = getIntent().getIntExtra("index",0);
        orderFragments = new ArrayList<>();
        orderFragments.add(new NewOrderFragment(EnumOrderStatus.ALL));
        orderFragments.add(new NewOrderFragment(EnumOrderStatus.PENDING_PAYMENT));
        orderFragments.add(new NewOrderFragment(EnumOrderStatus.TO_BE_SHIPPED));
        orderFragments.add(new NewOrderFragment(EnumOrderStatus.TO_BE_RECEIVED));
        orderFragments.add(new NewOrderFragment(EnumOrderStatus.IN_THE_PROCESS));
        orderFragments.add(new NewOrderFragment(EnumOrderStatus.TO_BE_EVALUATED));
        orderFragments.add(new NewOrderFragment(EnumOrderStatus.COMPLETED));
        orderFragments.add(new NewOrderFragment(EnumOrderStatus.CANCELED));
        OrderAdapter orderAdapter = new OrderAdapter(getSupportFragmentManager(),orderFragments);
        newOrderVP.setAdapter(orderAdapter);
        newOrderTab.setViewPager(newOrderVP, Constant.sTabTitles);
        newOrderVP.setOffscreenPageLimit(orderFragments.size());
        newOrderVP.setCurrentItem(index);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_neworder;
    }

}
