package com.twan.kotlinbase.ui

import android.view.View
import android.widget.CheckBox
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.OnClick
import com.blankj.utilcode.util.TimeUtils
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.haibin.calendarview.Calendar
import com.haibin.calendarview.CalendarView
import com.haibin.calendarview.CalendarView.OnYearChangeListener
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.CanSchdule
import com.twan.kotlinbase.bean.ServiceBean
import com.twan.kotlinbase.bean.ShopLiveFormat
import com.twan.kotlinbase.databinding.ActivityCalendarBinding
import com.twan.kotlinbase.databinding.ItemYuyueBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.DateUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_calendar.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse
import java.text.SimpleDateFormat


class CalendarActivity : BaseDataBindingActivity<ActivityCalendarBinding>(), CalendarView.OnCalendarSelectListener, OnYearChangeListener{
    val datas= mutableListOf<CanSchdule>(
        CanSchdule("上午\n1:00"),CanSchdule("上午\n2:00"),CanSchdule("上午\n3:00"),CanSchdule("上午\n4:00"),CanSchdule("上午\n5:00"),CanSchdule("上午\n6:00"),
        CanSchdule("上午\n7:00"),CanSchdule("上午\n8:00"),CanSchdule("上午\n9:00"),CanSchdule("上午\n10:00"),CanSchdule("上午\n11:00"),CanSchdule("上午\n12:00"),
        CanSchdule("下午\n1:00"),CanSchdule("下午\n2:00"),CanSchdule("下午\n3:00"),CanSchdule("下午\n4:00"),CanSchdule("下午\n5:00"),CanSchdule("下午\n6:00"),
        CanSchdule("下午\n7:00"),CanSchdule("下午\n8:00"),CanSchdule("下午\n9:00"),CanSchdule("下午\n10:00"),CanSchdule("下午\n11:00"),CanSchdule("下午\n12:00"))
    lateinit var serviceBean:ServiceBean
    lateinit var shopLiveFormat:ShopLiveFormat
    lateinit var need:Need
    var mLastItem:CanSchdule?=null
    var currDate=DateUtils.today()
    lateinit var mAdapter:BaseQuickAdapter<CanSchdule,BaseDataBindingHolder<ItemYuyueBinding>>
    override fun getLayout(): Int {
        return R.layout.activity_calendar
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="预约排期"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        tv_date.text="${DateUtils.getMonth()}月${DateUtils.getDay()}日"

        serviceBean= intent.getSerializableExtra("serviceBean") as ServiceBean
        need= intent.getSerializableExtra("need") as Need
        shopLiveFormat= intent.getSerializableExtra("shopLiveFormat") as ShopLiveFormat
        initCalendar()
        initSelectYuyue()
        intiRv()
    }

    fun getData(){
        RxHttpScope().launch {
            var hadYuyue=RxHttp.postJson("specification/live/alreadyBookedTimeList")
                .add("serviceId",serviceBean.id)
                .add("reservationDate",currDate)
                .toResponse<List<Int>>().await()
            hadYuyue.forEachIndexed { index, i ->
                datas[i-1].isCan=false
            }
            mAdapter.setList(datas)
        }
    }

    private fun initSelectYuyue() {
    }

    @OnClick(R.id.iv_dir)
    fun expand(){
        var lp= calendarView.layoutParams
        if (!calendarLayout.isExpand) {
            calendarLayout.expand()
        }
        if (calendarLayout.isExpand){
            calendarLayout.shrink()
        }
    }

    fun initCalendar(){
        calendarLayout.expand()
        calendarView.scrollToCurrent()
        //calendarView.setSelectStartCalendar()
        calendarView.setOnYearChangeListener(this)
        calendarView.setOnCalendarSelectListener(this)
    }
    fun intiRv(){
        rv_yuyue.layoutManager=LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rv_yuyue.adapter=object :BaseQuickAdapter<CanSchdule,BaseDataBindingHolder<ItemYuyueBinding>>(R.layout.item_yuyue){
            override fun convert(holder: BaseDataBindingHolder<ItemYuyueBinding>, item: CanSchdule) {
                holder.dataBinding!!.item=item
                holder.dataBinding!!.temp1.setOnClickListener {
                    item.isCheck=true
                    if (mLastItem != null) {
                        mLastItem!!.isCheck = false
                    }
                    mLastItem=item
                    notifyDataSetChanged()
                }
            }

        }.also {
            mAdapter=it
            getData()
        }
    }
    override fun onCalendarOutOfRange(calendar: Calendar?) {

    }

    override fun onCalendarSelect(calendar: Calendar, isClick: Boolean) {
        tv_date.text="${calendar.month}月${calendar.day}日"
        currDate= SimpleDateFormat("yyyy-MM-dd").format(calendar.timeInMillis)
        getData()
    }

    override fun onYearChange(year: Int) {
        ToastUtils.showShort("年2:$year")
    }

    @OnClick(R.id.btn_ok)
    fun next(){
        if (mLastItem ==null) {
            ToastUtils.showShort("请选择预约时间")
            return
        }
        shopLiveFormat.reservationDate=currDate
        shopLiveFormat.times=mLastItem!!.desc
        Router.newIntent(this)
            .putSerializable("need",need)
                .putSerializable("shopLiveFormat",shopLiveFormat)
                .putSerializable("serviceBean",serviceBean)
                .to(CommitOrderActivity::class.java).launch()
    }


}