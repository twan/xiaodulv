package com.twan.kotlinbase.ui

import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import butterknife.OnClick
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.OrderDTO
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.RechaegeHistory
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.databinding.ItemRechargeHistoryBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.widgets.router.Router
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//缴纳保证金
class BondActivity : BaseDataBindingActivity<ActivityCopyBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_bond
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="缴纳保证金"
    }

    @OnClick(R.id.btn_ok)
    fun bond(){
        Router.newIntent(this).putSerializable("order",OrderDTO()).to(PayTypeActivity::class.java).launch()
    }

}