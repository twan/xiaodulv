package com.twan.kotlinbase.ui.view;

import com.twan.kotlinbase.bean.OrderDetailBean;

/**
 * @anthor zhoujr
 * @time 2021/4/19 11:07
 * @describe
 */
public interface NewOrderDetailView {
    void getOrderDetailSucc(OrderDetailBean orderDetailBean);

    void cancelSucc();
}
