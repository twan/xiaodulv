package com.twan.kotlinbase.ui;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.SpanUtils;
import com.bumptech.glide.Glide;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.app.EnumOrderStatus;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.bean.OrderDetailBean;
import com.twan.kotlinbase.pop.ConfimSimplePop;
import com.twan.kotlinbase.pop.PayFailPop;
import com.twan.kotlinbase.pop.PressSimplePop;
import com.twan.kotlinbase.pop.SelPayWayPop;
import com.twan.kotlinbase.ui.presenter.NewOrderDetailPresenter;
import com.twan.kotlinbase.ui.view.NewOrderDetailView;
import com.twan.kotlinbase.util.UIUtils;
import com.twan.kotlinbase.widgets.RoundImageView;
import com.twan.kotlinbase.widgets.RoundImageView5;

import butterknife.BindView;
import butterknife.OnClick;
import cn.iwgang.countdownview.CountdownView;

/**
 * @anthor zhoujr
 * @time 2021/4/15 9:28
 * @describe
 */
public class NewOrderDetailActivity extends BaseActivity <NewOrderDetailView, NewOrderDetailPresenter> implements NewOrderDetailView {
    @BindView(R.id.orderDetailLl)
    LinearLayout orderDetailLl;
    @BindView(R.id.orderDetailCountDown)
    CountdownView orderDetailCountDown;
    @BindView(R.id.orderDetailName)
    TextView orderDetailName;
    @BindView(R.id.orderDetailMobile)
    TextView orderDetailMobile;
    @BindView(R.id.orderDetailCopy)
    TextView orderDetailCopy;
    @BindView(R.id.orderDetailServiceIcon)
    RoundImageView orderDetailServiceIcon;
    @BindView(R.id.orderDetailServiceName)
    TextView orderDetailServiceName;
    @BindView(R.id.orderDetailIcon)
    RoundImageView5 orderDetailIcon;
    @BindView(R.id.orderDetailTitle)
    TextView orderDetailTitle;
    @BindView(R.id.orderDetailPrice)
    TextView orderDetailPrice;
    @BindView(R.id.orderDetailDesc)
    TextView orderDetailDesc;
    @BindView(R.id.orderDetailCount)
    TextView orderDetailCount;
    @BindView(R.id.orderDetailRealPay)
    TextView orderDetailRealPay;
    @BindView(R.id.orderDetailOrderNo)
    TextView orderDetailOrderNo;
    @BindView(R.id.orderDetailOrderNoCopy)
    TextView orderDetailOrderNoCopy;
    @BindView(R.id.orderDetailCreateDate)
    TextView orderDetailCreateDate;
    @BindView(R.id.orderDetailContact)
    TextView orderDetailContact;
    @BindView(R.id.orderDetailAddress)
    TextView orderDetailAddress;
    @BindView(R.id.countdownLL)
    LinearLayout countdownLL;
    SelPayWayPop selPayWayPop;
    PayFailPop payFailPop;
    PressSimplePop pressSimplePop;
    ConfimSimplePop confimSimplePop;
    OrderDetailBean orderDetailBean;
    String orderId = "";

    @Override
    public void initView() {
        super.initView();
        status = getIntent().getIntExtra("status",0);
        orderId = getIntent().getStringExtra("orderId");

        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.head_bg));
        confimSimplePop = new ConfimSimplePop(this);
        confimSimplePop.setItemClick(new ConfimSimplePop.ItemClick() {
            @Override
            public void reWrite() {
                pressSimplePop.showPopupWindow();
            }

            @Override
            public void confimSubmit() {

            }
        });
        pressSimplePop = new PressSimplePop(this);
        pressSimplePop.setItemClick(new PressSimplePop.ItemClick() {
            @Override
            public void selServiceCompany() {

            }

            @Override
            public void confimSubmit() {
                confimSimplePop.showPopupWindow();
            }
        });
        payFailPop = new PayFailPop(this);
        payFailPop.setRePay(new PayFailPop.RePay() {
            @Override
            public void ConfimRePay() {
                selPayWayPop.showPopupWindow();
            }
        });
        selPayWayPop = new SelPayWayPop(this);
        selPayWayPop.setConfimPay(new SelPayWayPop.ConfimPay() {
            @Override
            public void ConfimSelPayWay(int position) {
                payFailPop.showPopupWindow();
            }
        });
        mPresenter.getOrderDetail(orderId);
    }

StringBuilder builder = new StringBuilder();
    @OnClick({R.id.orderDetailOrderNoCopy,R.id.orderDetailContact,R.id.orderDetailCopy})
    public void click(View view){
        switch (view.getId()){
            case R.id.orderDetailCopy:
                builder.delete(0,builder.toString().length());
                builder.append(orderDetailName.getText().toString().trim()+"\n"+orderDetailMobile.getText().toString().trim()+"\n"+orderDetailAddress.getText().toString().trim());
                UIUtils.copy(NewOrderDetailActivity.this,builder.toString().trim());
                UIUtils.showToast("已复制");
                break;
            case R.id.orderDetailContact:
                UIUtils.showToast("接口暂无该字段返回");
                break;
            case R.id.orderDetailOrderNoCopy:
                UIUtils.copy(NewOrderDetailActivity.this,orderDetailBean.getResult().getOrderContentDTO().getOrderCode());
                break;
        }
    }


    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case  R.id.btn_cancel:
                    //取消订单
                    mPresenter.cancelOrder(orderId);
                    break;
                case R.id.btn_pay:
                    //立即支付
                    selPayWayPop.showPopupWindow();
                    break;
                case R.id.btn_simple:
                    //寄送样品
                    pressSimplePop.showPopupWindow();
                    break;
                case R.id.orderDetailSH:
                    //审核作品
                    jumpToActivity(AuditActivity.class);
                    break;
                case R.id.orderDetailConfimReceive:
                    //确认收货
                    jumpToActivity(OrderFinishActivity.class);
                    break;
            }
        }
    };

    @Override
    protected NewOrderDetailPresenter createPresenter() {
        return new NewOrderDetailPresenter(this);
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_neworderdetail;
    }

    @Override
    public void getOrderDetailSucc(OrderDetailBean orderDetailBean) {
        this.orderDetailBean = orderDetailBean;
        setOrderData();
        setHeadView();
    }

    private void setOrderData() {
        orderDetailOrderNo.setText(orderDetailBean.getResult().getOrderContentDTO().getOrderCode());
        orderDetailCreateDate.setText(orderDetailBean.getResult().getOrderContentDTO().getCreateTime());
        Glide.with(this).load(orderDetailBean.getResult().getOrderServiceDTO().getAvatarUrl()).into(orderDetailServiceIcon);
        orderDetailServiceName.setText(orderDetailBean.getResult().getOrderServiceDTO().getNickName());
        Glide.with(this).load(orderDetailBean.getResult().getOrderServiceDTO().getImageUrl()).into(orderDetailIcon);
        orderDetailTitle.setText(orderDetailBean.getResult().getOrderServiceDTO().getTitle());
        orderDetailPrice.setText("¥"+orderDetailBean.getResult().getOrderServiceDTO().getPrice());
        String payTitle = "";
        if(status == Integer.parseInt(EnumOrderStatus.PENDING_PAYMENT.getId())){
            payTitle = "实付款";

        }else{
            payTitle = "已付款";
        }
        orderDetailRealPay.setText(new SpanUtils()
                .append(payTitle)
                .setFontSize(14,true)
                .setForegroundColor(Color.parseColor("#333333"))
                .append("￥")
                .setFontSize(12,true)
                .setForegroundColor(Color.parseColor("#FF3600"))
                .append(orderDetailBean.getResult().getOrderServiceDTO().getPrice())
                .setFontSize(14,true)
                .setForegroundColor(Color.parseColor("#FF3600"))
                .create()
        );
    }

    @Override
    public void cancelSucc() {
        UIUtils.showToast("订单已取消");
        finish();
    }

    //设置顶部显示信息
    int status ;
    private void setHeadView() {
        orderDetailLl.removeAllViews();
//        status  = orderDetailBean.getResult().getOrderContentDTO().get
        if(status == Integer.parseInt(EnumOrderStatus.PENDING_PAYMENT.getId())){
            //待付款
            setActivityTitle("待付款");
            View view = LayoutInflater.from(this).inflate(R.layout.view_waitpaybtn,null);
            countdownLL.setVisibility(View.VISIBLE);
            view.findViewById(R.id.btn_cancel).setOnClickListener(onClickListener);
            view.findViewById(R.id.btn_pay).setOnClickListener(onClickListener);
            long time4 = (long)150 * 24 * 60 * 60 * 1000;
            orderDetailCountDown.start(time4);
            orderDetailCountDown.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
                @Override
                public void onEnd(CountdownView cv) {

                }
            });
            orderDetailLl.addView(view);
        }else if(status == Integer.parseInt(EnumOrderStatus.TO_BE_SHIPPED.getId())){
            //待发货
            //寄送样品
            setActivityTitle("寄送样品");
            View view = LayoutInflater.from(this).inflate(R.layout.view_sendsimple,null);
            view.findViewById(R.id.btn_simple).setOnClickListener(onClickListener);
            orderDetailLl.addView(view);
        }else if(status == Integer.parseInt(EnumOrderStatus.TO_BE_EVALUATED.getId())){
            //待评价
            setActivityTitle("待评价");
        }else if(status == Integer.parseInt(EnumOrderStatus.COMPLETED.getId())){
            //已完成
            setActivityTitle("已完成");
        }else if(status == Integer.parseInt(EnumOrderStatus.CANCELED.getId())){
            //已取消
            setActivityTitle("已取消");
        }else if(status == Integer.parseInt(EnumOrderStatus.TO_BE_RECEIVED.getId())){
            //待收货
            setActivityTitle("待收货");
        }else if(status == Integer.parseInt(EnumOrderStatus.IN_THE_PROCESS.getId())){
            //进行中
            //服务中 审核作品/确认收货
            setActivityTitle("服务中");
            View view = LayoutInflater.from(this).inflate(R.layout.view_serviceing,null);
            view.findViewById(R.id.orderDetailSH).setOnClickListener(onClickListener);
            view.findViewById(R.id.orderDetailConfimReceive).setOnClickListener(onClickListener);
            orderDetailLl.addView(view);
        }else if(status == Integer.parseInt(EnumOrderStatus.REFUNDED.getId())){
            //已退款
            setActivityTitle("已退款");
        }else if(status == Integer.parseInt(EnumOrderStatus.TIMEOUT.getId())){
            //订单超时
            setActivityTitle("订单超时");
        }



    }
}
