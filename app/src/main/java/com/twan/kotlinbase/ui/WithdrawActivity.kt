package com.twan.kotlinbase.ui

import android.text.Editable
import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.jaeger.library.StatusBarUtil
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.BankItem
import com.twan.kotlinbase.bean.PersonalAccount
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.SelectBankPopup
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.SimpleTextWatcher
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_withdraw.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//提现
class WithdrawActivity : BaseDataBindingActivity<ActivityCopyBinding>(){
    lateinit var personalAccount: PersonalAccount
    lateinit var mBankItem: BankItem
    override fun getLayout(): Int {
        return R.layout.activity_withdraw
    }

    override fun initEventAndData() {
        whiteToolbar("提现","提现记录")
        edt_withdraw_amt.addTextChangedListener(object : SimpleTextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                var writeAmt = if (p0!!.toString().isEmpty()) 0f else p0!!.toString().toFloat()
                if (writeAmt > personalAccount.accountBalance.toFloat()) {
                    edt_withdraw_amt.removeTextChangedListener(this)
                    edt_withdraw_amt.setText(personalAccount.accountBalance)
                    edt_withdraw_amt.addTextChangedListener(this)
                }
            }
        })
        personalAccount = intent.getSerializableExtra("personalAccount") as PersonalAccount
        tv_amt.text = "可提现余额￥"+personalAccount.accountBalance
    }

    @OnClick(R.id.tv_all)
    fun setAll(){
        edt_withdraw_amt.setText(personalAccount.accountBalance)
    }

    override fun rightClick() {
        Router.newIntent(this).to(WithdrawHistoryActivity::class.java).launch()
    }

    @OnClick(R.id.btn_click)
    fun withdraw(){
        if (!::mBankItem.isInitialized){
            ToastUtils.showShort("请选择银行卡")
            return
        }

        if (edt_withdraw_amt.text.isNullOrEmpty() || edt_withdraw_amt.text.toString().toFloat() ==0f){
            ToastUtils.showShort("请输入提现金额")
            return
        }

        RxHttpScope().launch {
            RxHttp.postJson("account/withdrawMoney")
                .add("balance",edt_withdraw_amt.text.toString())
                .add("bankId",mBankItem.id)
                .add("userId",MyUtils.getUserid())
                .toResponse<Any>().await()
            ToastUtils.showShort("提现成功")
            this@WithdrawActivity.finish()
        }
    }

    @OnClick(R.id.rl_select,R.id.tv_select)
    fun selectBank(){
        XPopup.Builder(mContext).asCustom(SelectBankPopup(this){
            mBankItem = it
            if(mBankItem.accountType =="1"){
                tv_type.text="提现到支付宝"
                tv_select.text="${mBankItem.cardNumber}"
            }
            if(mBankItem.accountType =="2"){
                tv_type.text="提现到银行卡"
                tv_select.text="${mBankItem.cardNumber}"
            }
        }).show()
    }
}