package com.twan.kotlinbase.ui

import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import butterknife.OnClick
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.Discovery
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.RechaegeHistory
import com.twan.kotlinbase.bean.TradeHistory
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.databinding.ActivityRechargeHistoryBinding
import com.twan.kotlinbase.databinding.ItemRechargeHistoryBinding
import com.twan.kotlinbase.databinding.ItemTradeHistoryBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.*
import kotlinx.android.synthetic.main.activity_recharge_history.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class TradeHistoryActivity : BaseDataBindingActivity<ActivityRechargeHistoryBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_recharge_history
    }

    override fun initEventAndData() {
        whiteToolbar("交易记录")
        tv_curr_time.text=DateUtils.today3()
        initRv()
    }

    @OnClick(R.id.ll_time)
    fun timeSelect(){
        SelectDateUtils.initTimePicker(this,tv_curr_time,mode=SelectDateUtils.DateMode.YM){
            getData()
        }.show()
    }

    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(this))
        refreshLayout.setRefreshFooter(ClassicsFooter(this))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData(false) }
        rv_history.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rv_history.adapter=object : BaseQuickAdapter<TradeHistory, BaseDataBindingHolder<ItemTradeHistoryBinding>>(R.layout.item_trade_history){
            override fun convert(holder: BaseDataBindingHolder<ItemTradeHistoryBinding>, item: TradeHistory) {
                if (item.payType=="0"){
                    item.payType="余额支付"
                } else if (item.payType=="1"){
                    item.payType="支付宝支付"
                } else {
                    item.payType="微信支付"
                }
                holder.dataBinding!!.item=item
            }

        }.also {
            mAdpater=it
            getData()
        }
    }

    var currentPage=1
    lateinit var mAdpater:BaseQuickAdapter<TradeHistory, BaseDataBindingHolder<ItemTradeHistoryBinding>>
    fun getData(isRefresh:Boolean=true){
        if (isRefresh) currentPage=1
        RxHttpScope(this,refreshLayout).launch {
            var req=RxHttp.get("log/getAddOrderLog")
                    .add("userId",MyUtils.getUserid())
                    .add("startDate",tv_curr_time.text.toString())
                    .add("pageSize",20)
                    .add("currentPage",currentPage++)
                    .toResponse<PageData<TradeHistory>>().await()
            if (currentPage==2) mAdpater.setList(req.content)
            else  mAdpater.addData(req.content)
        }
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    fun event(event: String){
//
//    }
}