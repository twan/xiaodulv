package com.twan.kotlinbase.ui;

import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.flyco.tablayout.SlidingTabLayout;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.OrderAdapter;
import com.twan.kotlinbase.adapter.ServiceOrderFMAdapter;
import com.twan.kotlinbase.app.EnumOrderStatus;
import com.twan.kotlinbase.app.EnumServiceOrderStatus;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.fragment.NewOrderFragment;
import com.twan.kotlinbase.fragment.ServiceOrderFragment;
import com.twan.kotlinbase.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @anthor zhoujr
 * @time 2021/4/17 14:07
 * 服务商订单列表
 * @describe
 */
public class ServiceOrderActivity extends BaseActivity {
    @BindView(R.id.serviceOrderTab)
    SlidingTabLayout serviceOrderTab;
    @BindView(R.id.serviceOrderVP)
    ViewPager serviceOrderVP;
    ArrayList<ServiceOrderFragment> orderFragments;

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("订单列表");
        setToolBarImgAndBg(R.mipmap.ic_arrow_back_white, getResources().getColor(R.color.head_bg));

        orderFragments = new ArrayList<>();
        orderFragments.add(new ServiceOrderFragment(EnumServiceOrderStatus.ALL));
        orderFragments.add(new ServiceOrderFragment(EnumServiceOrderStatus.PENDING_PAYMENT));
        orderFragments.add(new ServiceOrderFragment(EnumServiceOrderStatus.TO_BE_SENDING));
        orderFragments.add(new ServiceOrderFragment(EnumServiceOrderStatus.TO_BE_SERVICING));
        orderFragments.add(new ServiceOrderFragment(EnumServiceOrderStatus.COMPLETED));
        ServiceOrderFMAdapter orderAdapter = new ServiceOrderFMAdapter(getSupportFragmentManager(),orderFragments);
        serviceOrderVP.setAdapter(orderAdapter);
        serviceOrderTab.setViewPager(serviceOrderVP, Constant.sServiceTabTitles);
        serviceOrderVP.setOffscreenPageLimit(orderFragments.size());
        serviceOrderVP.setCurrentItem(0);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_serviceorder;
    }

}
