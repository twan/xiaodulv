package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.App
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.DresserParam
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.getUrl
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_settle_dresser2.*
import kotlinx.android.synthetic.main.activity_settle_dresser3.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//直播达人 入驻3
class SettleDresserActivity3 : BaseDataBindingActivity<ActivitySettleDresser1Binding>(){
    lateinit var currImageView: ImageView
    lateinit var dresserParam:DresserParam
    override fun getLayout(): Int {
        return R.layout.activity_settle_dresser3
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="个人入驻"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        dresserParam=intent.getSerializableExtra("dresserParam") as DresserParam
        ll_idcard_z.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_z
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_f.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_f
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_handby.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_handby
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
    }

    @OnClick(R.id.btn_ok)
    fun done(){
        if (iv_idcard_z.getUrl().isEmpty()){
            ToastUtils.showShort("请上传正面")
            return
        }
        if (iv_idcard_f.getUrl().isEmpty()){
            ToastUtils.showShort("请上传反面")
            return
        }
        if (iv_idcard_handby.getUrl().isEmpty()){
            ToastUtils.showShort("请上传手持身份证")
            return
        }
        if (!InputUtils.checkEmpty(edt_name,edt_idcard)){
            return
        }
        dresserParam.actualName=edt_name.text.toString()
        dresserParam.cardId=edt_idcard.text.toString()
        dresserParam.frontUrl=iv_idcard_z.getUrl()
        dresserParam.backUrl=iv_idcard_f.getUrl()
        dresserParam.handHeldUrl=iv_idcard_handby.getUrl()

        RxHttpScope().launch {
            RxHttp.postJson("settle/shopLive/save")
                    .add("actualName",dresserParam.actualName)
                    .add("area",dresserParam.area)
                    .add("avatarUlr",dresserParam.avatarUlr)
                    .add("backUrl",dresserParam.backUrl)
                    .add("cardId",dresserParam.cardId)
                    .add("caseUrl",dresserParam.caseUrl)
                    .add("frontUrl",dresserParam.frontUrl)
                    .add("goodAtLabel",dresserParam.goodAtLabel)
                    .add("goodAtValue",dresserParam.goodAtValue)
                    .add("handHeldUrl",dresserParam.handHeldUrl)
                    //.add("id",)
                    .add("imageUrl",dresserParam.imageUrl)
                    .add("nickName",dresserParam.nickName)
                    .add("selfIntroduction",dresserParam.selfIntroduction)
                    .add("status",dresserParam.status)
                    .add("tagLabel",dresserParam.tagLabel)
                    .add("tagValue",dresserParam.tagValue)
                    .add("userId",MyUtils.getUserid())
                    .add("videoUrl",dresserParam.videoUrl)
                    .toResponse<Any>().await()

            ToastUtils.showShort("提交成功,请等待审核通过")
            App.removeActivity(SettleDresserActivity1::class.java)
            App.removeActivity(SettleDresserActivity2::class.java)
            this@SettleDresserActivity3.finish()
        }


    }

    override fun uploadCallback(url:String){
        if (uploadType == 3){
            if (::currImageView.isInitialized){
                currImageView.tag=url
                GlideUtils.load(this,currImageView,url)
            }
        }
    }
}