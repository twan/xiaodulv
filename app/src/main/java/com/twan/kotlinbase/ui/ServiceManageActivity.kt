package com.twan.kotlinbase.ui

import android.view.View
import androidx.fragment.app.Fragment
import com.blankj.utilcode.util.ToastUtils
import com.google.android.material.tabs.TabLayout.MODE_FIXED
import com.google.android.material.tabs.TabLayoutMediator
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.BottomViewPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.app.EnumSaleStatus
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.fragment.ServiceManageFragment
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.activity_order.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//服务管理
class ServiceManageActivity : BaseDataBindingActivity<ActivityCopyBinding>(){
    val tabs = mutableListOf("出售中","已下架")//"待发布",
    override fun getLayout(): Int {
        return R.layout.activity_order
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="服务管理"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text="全部删除"

        setupViewPager()
        tablayout.tabMode=MODE_FIXED
        initTab()
    }

    override fun rightClick() {
        RxHttpScope().launch {
            RxHttp.get("service/multiple/deleteAll").add("userId",MyUtils.getUserid()).toResponse<Any>().await()
            ToastUtils.showShort("删除成功")
        }
    }
    private fun setupViewPager() {
        //view_pager.offscreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT//禁用预加载
        var fragments= mutableListOf<Fragment>()
        //fragments.add(ServiceManageFragment(EnumSaleStatus.SALE_PENDING))
        fragments.add(ServiceManageFragment(EnumSaleStatus.SALE_UP))
        fragments.add(ServiceManageFragment(EnumSaleStatus.SALE_DOWN))
        var adapter = BottomViewPagerAdapter(this,fragments)
        view_pager.adapter = adapter
    }

    fun initTab(){
        TabLayoutMediator(tablayout,view_pager) { tab, position ->
            tab.text=tabs[position]
        }.attach()
        //默认选中
        //tablayout.getTabAt(0)!!.select()
        //view_pager.currentItem = 0
    }
}