package com.twan.kotlinbase.ui

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import butterknife.OnClick
import com.blankj.utilcode.util.JsonUtils
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.AttrValueDTO
import com.twan.kotlinbase.bean.ChidlGuige
import com.twan.kotlinbase.bean.GuigeProperty
import com.twan.kotlinbase.bean.ServiceAttrValueDTO
import com.twan.kotlinbase.databinding.ActivityGuigeBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.JsonUtil
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_guige.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class GuigeActivity : BaseDataBindingActivity<ActivityGuigeBinding>(){
    var viewShotList= mutableListOf<View>()
    var viewTimeList= mutableListOf<View>()
    var viewActorList= mutableListOf<View>()
    var serviceId=""
    lateinit var mShotGuige:GuigeProperty//拍摄方式
    lateinit var mTimeGuige:GuigeProperty//时长
    lateinit var mActorGuige:GuigeProperty//是否需要演员
    var serviceAttrValueDTOS= mutableListOf<ServiceAttrValueDTO>()
    override fun getLayout(): Int {
        return R.layout.activity_guige
    }

    override fun initEventAndData() {
        whiteToolbar("规格","")
        serviceId=intent.getStringExtra("serviceId")
        getGuigeProperty()
    }



    @OnClick(R.id.tv_add_shot)
    fun addShotGuige(){
        addView(viewShotList,ll_shot)
    }

    @OnClick(R.id.tv_add_shichang)
    fun addTimeGuige(){
        addView(viewTimeList,ll_time)
    }

    @OnClick(R.id.tv_add_actor)
    fun addActorGuige(){
        addView(viewActorList,ll_actor)
    }

    fun addView(viewList:MutableList<View>,container:LinearLayout):View{
        var view = layoutInflater.inflate(R.layout.item_guige,ll_actor,false)
        var iv_del = view.findViewById<ImageView>(R.id.iv_del)
        var edt_desc = view.findViewById<EditText>(R.id.edt_desc)
        var v_line = view.findViewById<View>(R.id.v_line)
        viewList.add(view)
        container.addView(view)
        iv_del.setOnClickListener {
            viewList.remove(view)
            container.removeView(view)
        }
        return view
    }

    @OnClick(R.id.btn_ok)
    fun next(){
        RxHttpScope().launch {
            serviceAttrValueDTOS.clear()
            //拍摄
            var attrValueDTOS= mutableListOf<AttrValueDTO>()
            viewShotList.forEachIndexed { index, view ->
                var edt_desc = view.findViewById<EditText>(R.id.edt_desc)
                if (edt_desc.text.toString().isEmpty()){
                    edt_desc.requestFocus()
                    edt_desc.error = edt_desc.hint
                    return@launch
                }
                attrValueDTOS.add(AttrValueDTO(edt_desc.text.toString(),"拍摄方式$index"))
            }
            if (attrValueDTOS.size==0){
                ToastUtils.showShort("请至少添加一个拍摄方式")
                return@launch
            }
            serviceAttrValueDTOS.add(ServiceAttrValueDTO(mShotGuige.id,mShotGuige.attrName,attrValueDTOS,serviceId))
            //时长
            var attrValueDTOS2= mutableListOf<AttrValueDTO>()
            viewTimeList.forEachIndexed { index, view ->
                var edt_desc = view.findViewById<EditText>(R.id.edt_desc)
                if (edt_desc.text.toString().isEmpty()){
                    edt_desc.requestFocus()
                    edt_desc.error = edt_desc.hint
                    return@launch
                }
                attrValueDTOS2.add(AttrValueDTO(edt_desc.text.toString(),"时长$index"))
            }
            if (attrValueDTOS2.size==0){
                ToastUtils.showShort("请至少添加一个时长")
                return@launch
            }
            serviceAttrValueDTOS.add(ServiceAttrValueDTO(mTimeGuige.id,mTimeGuige.attrName,attrValueDTOS2,serviceId))
            //是否需要演员
            var attrValueDTOS3= mutableListOf<AttrValueDTO>()
            viewActorList.forEachIndexed { index, view ->
                var edt_desc = view.findViewById<EditText>(R.id.edt_desc)
                if (edt_desc.text.toString().isEmpty()){
                    edt_desc.requestFocus()
                    edt_desc.error = edt_desc.hint
                    return@launch
                }
                attrValueDTOS3.add(AttrValueDTO(edt_desc.text.toString(),"是否需要演员$index"))
            }
            if (attrValueDTOS3.size==0){
                ToastUtils.showShort("请至少添加一个演员选项")
                return@launch
            }
            serviceAttrValueDTOS.add(ServiceAttrValueDTO(mActorGuige.id,mActorGuige.attrName,attrValueDTOS3,serviceId))

            RxHttp.postJsonArray("attrValue/saveAll").addAll(JsonUtil.objectToJson(serviceAttrValueDTOS)).toResponse<Any>().await()
            //ToastUtils.showShort("保存成功")
            Router.newIntent(this@GuigeActivity).putString("serviceId",serviceId).to(GuigeDetailActivity::class.java).launch()
        }
    }

    //先获取3个属性的id
    fun getGuigeProperty(){
        viewShotList.clear()
        viewTimeList.clear()
        viewActorList.clear()
        RxHttpScope().launch {
            var req=RxHttp.get("attrKey/findAll").toResponse<List<GuigeProperty>>().await()
            req.forEachIndexed { index ,parent->
                if (parent.attrName =="拍摄方式"){mShotGuige=parent}
                if (parent.attrName =="时长"){mTimeGuige=parent}
                if (parent.attrName =="是否需要演员"){mActorGuige=parent}

                //再根据属性id获取列表, (如果有)
                var childs= RxHttp.get("attrValue/findAllByServiceIdAndAttrKeyId").add("attrKeyId",parent.id).add("serviceId",serviceId).toResponse<List<ChidlGuige>>().await()
                childs.forEach { child ->
                    if (parent.attrName =="拍摄方式"){
                        var view = addView(viewShotList,ll_shot)
                        var edt_desc = view.findViewById<EditText>(R.id.edt_desc)
                        edt_desc.setText(child.attrLabel)
                    } else if (parent.attrName =="时长"){
                        var view = addView(viewTimeList,ll_time)
                        var edt_desc = view.findViewById<EditText>(R.id.edt_desc)
                        edt_desc.setText(child.attrLabel)
                    } else{
                        var view = addView(viewActorList,ll_actor)
                        var edt_desc = view.findViewById<EditText>(R.id.edt_desc)
                        edt_desc.setText(child.attrLabel)
                    }
                }
            }
        }
    }
}