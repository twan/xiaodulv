package com.twan.kotlinbase.ui

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.BottomViewPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.app.EnumOrderStatus
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.fragment.OrderAllFragment
import com.twan.kotlinbase.fragment.OrderWillPayFragment
import kotlinx.android.synthetic.main.activity_order.*


class OrderActivity : BaseDataBindingActivity<ActivityCopyBinding>(){
    val tabs = mutableListOf("全部","待付款","待发货","待收货","进行中","待评价","已完成","已取消","退款")
    override fun getLayout(): Int {
        return R.layout.activity_order
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="我的订单"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        setupViewPager()
        var index=intent.getIntExtra("index",0)
        initTab(index)
    }

    private fun setupViewPager() {
        //view_pager.offscreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT//禁用预加载
        var fragments= mutableListOf<Fragment>()
        fragments.add(OrderAllFragment(EnumOrderStatus.ALL))
        fragments.add(OrderAllFragment(EnumOrderStatus.PENDING_PAYMENT))
        fragments.add(OrderAllFragment(EnumOrderStatus.TO_BE_SHIPPED))
        fragments.add(OrderAllFragment(EnumOrderStatus.TO_BE_RECEIVED))
        fragments.add(OrderAllFragment(EnumOrderStatus.IN_THE_PROCESS))
        fragments.add(OrderAllFragment(EnumOrderStatus.TO_BE_EVALUATED))
        fragments.add(OrderAllFragment(EnumOrderStatus.COMPLETED))
        fragments.add(OrderAllFragment(EnumOrderStatus.CANCELED))
        fragments.add(OrderAllFragment(EnumOrderStatus.REFUNDED))
        //fragments.add(OrderAllFragment(EnumOrderStatus.CLOSED))
        var adapter = BottomViewPagerAdapter(this,fragments)
        view_pager.adapter = adapter
    }

    fun initTab(index:Int){
        TabLayoutMediator(tablayout,view_pager) { tab, position ->
            tab.text=tabs[position]
        }.attach()
        //默认选中
        tablayout.getTabAt(index)!!.select()
        view_pager.currentItem = index
    }
}