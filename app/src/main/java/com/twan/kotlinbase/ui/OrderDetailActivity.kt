package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.OrderContentDTO
import com.twan.kotlinbase.bean.OrderDTO
import com.twan.kotlinbase.bean.OrderDetail
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_order_detail.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class OrderDetailActivity : BaseDataBindingActivity<ActivityOrderDetailBinding>(){
    lateinit var orderDto:OrderDTO
    lateinit var orderDetail: OrderDetail
    override fun getLayout(): Int {
        return R.layout.activity_order_detail
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="订单详情"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        orderDto=intent.getSerializableExtra("data") as OrderDTO
        mBinding.isOperate=false //是否在运营中
        getData()
    }

    @OnClick(R.id.tv_copy)
    fun copy(){
        if (::orderDetail.isInitialized) {
            MyUtils.copy(this, orderDetail.orderContentDTO.orderCode)
            ToastUtils.showShort("已复制")
        }
    }

    @OnClick(R.id.btn_demo)
    fun demo(){
        Router.newIntent(this).to(SendDemoActivity::class.java).launch()
    }

    fun getData(){
        RxHttpScope().launch {
            orderDetail= RxHttp.get("order/findDetailByOrderId").add("orderId",orderDto.id).toResponse<OrderDetail>().await()
            mBinding.item=orderDetail
            GlideUtils.loadCicle(this@OrderDetailActivity,iv_head,orderDetail.orderServiceDTO.avatarUrl)
            GlideUtils.loadCicle(this@OrderDetailActivity,iv_pic,orderDetail.orderServiceDTO.imageUrl)
        }
    }

}