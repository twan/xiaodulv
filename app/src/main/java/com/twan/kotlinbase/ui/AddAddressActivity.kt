package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityAddAddressBinding
import com.twan.kotlinbase.databinding.ActivityAddZfbOrYhkBinding
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.activity_add_zfb_or_yhk.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class AddAddressActivity : BaseDataBindingActivity<ActivityAddAddressBinding>(){
    override fun getLayout(): Int {
        return R.layout.activity_add_address
    }

    override fun initEventAndData() {
        whiteToolbar("地址管理")
    }

    @OnClick(R.id.btn_click)
    fun add(){
        RxHttpScope().launch {
            RxHttp.postJson("")
                    .add("accountType", "1") //1支付宝，2银行卡
                    .add("cardNumber", edt_zfb_account.text.toString())
                    .add("realName", edt_zfb_name.text.toString())
                    .add("userId", MyUtils.getUserid())
                    .toResponse<Any>().await()
            ToastUtils.showShort("支付宝添加成功")
            this@AddAddressActivity.finish()
        }
    }


}