package com.twan.kotlinbase.ui

import android.util.Log
import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.SPUtils
import com.lxj.xpopup.XPopup
import com.tencent.qcloud.tim.uikit.TUIKit
import com.tencent.qcloud.tim.uikit.base.IUIKitCallBack
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.SetDefaultBean
import com.twan.kotlinbase.databinding.ActivitySettingBinding
import com.twan.kotlinbase.pop.MyCenterPopup
import com.twan.kotlinbase.util.CacheDataManager
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_setting.*
import org.greenrobot.eventbus.EventBus


class SettingActivity : BaseDataBindingActivity<ActivitySettingBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_setting
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="设置"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        tv_cache.text= "${CacheDataManager.getTotalCacheSize(this)}"
        tv_logout.visibility=if (MyUtils.isLogin()) View.VISIBLE else View.GONE
    }

    @OnClick(R.id.tv_modify_password)
    fun modifyPassword(){
        Router.newIntent(this).to(ModifyPasswordActivity::class.java).launch()
    }

    @OnClick(R.id.tv_suggest)
    fun suggest(){
        Router.newIntent(this).to(SuggestActivity::class.java).launch()
    }

    @OnClick(R.id.tv_about)
    fun about(){
        Router.newIntent(this).to(AboutActivity::class.java).launch()
    }

    @OnClick(R.id.tv_msg_noti)
    fun msgNoti(){
        Router.newIntent(this).to(MessageNotiActivity::class.java).launch()
    }

    @OnClick(R.id.tv_privacy)
    fun privacy(){
        Router.newIntent(mContext)
            .putString("url","https://api.xiaodulv6.com/protocol/p2")
            .putString("title","隐私政策")
            .to(WebActivity::class.java).launch()
        //Router.newIntent(this).to(PrivacyActivity::class.java).launch()
    }

    @OnClick(R.id.rl_clear_cache)
    fun clear(){
        CacheDataManager.clearAllCache(this)
        tv_cache.text= "${CacheDataManager.getTotalCacheSize(this)}"
    }
    @OnClick(R.id.tv_logout)
    fun logout(){
        XPopup.Builder(this).asCustom(MyCenterPopup(this,"确定要退出吗?"){
            SPUtils.getInstance().remove("token")
            SPUtils.getInstance().remove("personinfo")
            EventBus.getDefault().post(SetDefaultBean())
            TUIKit.logout(object :IUIKitCallBack{
                override fun onSuccess(data: Any?) {
                    Router.newIntent(this@SettingActivity).to(MainActivity::class.java).launch()
                }

                override fun onError(module: String?, errCode: Int, errMsg: String?) {
                    Router.newIntent(this@SettingActivity).to(MainActivity::class.java).launch()
                }

            })
        }).show()
    }
}