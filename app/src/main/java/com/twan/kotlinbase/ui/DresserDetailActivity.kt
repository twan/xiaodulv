package com.twan.kotlinbase.ui

import android.view.View
import androidx.fragment.app.Fragment
import com.blankj.utilcode.util.ToastUtils
import com.google.android.material.tabs.TabLayoutMediator
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.BottomViewPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.DresserAnli
import com.twan.kotlinbase.bean.DresserComment
import com.twan.kotlinbase.bean.DresserDetail
import com.twan.kotlinbase.bean.DresserService
import com.twan.kotlinbase.databinding.ActivityDresserDetailBinding
import com.twan.kotlinbase.fragment.DresserDetailAnliFragment
import com.twan.kotlinbase.fragment.DresserDetailEvaluteFragment
import com.twan.kotlinbase.fragment.DresserDetailServiceFragment
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.activity_dresser_detail.*
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.activity_order.tablayout
import kotlinx.android.synthetic.main.activity_order.view_pager
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//达人详情
class DresserDetailActivity : BaseDataBindingActivity<ActivityDresserDetailBinding>(){
    val tabs = mutableListOf("案例","服务")//,"评价")
    var dresserDetail:DresserDetail?=null
    var talentId=""
    var userId = ""
    var nickName = ""
    override fun getLayout(): Int {
        return R.layout.activity_dresser_detail
    }

    override fun initEventAndData() {
        title?.visibility=View.VISIBLE
        title.text = "达人名称"
        back?.visibility=View.VISIBLE
        searchView?.visibility=View.GONE
        tv_back_text?.visibility=View.GONE
        tv_right?.visibility=View.GONE
        tv_right?.text="+关注"
        tv_right?.setTextColor(resources.getColor(R.color.text_black))
        tv_right?.setBackgroundResource(R.drawable.spi_corner_solid_black)

        talentId=intent.getStringExtra("id")
        userId = intent.getStringExtra("userId");
        nickName = intent.getStringExtra("nickName");
        setupViewPager()
        initTab()
        getData()
    }

    private fun setupViewPager() {
        //view_pager.offscreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT//禁用预加载
        var fragments= mutableListOf<Fragment>()
        fragments.add(DresserDetailAnliFragment(userId))
        fragments.add(DresserDetailServiceFragment(talentId,userId,nickName))
        //fragments.add(DresserDetailEvaluteFragment())
        var adapter = BottomViewPagerAdapter(this,fragments)
        view_pager.adapter = adapter
    }

    fun initTab(){
        TabLayoutMediator(tablayout,view_pager) { tab, position ->
            tab.text=tabs[position]
        }.attach()
        //默认选中
        //tablayout.getTabAt(0)!!.select()
        //view_pager.currentItem = 0
    }

    fun getData(){
        RxHttpScope().launch {
            var req=RxHttp.get("talent/detail").add("talentId",talentId).toResponse<DresserDetail>().await()
            dresserDetail=req
            GlideUtils.loadCicle(this@DresserDetailActivity,iv_avater,req.avatarUrl)
            mBinding.item=req
            title.text = req.nickName
        }

        //计算案例数量
        RxHttpScope().launch {
            var req = RxHttp.get("talent/findAllCaseByUserId").add("userId", userId).toResponse<DresserAnli>().await()
            var anliTotal = req.imageUrls.size+req.videoUrls.size
            tv_anli_total.text= anliTotal.toString()
        }
        //计算服务数量
        RxHttpScope().launch {
            var req = RxHttp.get("talent/findAllServiceByUserId").add("userId",userId).toResponse<List<DresserService>>().await()
            tv_service_total.text= req.size.toString()
        }
        //计算评价数量
        //RxHttpScope().launch {
        //    var req= RxHttp.get("appraise/findAllAppraise").add("businessId", MyUtils.getUserid()).toResponse<List<DresserComment>>().await()
        //    var anliTotal = req.size
        //    tv_comment_total.text= anliTotal.toString()
        //}
    }

    //关注
    override fun rightClick() {
        if (!tv_right.text.contains("已关注")) {
            RxHttpScope().launch {
                RxHttp.postJson("talent/follow")
                        //.add("id","")
                        .add("talentId", talentId)
                        .add("userId", MyUtils.getUserid()).toResponse<Any>().await()
                tv_right.text = "已关注"
                ToastUtils.showShort("关注成功")
            }
        } else {
            RxHttpScope().launch {
                RxHttp.get("talent/unFollow")
                        .add("talentId", talentId)
                        .add("userId", MyUtils.getUserid()).toResponse<Any>().await()
                tv_right.text = "+关注"
                ToastUtils.showShort("已取消关注")
            }
        }
    }
}