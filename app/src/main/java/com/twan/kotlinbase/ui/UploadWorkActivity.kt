package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.ActivitySendDynamicBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.*
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.activity_upload_work.*
import kotlinx.android.synthetic.main.activity_upload_work.rv_multi_pic
import kotlinx.android.synthetic.main.viewstub2_zhibo.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//上传作品,上传营业额
class UploadWorkActivity : BaseDataBindingActivity<ActivitySendDynamicBinding>(){
    lateinit var currImageView: ImageView
    override fun getLayout(): Int {
        return R.layout.activity_upload_work
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="上传作品"

        initRvMultiPicOrVideo(rv_multi_pic,1)
    }

    @OnClick(R.id.btn_ok)
    fun upload(){
        var ischeck=InputUtils.checkEmpty(edt_content)
        if (!ischeck) return

        RxHttpScope().launch {
            var req=RxHttp.postJson("talent/works/save")
                .add("imagesUrl",getPicsUrl())
                .add("userId",MyUtils.getUserid())
                .toResponse<Any>().await()
            ToastUtils.showShort("作品上传成功")
            this@UploadWorkActivity.finish()
        }
    }

    override fun uploadCallback(url:String){
        if (uploadType == 1){
            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        }
    }
}