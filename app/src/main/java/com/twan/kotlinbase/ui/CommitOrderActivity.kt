package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.*
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.event.SelectCouponEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.JsonUtil
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_commit_order.*
import org.greenrobot.eventbus.Subscribe
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class CommitOrderActivity : BaseDataBindingActivity<ActivityCopyBinding>(){
    lateinit var serviceBean: ServiceBean
    lateinit var shopLiveFormat:ShopLiveFormat
    lateinit var need: Need
    lateinit var shortVideoPromoteFormat: ShortVideoPromoteFormat
    lateinit var shortVideoMakeFormat: ShortVideoMakeFormat
    var buyNumber=1
    var orderBaseDTO= OrderBaseDTO()
    override fun getLayout(): Int {
        return R.layout.activity_commit_order
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="确认订单"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        need= intent.getSerializableExtra("need") as Need
        serviceBean= intent.getSerializableExtra("serviceBean") as ServiceBean
        showUI()
    }

    fun showUI(){
        tv_name.text=serviceBean.studioName
        tv_do.text="【${need.title}】${serviceBean.title}"
        tv_price.text="¥"+serviceBean.totalAmount
    }

    @Subscribe
    fun selectCoupon(event:SelectCouponEvent){
        tv_coupon.text=event.param.couponName
        orderBaseDTO.couponIds=event.param.id
    }

    @OnClick(R.id.rl_coupon)
    fun coupon(){
        Router.newIntent(this).to(CouponActivity::class.java).launch()
    }

    @OnClick(R.id.btn_ok)
    fun next(){
        if (!InputUtils.checkEmpty(edt_mobile)) return
        orderBaseDTO.buyerPhone=edt_mobile.text.toString()
        orderBaseDTO.remarks=edt_remark.text.toString()
        orderBaseDTO.paymentAmount=serviceBean.totalAmount
        orderBaseDTO.paymentType="1" //0到付，1是支付宝，2是微信
        orderBaseDTO.serviceId=serviceBean.id
        orderBaseDTO.title=serviceBean.title
        orderBaseDTO.userId=MyUtils.getUserid()!!

        when (need) {
            Need.SHOP_LIVE -> {
                //店铺直播
                shopLiveFormat= intent.getSerializableExtra("shopLiveFormat") as ShopLiveFormat
                var orderDTO = CommitShopLiveOrder(orderBaseDTO,shopLiveFormat)
                RxHttpScope().launch {
                    var req=RxHttp.postJson("order/saveOrderShopLive").addAll(JsonUtil.objectToJson(orderDTO)).toResponse<OrderDTO>().await()
                    Router.newIntent(this@CommitOrderActivity).putSerializable("order",req).to(PayTypeActivity::class.java).launch()
                }
            }
            Need.ACTOR_MODEL -> {
                //演员模特
                buyNumber= intent.getIntExtra("buyNumber",1)
                var orderDTO = CommitActorModelOrder(orderBaseDTO,ModelAndActorFormat(buyNumber))
                RxHttpScope().launch {
                    var req=RxHttp.postJson("order/saveOrderModelOrActor").addAll(JsonUtil.objectToJson(orderDTO)).toResponse<OrderDTO>().await()
                    Router.newIntent(this@CommitOrderActivity).putSerializable("order",req).to(PayTypeActivity::class.java).launch()
                }
            }
            Need.SHORT_VIDEO_PROMOTE->{
                //短视频推广
                shortVideoPromoteFormat= intent.getSerializableExtra("shortVideoPromoteFormat") as ShortVideoPromoteFormat
                var orderDTO = CommitVideoExtOrder(orderBaseDTO,shortVideoPromoteFormat)
                RxHttpScope().launch {
                    var req=RxHttp.postJson("order/saveOrderShortVideoPromote").addAll(JsonUtil.objectToJson(orderDTO)).toResponse<OrderDTO>().await()
                    Router.newIntent(this@CommitOrderActivity).putSerializable("order",req).to(PayTypeActivity::class.java).launch()
                }
            }
            Need.SHORT_VIDEO_MAKE->{
                //短视频制作
                shortVideoMakeFormat= intent.getSerializableExtra("shortVideoMakeFormat") as ShortVideoMakeFormat
                var orderDTO = CommitVideoMakeOrder(orderBaseDTO,shortVideoMakeFormat)
                RxHttpScope().launch {
                    var req=RxHttp.postJson("order/saveOrderShortVideoMake").addAll(JsonUtil.objectToJson(orderDTO)).toResponse<OrderDTO>().await()
                    Router.newIntent(this@CommitOrderActivity).putSerializable("order",req).to(PayTypeActivity::class.java).launch()
                }
            }
        }


    }
}