package com.twan.kotlinbase.ui

import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.BottomViewPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivitySuggestBinding
import com.twan.kotlinbase.fragment.Suggest1Fragment
import com.twan.kotlinbase.fragment.Suggest2Fragment
import kotlinx.android.synthetic.main.activity_order.*


class SuggestActivity : BaseDataBindingActivity<ActivitySuggestBinding>(){
    val tabs = mutableListOf("反馈问题","我的反馈")
    override fun getLayout(): Int {
        return R.layout.activity_suggest
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="意见反馈"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        setupViewPager()
        initTab()
    }

    private fun setupViewPager() {
        //view_pager.offscreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT//禁用预加载
        var fragments= mutableListOf<Fragment>()
        fragments.add(Suggest1Fragment())
        fragments.add(Suggest2Fragment())
        var adapter = BottomViewPagerAdapter(this,fragments)
        view_pager.adapter = adapter
    }

    fun initTab(){
        TabLayoutMediator(tablayout,view_pager) { tab, position ->
            tab.text=tabs[position]
        }.attach()
        //默认选中
        //tablayout.getTabAt(0)!!.select()
        //view_pager.currentItem = 0
    }


}