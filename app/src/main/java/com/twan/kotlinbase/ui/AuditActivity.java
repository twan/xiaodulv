package com.twan.kotlinbase.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.luck.picture.lib.PictureSelector;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.AuditedAdapter;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.widgets.RecycleViewDivider;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @anthor zhoujr
 * @time 2021/4/17 9:22
 * 审核作品
 * @describe
 */
public class AuditActivity extends BaseActivity {
    @BindView(R.id.auditLeaveMsg)
    TextView auditLeaveMsg;
    @BindView(R.id.auditRecy)
    RecyclerView auditRecy;
    @BindView(R.id.auditConfimBtn)
    Button auditConfimBtn;
    AuditedAdapter auditedAdapter;

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("作品审核");
        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.head_bg));
        auditedAdapter = new AuditedAdapter();
        auditRecy.setLayoutManager(new GridLayoutManager(this,3));
        auditRecy.setAdapter(auditedAdapter);
        ArrayList<String> data = new ArrayList<>();
        for(int i = 0;i <9;i++){
            data.add("I");
        }
        auditedAdapter.setNewData(data);
        auditedAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                bundle.putInt("position",position);
                jumpToActivityForBundle(PreviewProductActivity.class,bundle);
            }
        });

    }


    @OnClick({R.id.auditConfimBtn})
    public void Click(View view){
        switch (view.getId()){
            case R.id.auditConfimBtn:
                jumpToActivity(OrderFinishActivity.class);
                break;
        }
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_audit;
    }
}
