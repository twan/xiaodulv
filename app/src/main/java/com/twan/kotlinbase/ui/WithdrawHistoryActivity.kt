package com.twan.kotlinbase.ui

import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import butterknife.OnClick
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.Discovery
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.RechaegeHistory
import com.twan.kotlinbase.bean.WithdrawHistory
import com.twan.kotlinbase.databinding.*
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.*
import kotlinx.android.synthetic.main.activity_single_rv.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class WithdrawHistoryActivity : BaseDataBindingActivity<ActivitySingleRvBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_single_rv
    }

    override fun initEventAndData() {
        whiteToolbar("提现记录")
        initRv()
    }

    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(this))
        refreshLayout.setRefreshFooter(ClassicsFooter(this))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData(false) }
        //single_rv.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        single_rv.adapter=object : BaseQuickAdapter<WithdrawHistory, BaseDataBindingHolder<ItemWithdrawHistoryBinding>>(R.layout.item_withdraw_history){
            override fun convert(holder: BaseDataBindingHolder<ItemWithdrawHistoryBinding>, item: WithdrawHistory) {
                if(item.status=="1") {
                    item.statusDesc="提现成功"
                } else if (item.status=="0"){
                    item.statusDesc="提现中"
                } else {
                    item.statusDesc="提现失败"
                }
                holder.dataBinding!!.item=item
            }

        }.also {
            mAdpater=it
            getData()
        }
    }

    var currentPage=1
    lateinit var mAdpater:BaseQuickAdapter<WithdrawHistory, BaseDataBindingHolder<ItemWithdrawHistoryBinding>>
    fun getData(isRefresh:Boolean=true){
        if (isRefresh) currentPage=1
        RxHttpScope(this,refreshLayout).launch {
            var req=RxHttp.get("log/getWithDrawLog")
                    .add("userId",MyUtils.getUserid())
                    .add("pageSize",20)
                    .add("currentPage",currentPage++)
                    .toResponse<PageData<WithdrawHistory>>().await()
            if (currentPage==2) mAdpater.setList(req.content)
            else  mAdpater.addData(req.content)
        }
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    fun event(event: String){
//
//    }
}