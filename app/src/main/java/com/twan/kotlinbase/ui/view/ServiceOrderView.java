package com.twan.kotlinbase.ui.view;

import com.twan.kotlinbase.bean.OrderBean;

/**
 * @anthor zhoujr
 * @time 2021/4/19 9:23
 * @describe
 */
public interface ServiceOrderView {

    void  getServiceOrderLstSucc(OrderBean orderBean);

    void confimReceivedSimpleSucc();
}
