package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.PersonalAccount
import com.twan.kotlinbase.databinding.ActivityWalletBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class WalletActivity : BaseDataBindingActivity<ActivityWalletBinding>(){
    lateinit var personalAccount:PersonalAccount
    override fun getLayout(): Int {
        return R.layout.activity_wallet
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="我的钱包"
        tv_right?.visibility = View.GONE
        getData()
    }

    fun getData(){
        RxHttpScope().launch {
            var req=RxHttp.get("account/personalAccount").add("userId",MyUtils.getUserid()).toResponse<PersonalAccount>().await()
            personalAccount=req
            mBinding.item=req
        }
    }

    @OnClick(R.id.tv_trade_history)
    fun tradeHistory(){
        Router.newIntent(this).to(TradeHistoryActivity::class.java).launch()
    }
    @OnClick(R.id.tv_recharge)
    fun recharge(){
        Router.newIntent(this).to(RechargeActivity::class.java).launch()
    }
    @OnClick(R.id.tv_withdraw)
    fun withdraw(){
        Router.newIntent(this).putSerializable("personalAccount",personalAccount).to(WithdrawActivity::class.java).launch()
    }
    @OnClick(R.id.tv_account)
    fun account(){
        Router.newIntent(this).to(AccountManageActivity::class.java).launch()

    }

}