package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.App
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.ActorModelParam
import com.twan.kotlinbase.bean.DresserParam
import com.twan.kotlinbase.bean.ShortVideoExtParam
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.getUrl
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_settle_dresser2.*
import kotlinx.android.synthetic.main.activity_settle_dresser3.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//演员模特 入驻3
class SettleActorModelActivity3 : BaseDataBindingActivity<ActivitySettleDresser1Binding>(){
    lateinit var currImageView: ImageView
    lateinit var actorModelParam: ActorModelParam
    override fun getLayout(): Int {
        return R.layout.activity_settle_dresser3
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="演员模特入驻认证"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        tv_desc.text="演员模特入驻流程"

        actorModelParam=intent.getSerializableExtra("actorModelParam") as ActorModelParam
        ll_idcard_z.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_z
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_f.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_f
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_handby.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_handby
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
    }

    @OnClick(R.id.btn_ok)
    fun done(){
        if (iv_idcard_z.getUrl().isEmpty()){
            ToastUtils.showShort("请上传正面")
            return
        }
        if (iv_idcard_f.getUrl().isEmpty()){
            ToastUtils.showShort("请上传反面")
            return
        }
        if (iv_idcard_handby.getUrl().isEmpty()){
            ToastUtils.showShort("请上传手持身份证")
            return
        }
        if (!InputUtils.checkEmpty(edt_name,edt_idcard)){
            return
        }
        actorModelParam.actualName=edt_name.text.toString()
        actorModelParam.cardId=edt_idcard.text.toString()
        actorModelParam.frontUrl=iv_idcard_z.getUrl()
        actorModelParam.backUrl=iv_idcard_f.getUrl()
        actorModelParam.handHeldUrl=iv_idcard_handby.getUrl()

        RxHttpScope().launch {
            RxHttp.postJson("settle/shopLive/save")
                .add("acceptAreaTypeLabel",actorModelParam.acceptAreaTypeLabel)
                .add("acceptAreaTypeValue",actorModelParam.acceptAreaTypeValue)
                .add("actualName",actorModelParam.actualName)
                .add("age",actorModelParam.age)
                .add("area",actorModelParam.area)
                .add("avatarUlr",actorModelParam.avatarUlr)
                .add("backUrl",actorModelParam.backUrl)
                .add("cardId",actorModelParam.cardId)
                .add("caseUrl",actorModelParam.caseUrl)
                .add("complexion",actorModelParam.complexion)
                .add("country",actorModelParam.country)
                .add("frontUrl",actorModelParam.frontUrl)
                .add("handHeldUrl",actorModelParam.handHeldUrl)
                .add("height",actorModelParam.height)
                //.add("id","")
                .add("imageUrl",actorModelParam.imageUrl)
                .add("mandarinLevel",actorModelParam.mandarinLevel)
                .add("modelTypeLabel",actorModelParam.modelTypeLabel)
                .add("modelTypeValue",actorModelParam.modelTypeValue)
                .add("nickName",actorModelParam.nickName)
                .add("selfIntroduction",actorModelParam.selfIntroduction)
                .add("sex",actorModelParam.sex)
                .add("status","")
                .add("tagLabel",actorModelParam.tagLabel)
                .add("tagValue",actorModelParam.tagValue)
                .add("userId",MyUtils.getUserid())
                .add("videoUrl",actorModelParam.videoUrl)
                .add("vitalStatistics",actorModelParam.vitalStatistics)
                .add("weight",actorModelParam.weight)
                .toResponse<Any>().await()


            ToastUtils.showShort("提交成功,请等待审核通过")
            App.removeActivity(SettleActorModelActivity1::class.java)
            App.removeActivity(SettleActorModelActivity2::class.java)
            this@SettleActorModelActivity3.finish()
        }


    }

    override fun uploadCallback(url:String){
        if (uploadType == 3){
            if (::currImageView.isInitialized){
                currImageView.tag=url
                GlideUtils.load(this,currImageView,url)
            }
        }
    }
}