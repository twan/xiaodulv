package com.twan.kotlinbase.ui

import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.GuigeZuhe
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.databinding.ItemGuigeZuheBinding
import com.twan.kotlinbase.network.RxHttpScope
import kotlinx.android.synthetic.main.activity_guige_detail.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//用到接口:批量保存服务规格
class GuigeDetailActivity : BaseDataBindingActivity<ActivityCopyBinding>(){
    var serviceId=""
    var datas= mutableListOf<GuigeZuhe>()
    override fun getLayout(): Int {
        return R.layout.activity_guige_detail
    }

    override fun initEventAndData() {
        whiteToolbar("编辑规格明细","保存")
        serviceId=intent.getStringExtra("serviceId")
        initRv()
    }

    override fun rightClick() {
        datas.forEach {
            if (it.price.isNullOrEmpty()){
                ToastUtils.showShort("价格不能为空")
                return
            }
        }
        RxHttpScope().launch {
            RxHttp.postJson("serviceSku/saveAll").add("serviceSkus",datas).toResponse<Any>().await()
            ToastUtils.showShort("已保存")
            this@GuigeDetailActivity.finish()
        }
    }

    lateinit var mAdapter:BaseQuickAdapter<GuigeZuhe, BaseDataBindingHolder<ItemGuigeZuheBinding>>
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(this))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableLoadMore(false)
        refreshLayout.setEnableRefresh(true)
        rv_guige.adapter=object : BaseQuickAdapter<GuigeZuhe, BaseDataBindingHolder<ItemGuigeZuheBinding>>(R.layout.item_guige_zuhe){
            override fun convert(holder: BaseDataBindingHolder<ItemGuigeZuheBinding>, item: GuigeZuhe) {
                holder.dataBinding!!.item=item
            }

        }.also {
            mAdapter=it
            getData()
        }
    }

    fun getData(){
        RxHttpScope(this,refreshLayout).launch {
            var req=RxHttp.get("serviceSku/findAllByServiceId").add("serviceId",serviceId).toResponse<List<GuigeZuhe>>().await()
            datas=req.toMutableList()
            mAdapter.setList(req)
        }
    }

}