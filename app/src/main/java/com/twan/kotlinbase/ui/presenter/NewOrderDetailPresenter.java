package com.twan.kotlinbase.ui.presenter;

import com.twan.kotlinbase.api.ApiRetrofit;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.bean.CancelOrderBean;
import com.twan.kotlinbase.bean.OrderDetailBean;
import com.twan.kotlinbase.factory.ApiErrorHelper;
import com.twan.kotlinbase.factory.BaseSubscriber;
import com.twan.kotlinbase.ui.view.NewOrderDetailView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @anthor zhoujr
 * @time 2021/4/19 11:07
 * @describe
 */
public class NewOrderDetailPresenter extends BasePresenter<NewOrderDetailView> {
    public NewOrderDetailPresenter(BaseActivity context) {
        super(context);
    }

    public void getOrderDetail(String orderId) {
        mContext.showWaitingDialog("");
        ApiRetrofit.getInstance().getorderDetail(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<OrderDetailBean>(mContext){
                    @Override
                    public void onNext(OrderDetailBean orderDetailBean) {
                        mContext.hideWaitingDialog();
                        getView().getOrderDetailSucc(orderDetailBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }

    public void cancelOrder(String orderId) {
        mContext.showWaitingDialog("");
        ApiRetrofit.getInstance().cancelOrder(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<CancelOrderBean>(mContext){
                    @Override
                    public void onNext(CancelOrderBean cancelOrderBean) {
                        mContext.hideWaitingDialog();
                        getView().cancelSucc();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }
}
