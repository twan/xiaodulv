package com.twan.kotlinbase.ui

import android.content.Context
import android.graphics.Color
import android.view.View
import butterknife.OnClick
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.MyFragmentPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityLoginBinding
import com.twan.kotlinbase.fragment.LoginFragment
import com.twan.kotlinbase.fragment.RegisterFragment
import kotlinx.android.synthetic.main.activity_login.*
import net.lucode.hackware.magicindicator.MagicIndicator
import net.lucode.hackware.magicindicator.ViewPagerHelper
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView


class LoginActivity : BaseDataBindingActivity<ActivityLoginBinding>(){
    private lateinit var CHANNELS:Array<String>
    override fun getLayout(): Int {
        return R.layout.activity_login
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text=""
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        back.visibility=View.GONE
        tv_back_text.visibility=View.GONE

        CHANNELS = arrayOf("登录","注册")

        initMagicIndicator3()
        var tab = intent.getIntExtra("tab",1)
        if (tab == 2){
            view_pager.setCurrentItem(1,true)
        }
    }

    @OnClick(R.id.tv_skip)
    fun skip(){
        this.finish()
    }

    private fun initMagicIndicator3() {
        setupViewPager()
        val magicIndicator = findViewById(R.id.magic_indicator3) as MagicIndicator
        magicIndicator.setBackgroundColor(Color.WHITE)
        val commonNavigator = CommonNavigator(this)
        commonNavigator.isAdjustMode = true
        commonNavigator.adapter = object : CommonNavigatorAdapter() {
            override fun getCount(): Int {
                return CHANNELS.size
            }

            override fun getTitleView(context: Context, index: Int): IPagerTitleView {
                val simplePagerTitleView: SimplePagerTitleView = ColorTransitionPagerTitleView(context)
                simplePagerTitleView.normalColor = resources.getColor(R.color.text_99)
                simplePagerTitleView.selectedColor = this@LoginActivity.resources.getColor(R.color.text_black)
                simplePagerTitleView.text = CHANNELS[index]
                simplePagerTitleView.textSize=18f
                simplePagerTitleView.setOnClickListener { view_pager.currentItem = index }
                return simplePagerTitleView
            }

            override fun getIndicator(context: Context): IPagerIndicator {
                val linePagerIndicator = LinePagerIndicator(context)
                linePagerIndicator.mode = LinePagerIndicator.MODE_WRAP_CONTENT
                linePagerIndicator.setColors(this@LoginActivity.resources.getColor(R.color.head_bg))
                return linePagerIndicator
            }
        }
        magicIndicator.navigator = commonNavigator
        ViewPagerHelper.bind(magicIndicator, view_pager)
    }

    private fun setupViewPager() {
        var adapter = MyFragmentPagerAdapter(supportFragmentManager)
        adapter.addFragment(LoginFragment())
        adapter.addFragment(RegisterFragment())
        view_pager.adapter = adapter
    }
}