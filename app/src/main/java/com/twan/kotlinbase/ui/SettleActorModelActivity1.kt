package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.widgets.router.Router

//演员模特 入驻
class SettleActorModelActivity1 : BaseDataBindingActivity<ActivitySettleDresser1Binding>(){
    override fun getLayout(): Int {
        return R.layout.activity_settle_dresser1  //暂时用一样的, 只是规则不一样
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="演员、模特入驻"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

    }

    @OnClick(R.id.btn_ok)
    fun next(){
        Router.newIntent(this).to(SettleActorModelActivity2::class.java).launch()
    }

}