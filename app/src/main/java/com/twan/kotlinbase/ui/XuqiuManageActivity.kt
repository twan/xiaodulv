package com.twan.kotlinbase.ui

import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.blankj.utilcode.util.ToastUtils
import com.google.android.material.tabs.TabLayoutMediator
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.BottomViewPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.app.EnumSaleStatus
import com.twan.kotlinbase.databinding.ActivitySuggestBinding
import com.twan.kotlinbase.fragment.Suggest1Fragment
import com.twan.kotlinbase.fragment.Suggest2Fragment
import com.twan.kotlinbase.fragment.XuqiuManageFragment
import kotlinx.android.synthetic.main.activity_order.*

//需求管理
class XuqiuManageActivity : BaseDataBindingActivity<ActivitySuggestBinding>(){
    val tabs = mutableListOf("出售中","已下架")
    override fun getLayout(): Int {
        return R.layout.activity_suggest
    }

    override fun initEventAndData() {
        whiteToolbar("需求管理","全部删除")
        setupViewPager()
        initTab()
    }

    override fun rightClick() {
        ToastUtils.showShort("全部删除")
    }

    private fun setupViewPager() {
        //view_pager.offscreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT//禁用预加载
        var fragments= mutableListOf<Fragment>()
        fragments.add(XuqiuManageFragment(EnumSaleStatus.SALE_UP))
        fragments.add(XuqiuManageFragment(EnumSaleStatus.SALE_DOWN))
        var adapter = BottomViewPagerAdapter(this,fragments)
        view_pager.adapter = adapter
    }

    fun initTab(){
        TabLayoutMediator(tablayout,view_pager) { tab, position ->
            tab.text=tabs[position]
        }.attach()
        //默认选中
        //tablayout.getTabAt(0)!!.select()
        //view_pager.currentItem = 0
    }


}