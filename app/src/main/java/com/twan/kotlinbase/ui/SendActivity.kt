package com.twan.kotlinbase.ui

import android.content.Intent
import android.view.View
import android.view.ViewStub
import android.widget.ImageView
import com.blankj.utilcode.util.FragmentUtils
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.UriUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.app.Role
import com.twan.kotlinbase.bean.SpiBean
import com.twan.kotlinbase.databinding.ActivitySendBinding
import com.twan.kotlinbase.event.TakePhotoEvent
import com.twan.kotlinbase.fragment.Tab1SpFragment
import com.twan.kotlinbase.fragment.Tab1UserFragment
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.SpiUtils
import com.twan.kotlinbase.viewstubpresenter.*
import com.zhihu.matisse.Matisse
import kotlinx.android.synthetic.main.activity_send.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class SendActivity : BaseDataBindingActivity<ActivitySendBinding>(){
    var type:Int=1//1-需求,2-服务
    var view1_zhibo=Zhibo1Presenter()//发需求-直播
    var view1_shot=Shot1Presenter() //发需求-拍摄
    var view1_video_ext=VideoExt1Presenter() //发需求-推广
    var view1_model=Model1Presenter() //发需求-模特
    var view1_actor=Actor1Presenter() //发需求-演员

    var view2_zhibo=Zhibo2Presenter()//发服务-直播
    var view2_shot=Shot2Presenter() //发服务-拍摄
    var view2_video_ext=VideoExt2Presenter() //发服务-推广
    var view2_model=Model2Presenter() //发服务-模特
    var view2_actor=Actor2Presenter() //发服务-演员

    override fun getLayout(): Int {
        return R.layout.activity_send
    }

    override fun initEventAndData() {
        type=intent.getIntExtra("data",1)
        title?.visibility= View.VISIBLE
        title?.text= if (type==1) "发需求" else "发服务"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        var index=intent.getIntExtra("index",0)
        initSpi(index)
    }

    fun initSpi(index:Int){
        var spiData = mutableListOf<SpiBean>()
        spiData.add(SpiBean("店铺直播"))
        spiData.add(SpiBean("短视频制作"))
        spiData.add(SpiBean("短视频推广"))
        spiData.add(SpiBean("模特"))
        spiData.add(SpiBean("演员"))
        SpiUtils.setSpi(spi_select, spiData,index) { pos, bean ->
            switch(pos)
        }
    }

    fun switch(pos:Int){
        if (type==1){
            if(pos==0) FragmentUtils.replace(supportFragmentManager,view1_zhibo,R.id.fl_fragment_send)
            if(pos==1) FragmentUtils.replace(supportFragmentManager,view1_shot,R.id.fl_fragment_send)
            if(pos==2) FragmentUtils.replace(supportFragmentManager,view1_video_ext,R.id.fl_fragment_send)
            if(pos==3) FragmentUtils.replace(supportFragmentManager,view1_model,R.id.fl_fragment_send)
            if(pos==4) FragmentUtils.replace(supportFragmentManager,view1_actor,R.id.fl_fragment_send)
        } else {
            if(pos==0) FragmentUtils.replace(supportFragmentManager,view2_zhibo,R.id.fl_fragment_send)
            if(pos==1) FragmentUtils.replace(supportFragmentManager,view2_shot,R.id.fl_fragment_send)
            if(pos==2) FragmentUtils.replace(supportFragmentManager,view2_video_ext,R.id.fl_fragment_send)
            if(pos==3) FragmentUtils.replace(supportFragmentManager,view2_model,R.id.fl_fragment_send)
            if(pos==4) FragmentUtils.replace(supportFragmentManager,view2_actor,R.id.fl_fragment_send)
        }
    }


}