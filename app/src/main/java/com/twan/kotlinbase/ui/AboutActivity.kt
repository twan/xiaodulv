package com.twan.kotlinbase.ui

import android.view.View
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityAboutBinding


class AboutActivity : BaseDataBindingActivity<ActivityAboutBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_about
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="关于我们"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
    }


}