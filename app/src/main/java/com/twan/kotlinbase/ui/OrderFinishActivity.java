package com.twan.kotlinbase.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.twan.kotlinbase.R;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @anthor zhoujr
 * @time 2021/4/17 10:26
 * @describe
 */
public class OrderFinishActivity extends BaseActivity {
    @BindView(R.id.finishBackToMain)
    TextView finishBackToMain;
    @BindView(R.id.finishOrderDetail)
    TextView finishOrderDetail;

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("");
        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.head_bg));
    }

    @OnClick({R.id.finishBackToMain,R.id.finishOrderDetail})
    public void click(View view){
        switch (view.getId()){
            case  R.id.finishBackToMain:
                jumpToActivityAndClearTop(MainActivity.class);
                break;
            case R.id.finishOrderDetail:
                jumpToActivityAndClearTop(NewOrderDetailActivity.class);
                finish();
                break;
        }
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_orderfinish;
    }
}
