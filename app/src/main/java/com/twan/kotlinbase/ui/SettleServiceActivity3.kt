package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.App
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.DresserParam
import com.twan.kotlinbase.bean.ServiceProParam
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.databinding.ActivitySettleService3Binding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.getUrl
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_settle_service3.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//服务商入驻 入驻3
class SettleServiceActivity3 : BaseDataBindingActivity<ActivitySettleService3Binding>(){
    lateinit var currImageView: ImageView
    lateinit var serviceProParam: ServiceProParam
    override fun getLayout(): Int {
        return R.layout.activity_settle_service3
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="企业入驻"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        serviceProParam=intent.getSerializableExtra("serviceProParam") as ServiceProParam
        ll_yingye.setOnClickListener {
            uploadType=3
            currImageView=iv_yingye
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_z.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_z
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_f.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_f
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_handby_z.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_handby_z
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_handby_f.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_handby_f
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
    }

    @OnClick(R.id.btn_ok)
    fun done(){
        if (!InputUtils.checkEmpty(edt_name,edt_idcard)){
            return
        }
        if (iv_yingye.getUrl().isEmpty()){
            ToastUtils.showShort("请上传营业执照")
            return
        }
        if (iv_idcard_z.getUrl().isEmpty()){
            ToastUtils.showShort("请上传法人身份证正面")
            return
        }
        if (iv_idcard_f.getUrl().isEmpty()){
            ToastUtils.showShort("请上传法人身份证反面")
            return
        }
        if (iv_idcard_handby_z.getUrl().isEmpty()){
            ToastUtils.showShort("请上传手持身份证正面")
            return
        }
        if (iv_idcard_handby_f.getUrl().isEmpty()){
            ToastUtils.showShort("请上传手持身份证反面")
            return
        }

        serviceProParam.enterpriseName=edt_name.text.toString()
        serviceProParam.businessRegistrationCode=edt_idcard.text.toString()
        serviceProParam.businessLicenseUrl=iv_yingye.getUrl()
        serviceProParam.frontUrl=iv_idcard_z.getUrl()
        serviceProParam.backUrl=iv_idcard_f.getUrl()
        serviceProParam.handHeldFrontUrl=iv_idcard_handby_z.getUrl()
        serviceProParam.handHeldBackUrl=iv_idcard_handby_f.getUrl()

        RxHttpScope().launch {
            RxHttp.postJson("settle/shortVideoMake/save")
                .add("area",serviceProParam.area)
                .add("avatarUlr",serviceProParam.avatarUlr)
                .add("backUrl",serviceProParam.backUrl)
                .add("businessLicenseUrl",serviceProParam.businessLicenseUrl)
                .add("businessRegistrationCode",serviceProParam.businessRegistrationCode)
                .add("caseUrl",serviceProParam.caseUrl)
                .add("companyName",serviceProParam.companyName)
                .add("enterpriseName",serviceProParam.enterpriseName)
                .add("frontUrl",serviceProParam.frontUrl)
                .add("goodAtLabel",serviceProParam.goodAtLabel)
                .add("goodAtValue",serviceProParam.goodAtValue)
                .add("handHeldBackUrl",serviceProParam.handHeldBackUrl)
                .add("handHeldFrontUrl",serviceProParam.handHeldFrontUrl)
                //.add("id","")
                .add("imageUrl",serviceProParam.imageUrl)
                .add("selfIntroduction",serviceProParam.selfIntroduction)
                .add("status","")
                .add("userId",MyUtils.getUserid())
                .add("videoUrl",serviceProParam.videoUrl)
                .toResponse<Any>().await()

            //Router.newIntent(this@SettleServiceActivity3).to(BondActivity::class.java)
            ToastUtils.showShort("提交成功,请等待审核通过")
            App.removeActivity(SettleServiceActivity1::class.java)
            App.removeActivity(SettleServiceActivity2::class.java)
            this@SettleServiceActivity3.finish()
        }


    }

    override fun uploadCallback(url:String){
        if (uploadType == 3){
            if (::currImageView.isInitialized){
                currImageView.tag=url
                GlideUtils.load(this,currImageView,url)
            }
        }
    }
}