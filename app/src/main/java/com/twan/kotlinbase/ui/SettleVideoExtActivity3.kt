package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.App
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.DresserParam
import com.twan.kotlinbase.bean.ShortVideoExtParam
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.getUrl
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_settle_dresser2.*
import kotlinx.android.synthetic.main.activity_settle_dresser3.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//短视频推广 入驻3
class SettleVideoExtActivity3 : BaseDataBindingActivity<ActivitySettleDresser1Binding>(){
    lateinit var currImageView: ImageView
    lateinit var shortVideoExtParam:ShortVideoExtParam
    override fun getLayout(): Int {
        return R.layout.activity_settle_dresser3
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="短视频推广入驻认证"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        tv_desc.text="短视频推广入驻流程"

        shortVideoExtParam=intent.getSerializableExtra("shortVideoExtParam") as ShortVideoExtParam
        ll_idcard_z.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_z
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_f.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_f
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        ll_idcard_handby.setOnClickListener {
            uploadType=3
            currImageView=iv_idcard_handby
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
    }

    @OnClick(R.id.btn_ok)
    fun done(){
        if (iv_idcard_z.getUrl().isEmpty()){
            ToastUtils.showShort("请上传正面")
            return
        }
        if (iv_idcard_f.getUrl().isEmpty()){
            ToastUtils.showShort("请上传反面")
            return
        }
        if (iv_idcard_handby.getUrl().isEmpty()){
            ToastUtils.showShort("请上传手持身份证")
            return
        }
        if (!InputUtils.checkEmpty(edt_name,edt_idcard)){
            return
        }
        shortVideoExtParam.actualName=edt_name.text.toString()
        shortVideoExtParam.idCard=edt_idcard.text.toString()
        shortVideoExtParam.frontUrl=iv_idcard_z.getUrl()
        shortVideoExtParam.backUrl=iv_idcard_f.getUrl()
        shortVideoExtParam.handHeldUrl=iv_idcard_handby.getUrl()

        RxHttpScope().launch {
            RxHttp.postJson("settle/shopLive/save")
                .add("actualName",shortVideoExtParam.actualName)
                .add("area",shortVideoExtParam.area)
                .add("avatarUlr",shortVideoExtParam.avatarUlr)
                .add("backUrl",shortVideoExtParam.backUrl)
                .add("frontUrl",shortVideoExtParam.frontUrl)
                .add("goodAtLabel",shortVideoExtParam.goodAtLabel)
                .add("goodAtValue",shortVideoExtParam.goodAtValue)
                .add("handHeldUrl",shortVideoExtParam.handHeldUrl)
                .add("idCard",shortVideoExtParam.idCard)
                .add("imageUrl",shortVideoExtParam.imageUrl)
                .add("nickName",shortVideoExtParam.nickName)
                .add("platforms",shortVideoExtParam.platforms)
                //.add("id",)
                .add("selfIntroduction",shortVideoExtParam.selfIntroduction)
                .add("status",shortVideoExtParam.status)
                .add("shootStyleLabel",shortVideoExtParam.shootStyleLabel)
                .add("shootStyleValue",shortVideoExtParam.shootStyleValue)
                .add("userId",MyUtils.getUserid())
                .add("videoUrl",shortVideoExtParam.videoUrl)
                .toResponse<Any>().await()

            ToastUtils.showShort("提交成功,请等待审核通过")
            App.removeActivity(SettleVideoExtActivity1::class.java)
            App.removeActivity(SettleVideoExtActivity2::class.java)
            this@SettleVideoExtActivity3.finish()
        }


    }

    override fun uploadCallback(url:String){
        if (uploadType == 3){
            if (::currImageView.isInitialized){
                currImageView.tag=url
                GlideUtils.load(this,currImageView,url)
            }
        }
    }
}