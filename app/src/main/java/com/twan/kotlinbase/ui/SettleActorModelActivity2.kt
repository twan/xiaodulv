package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.ActorModelParam
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.bean.ShortVideoExtParam
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_settle_actormodel2.*

//演员模特 入驻2
class SettleActorModelActivity2 : BaseDataBindingActivity<ActivitySettleDresser1Binding>(){
    lateinit var currImageView: ImageView
    lateinit var mModelType:Dict
    lateinit var mSex:Dict
    lateinit var mCountry:Dict
    lateinit var mLanuageLevel:Dict
    lateinit var mSkin:Dict
    lateinit var mTag:Dict
    lateinit var mArea:Dict
    override fun getLayout(): Int {
        return R.layout.activity_settle_actormodel2
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="演员、模特入驻"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        initRvMultiPicOrVideo(rv_multi_pic,1)
        initRvMultiPicOrVideo(rv_multi_video,2)
        //头像
        rl_avater.setOnClickListener {
            uploadType=3
            currImageView=iv_avater
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        //案例
        iv_anli.setOnClickListener {
            uploadType=3
            currImageView=iv_anli
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        //模特类型
        SpiUtils.setSpi(spi_actor_type, DictUtils.getDictCache(DictUtils.model_type)){ pos,item->
            mModelType=item
        }
        //性别
        SpiUtils.setSpi(spi_sex, DictUtils.getDictCache(DictUtils.sex)){ pos,item->
            mSex=item
        }
        //国籍
        SpiUtils.setSpi(spi_country, DictUtils.getDictCache(DictUtils.nationality)){ pos,item->
            mCountry=item
        }
        //普通话程度
        SpiUtils.setSpi(spi_lanuage_level, DictUtils.getDictCache(DictUtils.Mandarin_level)){ pos,item->
            mLanuageLevel=item
        }
        //肤色
        SpiUtils.setSpi(spi_skin, DictUtils.getDictCache(DictUtils.color)){ pos,item->
            mSkin=item
        }
        //标签 todo
        SpiUtils.setSpi(spi_tag, DictUtils.getDictCache(DictUtils.live_tag)){ pos,item->
            mTag=item
        }
        //所在地区 todo
        SpiUtils.setSpi(spi_area, DictUtils.getDictCache(DictUtils.live_tag)){ pos,item->
            mArea=item
        }

        btn_ok.setOnClickListener {
            if (uploadPics.size==1) {
                ToastUtils.showShort("请上传图片")
                return@setOnClickListener
            }
            if (uploadVideos.size==1) {
                ToastUtils.showShort("请上传视频")
                return@setOnClickListener
            }
            if (!InputUtils.checkEmpty(edt_adv_name,edt_produce,edt_height,edt_weight,edt_age,edt_sanwei)){
                return@setOnClickListener
            }

            var actorModelParam = ActorModelParam()
            actorModelParam.avatarUlr=iv_avater.getUrl()
            actorModelParam.nickName=edt_adv_name.text.toString()
            actorModelParam.selfIntroduction=edt_produce.text.toString()
            actorModelParam.imageUrl=getPicsUrl()
            actorModelParam.videoUrl=getVideosUrl()
            actorModelParam.caseUrl=iv_anli.getUrl()
            actorModelParam.modelTypeValue=mModelType.id.toString()
            actorModelParam.modelTypeLabel=mModelType.label
            actorModelParam.sex=mSex.id.toString()
            actorModelParam.country=mCountry.id.toString()
            actorModelParam.height=edt_height.text.toString()
            actorModelParam.weight=edt_weight.text.toString()
            actorModelParam.age=edt_age.text.toString()
            actorModelParam.vitalStatistics=edt_sanwei.text.toString()// 三围
            actorModelParam.mandarinLevel=mLanuageLevel.id.toString()
            actorModelParam.complexion=mSkin.id.toString()//肤色
            actorModelParam.tagLabel=mTag.id.toString()
            actorModelParam.tagValue=mTag.label
            actorModelParam.area=mArea.id.toString()//所在地区
            actorModelParam.acceptAreaTypeValue=if (rb_local.isChecked) "1" else if (rb_out.isChecked) "2" else "3"
            actorModelParam.acceptAreaTypeLabel=if (rb_local.isChecked) rb_local.text.toString() else if (rb_out.isChecked) rb_out.text.toString() else rb_nolimit.text.toString()

            Router.newIntent(this).putSerializable("actorModelParam",actorModelParam).to(SettleActorModelActivity3::class.java).launch()
        }
    }

    override fun uploadCallback(url:String){
        if (uploadType == 1){
            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==2){ //选择了视频就清空图片
            uploadVideos.add(UploadPic(filepath=url))
            rv_multi_video.adapter!!.notifyDataSetChanged()
        } else { //单个的图片或者视频
            if (::currImageView.isInitialized){
                currImageView.tag=url
                GlideUtils.load(this,currImageView,url)
            }
        }
    }

}