package com.twan.kotlinbase.ui

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.BottomViewPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.fragment.MycouponFragment
import kotlinx.android.synthetic.main.activity_order.*


class MyCouponActivity : BaseDataBindingActivity<ActivityCopyBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_my_coupon
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="我的优惠券"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        setupViewPager()
        initTab()
    }

    private fun setupViewPager() {
        //view_pager.offscreenPageLimit = ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT//禁用预加载
        var fragments= mutableListOf<Fragment>()
        fragments.add(MycouponFragment(1))
        fragments.add(MycouponFragment(2))
        fragments.add(MycouponFragment(3))
        var adapter = BottomViewPagerAdapter(this,fragments)
        view_pager.adapter = adapter
    }

    fun initTab(){
        TabLayoutMediator(tablayout,view_pager) { tab, position ->
            tab.text=tabs[position]
        }.attach()
        //默认选中
        //tablayout.getTabAt(0)!!.select()
        //view_pager.currentItem = 0
    }

    val tabs = mutableListOf("未使用","已使用","已过期")


}