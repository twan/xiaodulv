package com.twan.kotlinbase.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager2.widget.ViewPager2;

import com.blankj.utilcode.util.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.luck.picture.lib.PictureSelector;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.PreviewAdapter;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.util.StatusBarUtils;
import com.twan.kotlinbase.util.UIUtils;
import com.twan.kotlinbase.widgets.CustomProgress;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @anthor zhoujr
 * @time 2021/4/17 9:50
 * 预览作品
 * @describe
 */
public class PreviewProductActivity extends BaseActivity {
    @BindView(R.id.previewVP)
    ViewPager2 previewVP;
    int position;

    @Override
    public void init() {
        super.init();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                StatusBarUtils.setStatusBar(PreviewProductActivity.this, false, true);
            }
        },10);
    }

    @Override
    public void initView() {
        super.initView();
        position = getIntent().getIntExtra("position",0);
        setTitleCount(position);
        setToolBarImgAndBg(R.mipmap.ic_arrow_back_white, getResources().getColor(R.color.c_333333));
        ArrayList<Integer> urls = new ArrayList<>();
        for(int i = 0;i<9;i++){
            urls.add(android.R.color.black);
            urls.add(android.R.color.holo_purple);
            urls.add(android.R.color.holo_blue_dark);
            urls.add(android.R.color.holo_green_light);
        }
        PreviewAdapter previewAdapter = new PreviewAdapter(urls);
        previewVP.setAdapter(previewAdapter);
        previewVP.setCurrentItem(position);
        previewVP.setOffscreenPageLimit(urls.size());
        previewVP.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setTitleCount(position);
            }
        });
        previewAdapter.setItemClick(new PreviewAdapter.ItemClick() {
            @Override
            public void saveImg(int position) {
                CustomProgress.show(PreviewProductActivity.this,"加载中...");
                Glide.with(PreviewProductActivity.this).asBitmap().load("url").into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        CustomProgress.setdismiss();
                        ImageUtils.save2Album(resource, Bitmap.CompressFormat.PNG);
                        UIUtils.showToast("已保存");
                    }
                });
            }
        });
    }

    private void setTitleCount(int position) {
        setActivityTitle((position+1)+"/9",R.color.white);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_previewproduct;
    }

}
