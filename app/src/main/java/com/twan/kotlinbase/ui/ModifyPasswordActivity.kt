package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityPasswordBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.activity_password.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class ModifyPasswordActivity : BaseDataBindingActivity<ActivityPasswordBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_password
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="修改密码"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
    }

    @OnClick(R.id.btn_click)
    fun btn(){
        if (!InputUtils.checkEmpty(edt_password,edt_password_again)) return
        RxHttpScope().launch {
            RxHttp.postJson("users/updatePass").add("newPass",edt_password.text.toString())
                    .add("oldPass",edt_password_again.text.toString()).add("userId",MyUtils.getUserid()).toResponse<Any>().await()
            ToastUtils.showShort("修改成功")
            this@ModifyPasswordActivity.finish()
        }
    }



}