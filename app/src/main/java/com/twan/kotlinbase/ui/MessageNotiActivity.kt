package com.twan.kotlinbase.ui

import android.view.View
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityMsgNotiBinding


class MessageNotiActivity : BaseDataBindingActivity<ActivityMsgNotiBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_msg_noti
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="消息通知"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
    }


}