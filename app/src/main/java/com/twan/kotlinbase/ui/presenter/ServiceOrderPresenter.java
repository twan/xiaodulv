package com.twan.kotlinbase.ui.presenter;

import com.twan.kotlinbase.api.ApiRetrofit;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.factory.ApiErrorHelper;
import com.twan.kotlinbase.factory.BaseSubscriber;
import com.twan.kotlinbase.ui.view.ServiceOrderView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @anthor zhoujr
 * @time 2021/4/19 9:24
 * @describe
 */
public class ServiceOrderPresenter extends BasePresenter<ServiceOrderView> {
    public ServiceOrderPresenter(BaseActivity context) {
        super(context);
    }

    public void getServiceOrderLst(int page, String status) {
        ApiRetrofit.getInstance().getOrderLst(status,page+"","sellerId")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<OrderBean>(mContext){
                    @Override
                    public void onNext(OrderBean orderBean) {
                        getView().getServiceOrderLstSucc(orderBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }

    public void confimReceiveSimple(String orderId) {
        mContext.showWaitingDialog("");
        ApiRetrofit.getInstance().confimreceiveSimple(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<String>(mContext){
                    @Override
                    public void onNext(String string) {
                        getView().confimReceivedSimpleSucc();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }
}
