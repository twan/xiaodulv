package com.twan.kotlinbase.ui

import android.content.Context
import android.view.View
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.MyFragmentPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.databinding.ActivityServiceBinding
import com.twan.kotlinbase.fragment.*
import kotlinx.android.synthetic.main.activity_login.*
import net.lucode.hackware.magicindicator.MagicIndicator
import net.lucode.hackware.magicindicator.ViewPagerHelper
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView


class ServiceActivity : BaseDataBindingActivity<ActivityServiceBinding>(){
    private lateinit var CHANNELS:Array<String>
    private lateinit var need:Need
    override fun getLayout(): Int {
        return R.layout.activity_service
    }

    override fun initEventAndData() {
        title?.visibility=View.GONE
        back?.visibility=View.GONE
        searchView?.visibility=View.GONE//一期隐藏
        back.visibility=View.VISIBLE
        tv_back_text.setCompoundDrawables(null,null,null,null)
        tv_back_text?.visibility=View.VISIBLE
        tv_back_text?.text="短视频制作"
        tv_back_text?.textSize=16f

        need= intent.getSerializableExtra("need") as Need
        tv_back_text?.text=need.title

        CHANNELS = arrayOf("服务","店铺")

        initMagicIndicator3()
        var tab = intent.getIntExtra("tab",1)
        if (tab == 2){
            view_pager.setCurrentItem(1,true)
        }
    }

    private fun initMagicIndicator3() {
        setupViewPager()
        val magicIndicator = findViewById(R.id.magic_indicator3) as MagicIndicator
        magicIndicator.setBackgroundColor(resources.getColor((R.color.head_bg)))
        val commonNavigator = CommonNavigator(this)
        commonNavigator.isAdjustMode = true
        commonNavigator.adapter = object : CommonNavigatorAdapter() {
            override fun getCount(): Int {
                return CHANNELS.size
            }

            override fun getTitleView(context: Context, index: Int): IPagerTitleView {
                val simplePagerTitleView: SimplePagerTitleView = ColorTransitionPagerTitleView(context)
                simplePagerTitleView.normalColor = resources.getColor(R.color.text_99)
                simplePagerTitleView.selectedColor = this@ServiceActivity.resources.getColor(R.color.text_black)
                simplePagerTitleView.text = CHANNELS[index]
                simplePagerTitleView.textSize=14f
                simplePagerTitleView.setOnClickListener { view_pager.currentItem = index }
                return simplePagerTitleView
            }

            override fun getIndicator(context: Context): IPagerIndicator {
                val linePagerIndicator = LinePagerIndicator(context)
                linePagerIndicator.mode = LinePagerIndicator.MODE_WRAP_CONTENT
                linePagerIndicator.setColors(this@ServiceActivity.resources.getColor(R.color.white))
                return linePagerIndicator
            }
        }
        magicIndicator.navigator = commonNavigator
        ViewPagerHelper.bind(magicIndicator, view_pager)
    }

    private fun setupViewPager() {
        var adapter = MyFragmentPagerAdapter(supportFragmentManager)
        adapter.addFragment(ServiceFragment(need))
        adapter.addFragment(ShopFragment(need))
        view_pager.adapter = adapter
    }



}