package com.twan.kotlinbase.ui

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.Recharge
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.databinding.ItemRechargeBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_rechage.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//充值
class RechargeActivity : BaseDataBindingActivity<ActivityCopyBinding>(){
    var mLast:View?=null
    override fun getLayout(): Int {
        return R.layout.activity_rechage
    }

    override fun initEventAndData() {
        whiteToolbar("充值","充值记录")
        initRv()
    }

    override fun rightClick() {
        Router.newIntent(this).to(RechargeHistoryActivity::class.java).launch()
    }

    fun initRv(){
        rv_recharge.layoutManager=GridLayoutManager(this,3)
        rv_recharge.adapter=object :BaseQuickAdapter<Recharge,BaseDataBindingHolder<ItemRechargeBinding>>(R.layout.item_recharge){
            override fun convert(holder: BaseDataBindingHolder<ItemRechargeBinding>, item: Recharge) {
                holder.dataBinding!!.item=item
            }
        }.also {
            it.setOnItemClickListener { adapter, view, position ->
                if (mLast != null) mLast!!.background = resources.getDrawable(R.drawable.corner_recharge_no_solid)
                view.background = resources.getDrawable(R.drawable.corner_recharge)
                edt_money.setText(it.data[position].sellingPrice)
                mLast=view
            }
            getData(it)
        }
    }

    fun getData(adapter: BaseQuickAdapter<Recharge,BaseDataBindingHolder<ItemRechargeBinding>>){
        RxHttpScope().launch {
            var req=RxHttp.get("account/findRechargeBase").toResponse<List<Recharge>>().await()
            adapter.setList(req)
        }
    }


    @OnClick(R.id.btn_click)
    fun rechare(){
        var sellingPrice=edt_money.text.toString()
        if (sellingPrice.isEmpty()){
            ToastUtils.showShort("请输入充值金额")
            return
        }
        RxHttpScope().launch {
            var req = RxHttp.postJson("account/addCredit")
                    .add("balance", sellingPrice)
                    .add("userId", MyUtils.getUserid())
                    .toResponse<Any>().await()
            ToastUtils.showShort("充值成功")
        }

    }

}