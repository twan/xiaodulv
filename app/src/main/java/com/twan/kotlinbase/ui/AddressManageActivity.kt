package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.BankItem
import com.twan.kotlinbase.bean.RechaegeHistory
import com.twan.kotlinbase.databinding.ActivityAccountManageBinding
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.databinding.ItemBankBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_account_manage.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class AddressManageActivity : BaseDataBindingActivity<ActivityAccountManageBinding>(){
    lateinit var mAdpater:BaseQuickAdapter<BankItem, BaseDataBindingHolder<ItemBankBinding>>
    override fun getLayout(): Int {
        return R.layout.activity_account_manage  //和账号管理是一样的, 这里共用了xml
    }

    override fun initEventAndData() {
        whiteToolbar("地址管理")
        btn_add.text="新增地址"
        initRv()
    }

    @OnClick(R.id.btn_add)
    fun addAddress(){
        Router.newIntent(this).to(AddAddressActivity::class.java).launch()
    }

    fun initRv() {
        refreshLayout.setRefreshHeader(ClassicsHeader(this))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        rv_card.adapter = object : BaseQuickAdapter<BankItem, BaseDataBindingHolder<ItemBankBinding>>(R.layout.item_bank) {
            override fun convert(holder: BaseDataBindingHolder<ItemBankBinding>, item: BankItem) {
                holder.dataBinding!!.item = item
                if(item.accountType=="1"){
                    holder.dataBinding!!.rlBg.background=resources.getDrawable(R.mipmap.zhifubao)
                } else {
                    holder.dataBinding!!.rlBg.background=resources.getDrawable(R.mipmap.jianshe_bank)
                }
                holder.dataBinding!!.ivDel.setOnClickListener {
                    delBank(item.id)
                }
            }

        }.also {
            mAdpater=it
            getData()
        }
    }

    private fun getData() {
        RxHttpScope(this,refreshLayout).launch {
            var req=RxHttp.get("").add("userId",MyUtils.getUserid()).toResponse<List<BankItem>>().await()
            mAdpater.setList(req)
        }
    }

    fun delBank(id:String){
        RxHttpScope().launch {
            RxHttp.get("").add("id",id).toResponse<Any>().await()
            getData()
            ToastUtils.showShort("删除成功")
        }
    }
}