package com.twan.kotlinbase.ui.view;

import com.twan.kotlinbase.bean.AliPayBean;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.bean.WeChatPayBean;

/**
 * @anthor zhoujr
 * @time 2021/4/13 13:26
 * @describe
 */
public interface NewOrderView {
    void getOrderLstSucc(OrderBean orderBean);
    void confimreceiveSucc();
    void cancelSucc();
    void deleteSucc();
    void getAliPayDataSucc(AliPayBean aliPayBean);
    void getWeChatPayDataSucc(WeChatPayBean weChatPayBean);
}
