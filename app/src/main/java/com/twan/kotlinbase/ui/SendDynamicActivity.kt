package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.ActivitySendDynamicBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.DateUtils
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.getUrl
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.activity_send_dynamic.*
import kotlinx.android.synthetic.main.activity_send_dynamic.edt_content
import kotlinx.android.synthetic.main.activity_send_dynamic.rv_multi_pic
import kotlinx.android.synthetic.main.activity_upload_work.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//发动态
class SendDynamicActivity : BaseDataBindingActivity<ActivitySendDynamicBinding>(){
    lateinit var currImageView: ImageView
    override fun getLayout(): Int {
        return R.layout.activity_send_dynamic
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="发动态"
        initRvMultiPicOrVideo(rv_multi_pic,1)
    }
        //XPopup.Builder(this).asCustom(AddPicPopup(this,mimeType = MimeType.of(MimeType.JPEG,MimeType.PNG,MimeType.MP4,MimeType.GIF))).show()

    @OnClick(R.id.btn_ok)
    fun sendMoment(){
        var ischeck=InputUtils.checkEmpty(edt_content)
        if (!ischeck) return

        RxHttpScope().launch {
            var req=RxHttp.postJson("/moment/save")
                .add("content",edt_content.text.toString())
                .add("delFlag","0")
                //.add("id","")
                .add("imagesUrl",getPicsUrl())
                .add("likeCount","")//: 0,
                .add("releaseTime",DateUtils.today2())
                .add("replyCount","")//: 0,
                .add("userId",MyUtils.getUserid())
                .toResponse<Any>().await()
            ToastUtils.showShort("动态已发布")
            this@SendDynamicActivity.finish()
        }
    }

    override fun uploadCallback(url:String){
        if (uploadType == 1){
            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        }
    }

}