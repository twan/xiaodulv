package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityAddZfbOrYhkBinding
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.activity_add_zfb_or_yhk.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class AddzfbActivity : BaseDataBindingActivity<ActivityAddZfbOrYhkBinding>(){
    override fun getLayout(): Int {
        return R.layout.activity_add_zfb_or_yhk
    }

    override fun initEventAndData() {
        whiteToolbar("添加支付宝")

        mBinding.isZfb=true
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            mBinding.isZfb= (checkedId==R.id.rb_zfb)
            title?.text= if (mBinding.isZfb==true) "添加支付宝" else "添加银行卡"
        }
    }

    @OnClick(R.id.btn_click)
    fun add(){
        RxHttpScope().launch {
            if (mBinding.isZfb!!) {
                RxHttp.postJson("bankCard/save")
                        .add("accountType", "1") //1支付宝，2银行卡
                        .add("cardNumber", edt_zfb_account.text.toString())
                        .add("realName", edt_zfb_name.text.toString())
                        .add("userId", MyUtils.getUserid())
                        .toResponse<Any>().await()
                ToastUtils.showShort("支付宝添加成功")
                this@AddzfbActivity.finish()
            } else {
                RxHttp.postJson("bankCard/save")
                        .add("accountBank", edt_card_bank.text.toString())
                        .add("accountBranch", edt_card_bank2.text.toString())
                        .add("accountType", "2") //1支付宝，2银行卡
                        //.add("bankCardName", "")
                        .add("cardNumber", edt_card_no.text.toString())
                        .add("realName", edt_card_name.text.toString())
                        .add("userId", MyUtils.getUserid())
                        .toResponse<Any>().await()
                ToastUtils.showShort("添加银行卡成功")
                this@AddzfbActivity.finish()
            }
        }
    }


}