package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.OrderContentDTO
import com.twan.kotlinbase.bean.OrderDTO
import com.twan.kotlinbase.bean.OrderDetail
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySendDemoBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.activity_order_detail.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//发送样品
class SendDemoActivity : BaseDataBindingActivity<ActivitySendDemoBinding>(){
    lateinit var orderDto:OrderDTO
    lateinit var orderDetail: OrderDetail
    override fun getLayout(): Int {
        return R.layout.activity_send_demo
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="寄送样品"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        //orderDto=intent.getSerializableExtra("data") as OrderDTO
        mBinding.isOperate=true //是否在运营中
        //getData()
    }

    @OnClick(R.id.tv_copy)
    fun copy(){
        if (::orderDetail.isInitialized) {
            MyUtils.copy(this, orderDetail.orderContentDTO.orderCode)
            ToastUtils.showShort("已复制")
        }
    }

    fun getData(){
        RxHttpScope().launch {
            orderDetail= RxHttp.get("orderShipping/mailingSample")
                    .add("orderId",orderDto.id)
                    .add("receiverAddress", "")
                    .add("receiverName", "")
                    .add("receiverPhone", "")
                    .add("waybillNumber", "")
                    .toResponse<OrderDetail>().await()
        }
    }

}