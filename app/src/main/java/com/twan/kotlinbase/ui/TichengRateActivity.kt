package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.event.TichengEvent
import com.twan.kotlinbase.util.InputUtils
import kotlinx.android.synthetic.main.activity_ticheng_rate.*
import org.greenrobot.eventbus.EventBus


class TichengRateActivity : BaseDataBindingActivity<ActivityCopyBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_ticheng_rate
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="提成比例"
        tv_right?.visibility = View.VISIBLE
        tv_right?.setTextColor(resources.getColor(R.color.text_black))
        tv_right?.text="保存"
        tv_right.setOnClickListener {
            if (!InputUtils.checkEmpty(edt_custom_ticheng)) return@setOnClickListener
            EventBus.getDefault().post(TichengEvent("${edt_custom_ticheng.text.toString()}%"))
            this.finish()
        }
    }



    @OnClick(R.id.rl_none)
    fun none(){
        EventBus.getDefault().post(TichengEvent("无"))
        this.finish()
    }
    @OnClick(R.id.rl_1)
    fun rl1(){
        EventBus.getDefault().post(TichengEvent("1%"))
        this.finish()
    }
    @OnClick(R.id.rl_2)
    fun rl2(){
        EventBus.getDefault().post(TichengEvent("2%"))
        this.finish()
    }
    @OnClick(R.id.rl_3)
    fun rl3(){
        EventBus.getDefault().post(TichengEvent("3%"))
        this.finish()
    }
    @OnClick(R.id.rl_4)
    fun rl4(){
        EventBus.getDefault().post(TichengEvent("4%"))
        this.finish()
    }
    @OnClick(R.id.rl_5)
    fun rl5(){
        EventBus.getDefault().post(TichengEvent("5%"))
        this.finish()
    }

}