package com.twan.kotlinbase.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twan.kotlinbase.R;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.widgets.RoundImageView;
import com.twan.kotlinbase.widgets.RoundImageView5;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.iwgang.countdownview.CountdownView;

/**
 * @anthor zhoujr
 * @time 2021/4/17 10:56
 * 服务中
 * @describe
 */
public class ServicingActivity extends BaseActivity {
    @BindView(R.id.orderDetailCountDown)
    CountdownView orderDetailCountDown;
    @BindView(R.id.orderDetailName)
    TextView orderDetailName;
    @BindView(R.id.orderDetailMobile)
    TextView orderDetailMobile;
    @BindView(R.id.orderDetailAddress)
    TextView orderDetailAddress;
    @BindView(R.id.orderDetailCopy)
    TextView orderDetailCopy;
    @BindView(R.id.orderDetailServiceIcon)
    RoundImageView orderDetailServiceIcon;
    @BindView(R.id.orderDetailServiceName)
    TextView orderDetailServiceName;
    @BindView(R.id.countdownLL)
    LinearLayout countdownLL;
    @BindView(R.id.orderDetailIcon)
    RoundImageView5 orderDetailIcon;
    @BindView(R.id.orderDetailTitle)
    TextView orderDetailTitle;
    @BindView(R.id.orderDetailPrice)
    TextView orderDetailPrice;
    @BindView(R.id.orderDetailDesc)
    TextView orderDetailDesc;
    @BindView(R.id.orderDetailCount)
    TextView orderDetailCount;
    @BindView(R.id.orderDetailRealPay)
    TextView orderDetailRealPay;
    @BindView(R.id.orderDetailOrderNo)
    TextView orderDetailOrderNo;
    @BindView(R.id.orderDetailOrderNoCopy)
    TextView orderDetailOrderNoCopy;
    @BindView(R.id.orderDetailCreateDate)
    TextView orderDetailCreateDate;
    @BindView(R.id.orderDetailFinishTime)
    TextView orderDetailFinishTime;
    @BindView(R.id.orderDetailPayWay)
    TextView orderDetailPayWay;
    @BindView(R.id.servingLL)
    LinearLayout servingLL;
    @BindView(R.id.addressLL)
    LinearLayout addressLL;

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("订单信息");
        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.head_bg));
        countdownLL.setVisibility(View.GONE);
        addressLL.setBackground(getResources().getDrawable(R.color.layout_bg));
        //服务中
//        View view = LayoutInflater.from(this).inflate(R.layout.view_serving,null);
//        servingLL.addView(view);
        //交易关闭
//        View view = LayoutInflater.from(this).inflate(R.layout.view_closed,null);
//        servingLL.addView(view);

        //交易完成
//        View view = LayoutInflater.from(this).inflate(R.layout.view_orderfinish,null);
//        servingLL.addView(view);
        //寄样中
        View view = LayoutInflater.from(this).inflate(R.layout.view_sendsimpleing,null);
        servingLL.addView(view);
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_servicing;
    }
}
