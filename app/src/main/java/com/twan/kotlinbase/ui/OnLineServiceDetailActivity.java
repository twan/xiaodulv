package com.twan.kotlinbase.ui;

import com.twan.kotlinbase.R;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;

public class OnLineServiceDetailActivity extends BaseActivity {

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("订单详情");
        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.head_bg));
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_onlineservice;
    }
}
