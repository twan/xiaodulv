package com.twan.kotlinbase.ui

import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.header.ClassicsHeader
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.InviteFried
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.PersonInfo
import com.twan.kotlinbase.databinding.ActivityInviteFriendBinding
import com.twan.kotlinbase.databinding.ItemInviteFriendBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.WxShareUtil
import kotlinx.android.synthetic.main.activity_invite_friend.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class InviteFriendActivity : BaseDataBindingActivity<ActivityInviteFriendBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_invite_friend
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text=""
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        initRv()
    }

    @OnClick(R.id.iv_back2)
    fun back(){
        this.finish()
    }
    @OnClick(R.id.tv_copy)
    fun copy(){
        MyUtils.copy(this,tv_invite_code.text.toString())
        ToastUtils.showShort("已复制")
    }

    lateinit var mAdpater:BaseQuickAdapter<InviteFried, BaseDataBindingHolder<ItemInviteFriendBinding>>
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(this))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        rv_list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rv_list.adapter=object : BaseQuickAdapter<InviteFried, BaseDataBindingHolder<ItemInviteFriendBinding>>(R.layout.item_invite_friend){
            override fun convert(holder: BaseDataBindingHolder<ItemInviteFriendBinding>, item: InviteFried) {
                holder.dataBinding!!.item=item
                //GlideUtils.loadCicle(this@InviteFriendActivity,holder.dataBinding!!.ivAvater,item.)
            }

        }.also {
            mAdpater=it
            getData()
        }
    }

    var currentPage=1
    fun getData(isRefresh:Boolean=true){
        if (isRefresh) currentPage=1
        RxHttpScope(this,refreshLayout).launch {
            var req= RxHttp.get("invite/getByCondition")
                    .add("userId",MyUtils.getUserid())
                    .add("pageSize",20)
                    .add("currentPage",currentPage++)
                    .toResponse<PageData<InviteFried>>().await()
            if (currentPage==2) mAdpater.setList(req.content)
            else  mAdpater.addData(req.content)
        }


        RxHttpScope().launch {
            var req=RxHttp.get("users/findByUserId").add("userId",MyUtils.getUserid()).toResponse<PersonInfo>().await()
            tv_invite_code.text=req.invitationCode
        }
    }

    @OnClick(R.id.rl_share)
    fun wxshare(){
        var shareUrl="http://baidu.com"
        var iconUrl="http://dw.ccrjkf.com/img/108.png"
        WxShareUtil.getInstance().shareUrlToWx(shareUrl,"我向你推荐小肚驴","下载APP, 好礼送不停",iconUrl,
            SendMessageToWX.Req.WXSceneSession
        )
    }
}