package com.twan.kotlinbase.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.broadcast.BroadcastAction;
import com.luck.picture.lib.broadcast.BroadcastManager;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnItemClickListener;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.GridImageAdapter;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.util.GlideEngine;
import com.twan.kotlinbase.util.UIUtils;
import com.twan.kotlinbase.widgets.FullyGridLayoutManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @anthor zhoujr
 * @time 2021/4/17 15:33
 * @describe
 */
public class UploadProjectActivity extends BaseActivity {
    @BindView(R.id.uploadLeaveMsg)
    EditText uploadLeaveMsg;
    @BindView(R.id.uploadRecy)
    RecyclerView uploadRecy;
    @BindView(R.id.uploadConfimBtn)
    Button uploadConfimBtn;
    GridImageAdapter gridImageAdapter;
    ArrayList<LocalMedia> imgurls=new ArrayList<>();
    private int themeId;
    private int chooseMode = PictureMimeType.ofImage();


    @Override
    public void initView() {
        super.initView();
        themeId = R.style.picture_default_style;
        setActivityTitle("编辑作品");
        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.head_bg));

        FullyGridLayoutManager manager = new FullyGridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        uploadRecy.setLayoutManager(manager);
        gridImageAdapter = new GridImageAdapter(this, onAddPicClickListener);
        gridImageAdapter.setList(imgurls);
        gridImageAdapter.setSelectMax(100);
        uploadRecy.setAdapter(gridImageAdapter);
        gridImageAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                PictureSelector.create(UploadProjectActivity.this)
                        .themeStyle(R.style.picture_default_style)
                        .setPictureStyle(UIUtils.getPictureStyle())
                        .isNotPreviewDownload(true)
                        .imageEngine(GlideEngine.createGlideEngine())
                        .openExternalPreview(position, imgurls);
            }
        });
        // 注册广播
        BroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                BroadcastAction.ACTION_DELETE_PREVIEW_POSITION);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            BroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver,
                    BroadcastAction.ACTION_DELETE_PREVIEW_POSITION);
        }
    }

    private GridImageAdapter.onAddPicClickListener onAddPicClickListener = new GridImageAdapter.onAddPicClickListener() {
        @Override
        public void onAddPicClick() {
            PictureSelector.create(UploadProjectActivity.this)
                    .openGallery(chooseMode)
                    .theme(themeId)
                    .maxSelectNum(9)
                    .minSelectNum(1)
                    .imageEngine(GlideEngine.createGlideEngine())
                    .selectionMode(PictureConfig.MULTIPLE)
                    .isCamera(true)
                    .isMaxSelectEnabledMask(true)
                    .imageSpanCount(4)
                    .isCompress(true)
                    .selectionData(gridImageAdapter.getData())
                    .minimumCompressSize(100)
                    .forResult(PictureConfig.CHOOSE_REQUEST);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择
                    imgurls = (ArrayList<LocalMedia>) PictureSelector.obtainMultipleResult(data);
                    gridImageAdapter.setList(imgurls);
                    gridImageAdapter.notifyDataSetChanged();
//                    for(int i=0;i<PictureSelector.obtainMultipleResult(data).size();i++){
//                        File file= new File(PictureSelector.obtainMultipleResult(data).get(i).getCompressPath());
//                        mPresenter.uploadimg(file);
//                    }
                    break;
            }
        }
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TextUtils.isEmpty(action)) {
                return;
            }
            if (BroadcastAction.ACTION_DELETE_PREVIEW_POSITION.equals(action)) {
                // 外部预览删除按钮回调
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    int position = extras.getInt(PictureConfig.EXTRA_PREVIEW_DELETE_POSITION);
                    gridImageAdapter.remove(position);
                    gridImageAdapter.notifyItemRemoved(position);
                }
            }
        }
    };

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_uploadpproject;
    }

}
