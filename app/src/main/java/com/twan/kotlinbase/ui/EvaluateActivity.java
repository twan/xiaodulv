package com.twan.kotlinbase.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnItemClickListener;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.GridImageAdapter;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.util.GlideEngine;
import com.twan.kotlinbase.util.UIUtils;
import com.twan.kotlinbase.widgets.FullyGridLayoutManager;
import com.twan.kotlinbase.widgets.RatingBar;
import com.twan.kotlinbase.widgets.RoundImageView;
import com.twan.kotlinbase.widgets.RoundImageView5;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @anthor zhoujr
 * @time 2021/4/14 14:29
 * 评论
 * @describe
 */
public class EvaluateActivity extends BaseActivity {
    @BindView(R.id.evaluteheadIcon)
    RoundImageView evaluteheadIcon;
    @BindView(R.id.evaluteName)
    TextView evaluteName;
    @BindView(R.id.evaluteIcon)
    RoundImageView5 evaluteIcon;
    @BindView(R.id.evaluteTitle)
    TextView evaluteTitle;
    @BindView(R.id.evalutePrice)
    TextView evalutePrice;
    @BindView(R.id.evaluteContent)
    EditText evaluteContent;
    @BindView(R.id.evaluteCount)
    TextView evaluteCount;
    @BindView(R.id.evaluteRecy)
    RecyclerView evaluteRecy;
//    @BindView(R.id.evaluteIsMatch)
//    RatingBar evaluteIsMatch;
    @BindView(R.id.evaluteIsGood)
    RatingBar evaluteIsGood;
    @BindView(R.id.evaluteSubmit)
    Button evaluteSubmit;
    GridImageAdapter pjImageAdapter;
    ArrayList<LocalMedia> pjurls=new ArrayList<>();
    private int themeId;
    private int chooseMode = PictureMimeType.ofImage();

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("评价");
        themeId = R.style.picture_default_style;
        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.white));
        ImmersionBar.with(this)
                .statusBarColor(R.color.white)
                .statusBarDarkFont(true)
                .flymeOSStatusBarFontColor(R.color.black)
                .init();
        evaluteContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = evaluteContent.getText().toString().trim().length();
                evaluteCount.setText((300-length)+"字");
            }
        });
//        evaluteIsMatch.setmClickable(true);
//        evaluteIsMatch.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
//            @Override
//            public void onRatingChange(int var1) {
//                UIUtils.showToast(var1+"");
//            }
//        });
        evaluteIsGood.setmClickable(true);
        evaluteIsGood.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(int var1) {
                UIUtils.showToast(var1+"");
            }
        });
        FullyGridLayoutManager manager = new FullyGridLayoutManager(this, 4, GridLayoutManager.VERTICAL, false);
        evaluteRecy.setLayoutManager(manager);
        pjImageAdapter = new GridImageAdapter(this, onAddPicClickListener);
        pjImageAdapter.setList(pjurls);
        pjImageAdapter.setSelectMax(9);
        evaluteRecy.setAdapter(pjImageAdapter);
        pjImageAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                PictureSelector.create(EvaluateActivity.this)
                        .themeStyle(R.style.picture_default_style)
                        .isNotPreviewDownload(true)
                        .openExternalPreview(position, pjurls);
            }
        });
    }

    private GridImageAdapter.onAddPicClickListener onAddPicClickListener = new GridImageAdapter.onAddPicClickListener() {
        @Override
        public void onAddPicClick() {
            PictureSelector.create(EvaluateActivity.this)
                    .openGallery(chooseMode)
                    .theme(themeId)
                    .maxSelectNum(9)
                    .minSelectNum(1)
                    .imageEngine(GlideEngine.createGlideEngine())
                    .selectionMode(PictureConfig.MULTIPLE)
                    .isCamera(true)
                    .isMaxSelectEnabledMask(true)
                    .imageSpanCount(4)
                    .isCompress(true)
                    .selectionData(pjImageAdapter.getData())
                    .minimumCompressSize(100)
                    .forResult(PictureConfig.CHOOSE_REQUEST);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择
                    pjurls = (ArrayList<LocalMedia>) PictureSelector.obtainMultipleResult(data);
                    pjImageAdapter.setList(pjurls);
                    pjImageAdapter.notifyDataSetChanged();
//                    for(int i=0;i<PictureSelector.obtainMultipleResult(data).size();i++){
//                        File file= new File(PictureSelector.obtainMultipleResult(data).get(i).getCompressPath());
//                        mPresenter.uploadimg(file);
//                    }
                    break;
            }
        }
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_evalute;
    }

}
