package com.twan.kotlinbase.ui

import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.RechaegeHistory
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.databinding.ItemRechargeHistoryBinding
import com.twan.kotlinbase.network.RxHttpScope
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class CopyActivity : BaseDataBindingActivity<ActivityCopyBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_copy
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text=""
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
    }

    //RechargeHistoryActivity
//    fun initRv(){
//        refreshLayout.setRefreshHeader(ClassicsHeader(this))
//        refreshLayout.setRefreshFooter(ClassicsFooter(this))
//        refreshLayout.setOnRefreshListener { getData() }
//        refreshLayout.setEnableLoadMore(true)
//        refreshLayout.setEnableRefresh(true)
//        refreshLayout.setOnLoadMoreListener { getData() }
//        rv_history.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
//        rv_history.adapter=object : BaseQuickAdapter<RechaegeHistory, BaseDataBindingHolder<ItemRechargeHistoryBinding>>(R.layout.item_my_fav){
//            override fun convert(holder: BaseDataBindingHolder<ItemRechargeHistoryBinding>, item: RechaegeHistory) {
//                holder.dataBinding!!.item=item
//            }
//
//        }.also {
//              mAdpater=it
//            getData()
//        }
//    }
//
//    var currentPage=1
//    lateinit var mAdpater:BaseQuickAdapter<RechaegeHistory, BaseDataBindingHolder<ItemRechargeHistoryBinding>>
//    fun getData(isRefresh:Boolean=true){
//        if (isRefresh) currentPage=1
//        RxHttpScope().launch {
//            var req= RxHttp.postJson("log/getAddCreditLog")
//                    .add("pageSize",20)
//                    .add("currentPage",currentPage++)
//                    .toResponse<PageData<RechaegeHistory>>().await()
//            if (currentPage==2) mAdpater.setList(req.content)
//            else  mAdpater.addData(req.content)
//        }
//    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    fun event(event: String){
//
//    }
}