package com.twan.kotlinbase.ui

import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_settle_dresser1.*
import kotlinx.android.synthetic.main.activity_settle_dresser1.chb_privacy
import kotlinx.android.synthetic.main.fragment_register.*

//服务商 入驻
class SettleServiceActivity1 : BaseDataBindingActivity<ActivitySettleDresser1Binding>(){
    override fun getLayout(): Int {
        return R.layout.activity_settle_dresser1  //暂时用一样的, 只是规则不一样
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="企业入驻"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        tv_settle_order.text="企业入驻流程"
    }

    @OnClick(R.id.btn_ok)
    fun next(){
        if (!chb_privacy.isChecked){
            ToastUtils.showShort("请同意小肚驴入驻协议")
            return
        }
        Router.newIntent(this).to(SettleServiceActivity2::class.java).launch()
    }

    @OnClick(R.id.chbGZ)
    fun go(){
        Router.newIntent(mContext)
            .putString("url","https://api.xiaodulv6.com/protocol/p3")
            .putString("title","入驻规则")
            .to(WebActivity::class.java).launch()
    }

}