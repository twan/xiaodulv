package com.twan.kotlinbase.ui

import android.view.View
import android.widget.ImageView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.bean.DresserParam
import com.twan.kotlinbase.bean.ServiceProParam
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_settle_service2.*

//直播达人 入驻2
class SettleServiceActivity2 : BaseDataBindingActivity<ActivitySettleDresser1Binding>(){
    lateinit var currImageView: ImageView
    lateinit var mShanchang:Dict
    override fun getLayout(): Int {
        return R.layout.activity_settle_service2
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="企业入驻"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        initRvMultiPicOrVideo(rv_multi_pic,1)
        initRvMultiPicOrVideo(rv_multi_video,2)
        initRvMultiPicOrVideo(rv_multi_anli,4)
        //头像
        rl_avater.setOnClickListener {
            uploadType=3
            currImageView=iv_avater
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        //擅长类型
        SpiUtils.setSpi(spi_shanchang, DictUtils.getDictCache(DictUtils.short_video_type)){ pos, dict ->
            mShanchang=dict
        }

        btn_ok.setOnClickListener {
            if (uploadPics.size==1) {
                ToastUtils.showShort("请上传图片")
                return@setOnClickListener
            }
            if (uploadVideos.size==1) {
                ToastUtils.showShort("请上传视频")
                return@setOnClickListener
            }
            if (uploadAnlis.size==1){
                ToastUtils.showShort("请上传案例图片")
                return@setOnClickListener
            }
            if (!InputUtils.checkEmpty(edt_adv_name,edt_produce)){
                return@setOnClickListener
            }

            var serviceProParam = ServiceProParam()
            serviceProParam.avatarUlr=iv_avater.getUrl()
            serviceProParam.companyName=edt_adv_name.text.toString()
            serviceProParam.selfIntroduction=edt_produce.text.toString()
            serviceProParam.goodAtValue=mShanchang.id.toString()
            serviceProParam.goodAtLabel=mShanchang.label
            serviceProParam.imageUrl=getPicsUrl()
            serviceProParam.videoUrl=getVideosUrl()
            serviceProParam.caseUrl=getAnliUrl()

            Router.newIntent(this).putSerializable("serviceProParam",serviceProParam).to(SettleServiceActivity3::class.java).launch()
        }
    }

    override fun uploadCallback(url:String){
        super.uploadCallback(url)
        if (uploadType == 1){
            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==2){ //选择了视频就清空图片
            uploadVideos.add(UploadPic(filepath=url))
            rv_multi_video.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==4) {
            uploadAnlis.add(UploadPic(filepath=url))
            rv_multi_anli.adapter!!.notifyDataSetChanged()
        } else { //单个的图片或者视频
            if (::currImageView.isInitialized){
                currImageView.tag=url
                GlideUtils.load(this,currImageView,url)
            }
        }
    }

}