package com.twan.kotlinbase.ui

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.skydoves.powerspinner.PowerSpinnerView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.*
import com.twan.kotlinbase.databinding.ActivityOrderDetailBinding
import com.twan.kotlinbase.databinding.ActivitySettleDresser1Binding
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_settle_videoext2.*
import kotlinx.android.synthetic.main.activity_settle_videoext2.btn_ok
import kotlinx.android.synthetic.main.activity_settle_videoext2.edt_adv_name
import kotlinx.android.synthetic.main.activity_settle_videoext2.edt_produce
import kotlinx.android.synthetic.main.activity_settle_videoext2.rl_avater

//视频推广 入驻2
class SettleVideoExtActivity2 : BaseDataBindingActivity<ActivitySettleDresser1Binding>(){
    lateinit var mStyle : Dict
    lateinit var mShanchang : Dict
    lateinit var currImageView: ImageView
    var platforms = mutableListOf<Platform>()
    override fun getLayout(): Int {
        return R.layout.activity_settle_videoext2
    }

    override fun initEventAndData() {
        title?.visibility= View.VISIBLE
        title?.text="短视频推广入驻"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        initRvMultiPicOrVideo(rv_multi_pic,1)
        initRvMultiPicOrVideo(rv_multi_video,2)
        //头像
        rl_avater.setOnClickListener {
            uploadType=3
            currImageView=iv_avater
            XPopup.Builder(this).asCustom(AddPicPopup(this)).show()
        }
        //擅长类目
        SpiUtils.setSpi(spi_shanchang, DictUtils.getDictCache(DictUtils.short_video_type)){ pos, dict ->
            mShanchang=dict
        }
        //拍摄风格
        SpiUtils.setSpi(spi_take_style, DictUtils.getDictCache(DictUtils.shooting_style)){ pos, dict ->
            mStyle=dict
        }

        btn_ok.setOnClickListener {
            if (uploadPics.size==1) {
                ToastUtils.showShort("请上传图片")
                return@setOnClickListener
            }
            if (uploadVideos.size==1) {
                ToastUtils.showShort("请上传视频")
                return@setOnClickListener
            }
            if (!InputUtils.checkEmpty(edt_adv_name,edt_produce)){
                return@setOnClickListener
            }
            if (!checkPlatform()){
                return@setOnClickListener
            }

            var shortVideoExtParam = ShortVideoExtParam()
            shortVideoExtParam.avatarUlr=iv_avater.getUrl()
            shortVideoExtParam.nickName=edt_adv_name.text.toString()
            shortVideoExtParam.selfIntroduction=edt_produce.text.toString()
            shortVideoExtParam.imageUrl=getPicsUrl()
            shortVideoExtParam.videoUrl=getVideosUrl()
            shortVideoExtParam.goodAtValue=mShanchang.id.toString()
            shortVideoExtParam.goodAtLabel=mShanchang.label
            shortVideoExtParam.shootStyleValue=mStyle.id.toString()
            shortVideoExtParam.shootStyleLabel=mStyle.label
            shortVideoExtParam.platforms=platforms

            Router.newIntent(this).putSerializable("shortVideoExtParam",shortVideoExtParam).to(SettleVideoExtActivity3::class.java).launch()
        }
    }

    override fun uploadCallback(url:String){
        if (uploadType == 1){
            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==2){ //选择了视频就清空图片
            uploadVideos.add(UploadPic(filepath=url))
            rv_multi_video.adapter!!.notifyDataSetChanged()
        } else { //单个的图片或者视频
            if (::currImageView.isInitialized){
                currImageView.tag=url
                GlideUtils.load(this,currImageView,url)
            }
        }
    }

    var viewList= mutableListOf<View>()
    @OnClick(R.id.tv_add)
    fun addPaltform(){
        var view = layoutInflater.inflate(R.layout.add_video_ext,ll_container,false)
        var iv_del = view.findViewById<ImageView>(R.id.iv_del)
        var tv_name = view.findViewById<TextView>(R.id.tv_name)

        var spi_platform1 = view.findViewById<PowerSpinnerView>(R.id.spi_platform1)

        //平台
        SpiUtils.setSpi(spi_platform1, DictUtils.getDictCache(DictUtils.short_video_platform)){ pos, dict ->
            spi_platform1.tag=dict
        }
        viewList.add(view)
        tv_name.text="平台${viewList.size}"
        ll_container.addView(view)
        iv_del.setOnClickListener {
            viewList.remove(view)
            ll_container.removeView(view)
            updateName()
        }

    }

    fun updateName(){
        viewList.forEachIndexed { index, view ->
            var tv_name = view.findViewById<TextView>(R.id.tv_name)
            tv_name.text="平台${index+1}"
        }
    }

    fun checkPlatform():Boolean{
        viewList.forEachIndexed { index, view ->
            var spi_platform1 = view.findViewById<PowerSpinnerView>(R.id.spi_platform1)
            var edt_fans1 = view.findViewById<EditText>(R.id.edt_fans1)
            var edt_account1 = view.findViewById<EditText>(R.id.edt_account1)
            var edt_price1 = view.findViewById<EditText>(R.id.edt_price1)
            if (!InputUtils.checkEmpty(edt_fans1,edt_account1,edt_price1)) return false

            var platform = Platform()
            platform.cooperationPrice=edt_price1.text.toString()
            platform.fansNum=edt_fans1.text.toString()
            platform.platformAccount=edt_account1.text.toString()
            platform.platformLabel=(spi_platform1.tag as Dict).label
            platform.platformValue=(spi_platform1.tag as Dict).id.toString()
            platforms.add(platform)
        }
        return true
    }

}