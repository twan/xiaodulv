package com.twan.kotlinbase.ui

import android.view.View
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityPrivacyBinding


class PrivacyActivity : BaseDataBindingActivity<ActivityPrivacyBinding>(){

    override fun getLayout(): Int {
        return R.layout.activity_privacy
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="隐私设置"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
    }


}