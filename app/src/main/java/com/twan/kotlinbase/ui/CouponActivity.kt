package com.twan.kotlinbase.ui

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.BottomViewPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.databinding.ActivityCouponBinding
import com.twan.kotlinbase.fragment.CouponFragment
import kotlinx.android.synthetic.main.activity_order.*

//优惠券
class CouponActivity : BaseDataBindingActivity<ActivityCouponBinding>(){
    val tabs = mutableListOf("未使用","已使用","已过期")
    override fun getLayout(): Int {
        return R.layout.activity_coupon
    }

    override fun initEventAndData() {
        whiteStatusBar()
        title?.visibility= View.VISIBLE
        title?.text="我的优惠券"
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""

        setupViewPager()
        initTab()
    }

    //3个tab都是列表,这里共用一个fragment
    private fun setupViewPager() {
        var fragments= mutableListOf<Fragment>()
        fragments.add(CouponFragment("0"))//状态：0未使用，1已使用，2过期
        fragments.add(CouponFragment("1"))
        fragments.add(CouponFragment("2"))
        var adapter = BottomViewPagerAdapter(this,fragments)
        view_pager.adapter = adapter
    }

    fun initTab(){
        TabLayoutMediator(tablayout,view_pager) { tab, position ->
            tab.text=tabs[position]
        }.attach()
    }

}