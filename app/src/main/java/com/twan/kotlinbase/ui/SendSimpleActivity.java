package com.twan.kotlinbase.ui;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.blankj.utilcode.util.SpanUtils;
import com.gyf.immersionbar.ImmersionBar;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.bean.OrderDetailBean;
import com.twan.kotlinbase.pop.ExpressNoPop;
import com.twan.kotlinbase.ui.presenter.NewOrderDetailPresenter;
import com.twan.kotlinbase.ui.view.NewOrderDetailView;
import com.twan.kotlinbase.util.UIUtils;
import com.twan.kotlinbase.widgets.RoundImageView5;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @anthor zhoujr
 * @time 2021/4/14 10:24
 * @describe
 */
public class SendSimpleActivity extends BaseActivity {
    @BindView(R.id.sendsimpleReceiveContact)
    TextView sendsimpleReceiveContact;
    @BindView(R.id.sendsimpleReceiveAddress)
    TextView sendsimpleReceiveAddress;
    @BindView(R.id.sendsimpleCopy)
    TextView sendsimpleCopy;
    @BindView(R.id.sendsimplePressExpressNo)
    TextView sendsimplePressExpressNo;
    @BindView(R.id.sendsimplePressExpress)
    TextView sendsimplePressExpress;
    @BindView(R.id.sendsimpleIcon)
    RoundImageView5 sendsimpleIcon;
    @BindView(R.id.sendsimpleTitle)
    TextView sendsimpleTitle;
    @BindView(R.id.sendsimplePrice)
    TextView sendsimplePrice;
    @BindView(R.id.sendsimpleOrderDate)
    TextView sendsimpleOrderDate;
    @BindView(R.id.sendsimpleOrderNo)
    TextView sendsimpleOrderNo;
    @BindView(R.id.sendsimpleRemark)
    TextView sendsimpleRemark;
    @BindView(R.id.sendsimpleFinish)
    Button sendsimpleFinish;
    ExpressNoPop expressNoPop;

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("寄送样品");
        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.white));
        ImmersionBar.with(this)
                .statusBarColor(R.color.white)
                .statusBarDarkFont(true)
                .flymeOSStatusBarFontColor(R.color.black)
                .init();
        expressNoPop = new ExpressNoPop(this);
        expressNoPop.setConfimPress(new ExpressNoPop.ConfimPress() {
            @Override
            public void confim(String expressno) {
                UIUtils.showToast(expressno);
            }
        });
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @OnClick({R.id.sendsimpleCopy,R.id.sendsimplePressExpressNo,R.id.sendsimplePressExpress,R.id.sendsimpleFinish})
    public void click(View view){
        switch (view.getId()){
            case R.id.sendsimpleFinish:
                break;
            case R.id.sendsimpleCopy:
                break;
            case R.id.sendsimplePressExpressNo:
            case R.id.sendsimplePressExpress:
                expressNoPop.showPopupWindow();
                break;
        }
    }


    @Override
    protected int provideContentViewId() {
        return R.layout.activity_sendsimple;
    }

}
