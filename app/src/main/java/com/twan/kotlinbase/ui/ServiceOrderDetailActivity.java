package com.twan.kotlinbase.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twan.kotlinbase.R;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.widgets.RoundImageView;
import com.twan.kotlinbase.widgets.RoundImageView5;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.iwgang.countdownview.CountdownView;

/**
 * @anthor zhoujr
 * @time 2021/4/17 15:18
 * 卖家订单详情
 * @describe
 */
public class ServiceOrderDetailActivity extends BaseActivity {
    @BindView(R.id.orderDetailLl)
    LinearLayout orderDetailLl;
    @BindView(R.id.orderDetailCountDown)
    CountdownView orderDetailCountDown;
    @BindView(R.id.countdownLL)
    LinearLayout countdownLL;
    @BindView(R.id.imageview)
    ImageView imageview;
    @BindView(R.id.orderDetailName)
    TextView orderDetailName;
    @BindView(R.id.orderDetailMobile)
    TextView orderDetailMobile;
    @BindView(R.id.orderDetailAddress)
    TextView orderDetailAddress;
    @BindView(R.id.orderDetailCopy)
    TextView orderDetailCopy;
    @BindView(R.id.addressLL)
    LinearLayout addressLL;
    @BindView(R.id.orderDetailServiceIcon)
    RoundImageView orderDetailServiceIcon;
    @BindView(R.id.orderDetailServiceName)
    TextView orderDetailServiceName;
    @BindView(R.id.orderDetailIcon)
    RoundImageView5 orderDetailIcon;
    @BindView(R.id.orderDetailTitle)
    TextView orderDetailTitle;
    @BindView(R.id.orderDetailPrice)
    TextView orderDetailPrice;
    @BindView(R.id.orderDetailDesc)
    TextView orderDetailDesc;
    @BindView(R.id.orderDetailCount)
    TextView orderDetailCount;
    @BindView(R.id.orderDetailRealPay)
    TextView orderDetailRealPay;
    @BindView(R.id.orderDetailOrderNo)
    TextView orderDetailOrderNo;
    @BindView(R.id.orderDetailOrderNoCopy)
    TextView orderDetailOrderNoCopy;
    @BindView(R.id.orderDetailCreateDate)
    TextView orderDetailCreateDate;
    @BindView(R.id.orderDetailContact)
    TextView orderDetailContact;

    @Override
    public void initView() {
        super.initView();
        setActivityTitle("待收样品");
        setToolBarImgAndBg(R.mipmap.back_left, getResources().getColor(R.color.head_bg));
        countdownLL.setVisibility(View.GONE);
        orderDetailContact.setText("联系买家");

        //确认收到样品
//        View view = LayoutInflater.from(this).inflate(R.layout.view_receivesimple,null);
//        view.findViewById(R.id.btn_simple).setOnClickListener(onClickListener);
//        orderDetailLl.addView(view);

        //上传/编辑作品
        View view = LayoutInflater.from(this).inflate(R.layout.view_uploadproject,null);
        view.findViewById(R.id.btn_upload).setOnClickListener(onClickListener);
        orderDetailLl.addView(view);
    }


    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btn_simple:
                    //确认收到样品
                    break;
                case R.id.btn_upload:
                    //上传/编辑作品
                    jumpToActivity(UploadProjectActivity.class);
                    break;
            }
        }
    };

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.activity_serviceorderdetail;
    }

}
