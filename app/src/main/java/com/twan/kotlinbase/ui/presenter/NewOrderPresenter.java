package com.twan.kotlinbase.ui.presenter;

import com.twan.kotlinbase.api.ApiRetrofit;
import com.twan.kotlinbase.app.EnumOrderStatus;
import com.twan.kotlinbase.base.BaseActivity;
import com.twan.kotlinbase.base.BasePresenter;
import com.twan.kotlinbase.bean.AliPayBean;
import com.twan.kotlinbase.bean.CancelOrderBean;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.bean.WeChatPayBean;
import com.twan.kotlinbase.factory.ApiErrorHelper;
import com.twan.kotlinbase.factory.BaseSubscriber;
import com.twan.kotlinbase.ui.view.NewOrderView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @anthor zhoujr
 * @time 2021/4/13 13:26
 * @describe
 */
public class NewOrderPresenter extends BasePresenter<NewOrderView> {
    public NewOrderPresenter(BaseActivity context) {
        super(context);
    }

    public void getOrderLst(int page, String status) {
        ApiRetrofit.getInstance().getOrderLst(status,page+"","buyerId")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<OrderBean>(mContext){
                    @Override
                    public void onNext(OrderBean orderBean) {
                        getView().getOrderLstSucc(orderBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }

    public void confimreceive(String orderId) {
        mContext.showWaitingDialog("");
        ApiRetrofit.getInstance().confimreceive(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<String>(mContext){
                    @Override
                    public void onNext(String s) {
                        mContext.hideWaitingDialog();
                        getView().confimreceiveSucc();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }

    public void cancelOrder(String orderId) {
        mContext.showWaitingDialog("");
        ApiRetrofit.getInstance().cancelOrder(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<CancelOrderBean>(mContext){
                    @Override
                    public void onNext(CancelOrderBean cancelOrderBean) {
                        mContext.hideWaitingDialog();
                        getView().cancelSucc();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }

    public void deleteOrder(String orderId) {
        mContext.showWaitingDialog("");
        ApiRetrofit.getInstance().deleteOrder(orderId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<CancelOrderBean>(mContext){
                    @Override
                    public void onNext(CancelOrderBean cancelOrderBean) {
                        mContext.hideWaitingDialog();
                        getView().deleteSucc();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }

    public void getALiPayData(String orderId, String totalFee) {
        mContext.showWaitingDialog("");
        ApiRetrofit.getInstance().getalipaydata(orderId,totalFee)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<AliPayBean>(mContext){
                    @Override
                    public void onNext(AliPayBean aliPayBean) {
                        mContext.hideWaitingDialog();
                        getView().getAliPayDataSucc(aliPayBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }

    public void getWeChatPayData(String orderId, String totalFee) {
        mContext.showWaitingDialog("");
        ApiRetrofit.getInstance().getwechatpaydata(orderId,totalFee)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseSubscriber<WeChatPayBean>(mContext){
                    @Override
                    public void onNext(WeChatPayBean weChatPayBean) {
                        mContext.hideWaitingDialog();
                        getView().getWeChatPayDataSucc(weChatPayBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ApiErrorHelper.handleCommonError(context_, e);
                    }
                });
    }
}
