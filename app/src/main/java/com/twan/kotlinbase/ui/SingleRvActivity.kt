package com.twan.kotlinbase.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.SPUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingActivity
import com.twan.kotlinbase.bean.EnumSingleRv
import com.twan.kotlinbase.bean.MyFavorFoot
import com.twan.kotlinbase.bean.MyFocus
import com.twan.kotlinbase.databinding.ActivityCopyBinding
import com.twan.kotlinbase.databinding.ActivitySingleRvBinding
import com.twan.kotlinbase.databinding.ItemMyFavBinding
import com.twan.kotlinbase.databinding.ItemMyFocusBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_single_rv.*
import kotlinx.android.synthetic.main.activity_single_rv.refreshLayout
import kotlinx.android.synthetic.main.tab1_fragment_user.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse


class SingleRvActivity : BaseDataBindingActivity<ActivitySingleRvBinding>(){
    lateinit var mTitle:EnumSingleRv
    lateinit var mFavAdapter:BaseQuickAdapter<MyFavorFoot, BaseDataBindingHolder<ItemMyFavBinding>>
    lateinit var mFocusAdapter:BaseQuickAdapter<MyFocus, BaseDataBindingHolder<ItemMyFocusBinding>>
    override fun getLayout(): Int {
        return R.layout.activity_single_rv
    }

    override fun initEventAndData() {
        whiteStatusBar()
        mTitle= intent.getSerializableExtra("data") as EnumSingleRv
        title?.visibility= View.VISIBLE
        title?.text=mTitle.desc
        tv_right?.visibility = View.VISIBLE
        tv_right?.text=""
        initRv()
    }

    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(this))
        refreshLayout.setRefreshFooter(ClassicsFooter(this))
        refreshLayout.setOnRefreshListener {
            currentPage=1
            getData()
        }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        if (mTitle.id==EnumSingleRv.MY_FAV.id) {
            single_rv.adapter = object : BaseQuickAdapter<MyFavorFoot, BaseDataBindingHolder<ItemMyFavBinding>>(R.layout.item_my_fav) {
                override fun convert(holder: BaseDataBindingHolder<ItemMyFavBinding>, item: MyFavorFoot) {
                    holder.dataBinding!!.item=item
                    GlideUtils.load(this@SingleRvActivity,holder.dataBinding!!.ivAvater,item.imagesUrl)
                }

            }.also {
                mFavAdapter=it
                getData()
            }
        }
        if (mTitle.id==EnumSingleRv.MY_FOOTPRINT.id) {
            single_rv.adapter = object : BaseQuickAdapter<MyFavorFoot, BaseDataBindingHolder<ItemMyFavBinding>>(R.layout.item_my_fav) {
                override fun convert(holder: BaseDataBindingHolder<ItemMyFavBinding>, item: MyFavorFoot) {
                    holder.dataBinding!!.item=item
                    GlideUtils.load(this@SingleRvActivity,holder.dataBinding!!.ivAvater,item.imagesUrl)
                }

            }.also {
                mFavAdapter=it
                getData()
            }
        }
        if (mTitle.id==EnumSingleRv.MY_FOCUS.id) {
            single_rv.adapter = object : BaseQuickAdapter<MyFocus, BaseDataBindingHolder<ItemMyFocusBinding>>(R.layout.item_my_focus) {
                override fun convert(holder: BaseDataBindingHolder<ItemMyFocusBinding>, item: MyFocus) {
                    holder.dataBinding!!.item=item
                    GlideUtils.load(this@SingleRvActivity,holder.dataBinding!!.ivAvater,item.imagesUrl)
                }

            }.also {
                mFocusAdapter=it
                getData()
            }
        }


    }

    var currentPage=1
    fun getData(){
        if (mTitle.id==EnumSingleRv.MY_FAV.id || mTitle.id==EnumSingleRv.MY_FOOTPRINT.id) {
            RxHttpScope(this, refreshLayout).launch {
                var req = RxHttp.get("/favorites/findByUserId").add("userId", MyUtils.getUserid())
                        .add("pageSize",20)
                        .add("currentPage",currentPage++)
                        .toResponse<List<MyFavorFoot>>().await()
                if (currentPage==2) mFavAdapter.setList(req) else  mFavAdapter.addData(req)
            }
        }
        if (mTitle.id==EnumSingleRv.MY_FOCUS.id) {
            RxHttpScope(this, refreshLayout).launch {
                var req = RxHttp.get("/attention/findByUserId").add("userId", MyUtils.getUserid())
                        .add("pageSize",20)
                        .add("currentPage",currentPage++)
                        .toResponse<List<MyFocus>>().await()
                if (currentPage==2) mFocusAdapter.setList(req) else  mFocusAdapter.addData(req)
            }
        }
        if (mTitle.id==EnumSingleRv.MY_FOOTPRINT.id) {
            RxHttpScope(this, refreshLayout).launch {
                var req = RxHttp.get("/footprint/getByCondition").add("userId", MyUtils.getUserid())
                        .add("pageSize",20)
                        .add("currentPage",currentPage++)
                        .toResponse<List<MyFocus>>().await()
                if (currentPage==2) mFocusAdapter.setList(req) else  mFocusAdapter.addData(req)
            }
        }
    }
}