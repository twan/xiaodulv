package com.twan.kotlinbase.network

import rxhttp.wrapper.annotation.DefaultDomain

object Api {
    @DefaultDomain //设置为默认域名
    const val baseUrl = "https://api.xiaodulv6.com/api/"//正式
//    const val baseUrl = "https://xiaodulv.jomokeji.com/api/"//测试
}