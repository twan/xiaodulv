package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.jaeger.library.StatusBarUtil
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.Tab2Dynaic
import com.twan.kotlinbase.databinding.FragmentServiceBinding
import com.twan.kotlinbase.databinding.ItemSpDynaicBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import kotlinx.android.synthetic.main.fragment_service.*
import kotlinx.android.synthetic.main.layout_common_header.*
import kotlinx.android.synthetic.main.tab1_fragment_user.refreshLayout
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

// 和 ServiceFragment 一样
class Tab2SpFragment: BaseDataBindingFragment<FragmentServiceBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.fragment_service
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        StatusBarUtil.setColor(mActivity, resources.getColor(R.color.head_bg), 0)
        toolbar?.visibility=View.VISIBLE
        title?.visibility=View.VISIBLE
        title?.text="发现"
        back?.visibility=View.GONE
        searchView?.visibility=View.GONE //一期隐藏
        tv_back_text?.visibility=View.GONE //一期隐藏
        tv_back_text?.text="请选择"
        tv_back_text?.textSize=16f

        initRv()
    }


    lateinit var mAdapter: BaseQuickAdapter<Tab2Dynaic, BaseDataBindingHolder<ItemSpDynaicBinding>>
    var currentPage=1
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener {
            currentPage=1
            getData()
        }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        rv_service.adapter=object : BaseQuickAdapter<Tab2Dynaic, BaseDataBindingHolder<ItemSpDynaicBinding>>(R.layout.item_sp_dynaic){
            override fun convert(holder: BaseDataBindingHolder<ItemSpDynaicBinding>, item: Tab2Dynaic) {
                holder.dataBinding!!.item=item
                holder.dataBinding!!.progress1.progress=50
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.imageUrl)
            }

        }.also {
            mAdapter=it
            //it.setOnItemClickListener { adapter, view, position -> Router.newIntent(mActivity).to(ServiceDetailActivity::class.java).launch()  }
            getData()
        }
    }

    fun getData(){
        RxHttpScope(mActivity,refreshLayout).launch {
            var req= RxHttp.get("/demand/multiple/getByCondition")
                    .add("pageSize",20)
                    .add("currentPage",currentPage++)
                    .toResponse<PageData<Tab2Dynaic>>().await()
            if (currentPage==2) mAdapter.setList(req.content)
            else  mAdapter.addData(req.content)
        }
    }

}