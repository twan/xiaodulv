package com.twan.kotlinbase.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import butterknife.BindView
import butterknife.OnClick
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.SPUtils
import com.blankj.utilcode.util.ToastUtils
import com.scwang.smart.refresh.header.ClassicsHeader
import com.scwang.smart.refresh.layout.SmartRefreshLayout
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Role
import com.twan.kotlinbase.bean.CatoeryNums
import com.twan.kotlinbase.bean.EnumSingleRv
import com.twan.kotlinbase.bean.SetDefaultBean
import com.twan.kotlinbase.bean.SettleStatus
import com.twan.kotlinbase.databinding.Tab4FragmentBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.*
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.tab4_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class Tab4Fragment: BaseDataBindingFragment<Tab4FragmentBinding>() {
    @BindView(R.id.refreshLayout) lateinit var refreshLayout:SmartRefreshLayout
    lateinit var settleStatus:SettleStatus
    override fun getLayoutId(): Int {
        return R.layout.tab4_fragment
    }

    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setOnRefreshListener { reqData() }
    }


    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = resources.getText(R.string.tab4)
        back?.visibility=View.GONE
        LogUtils.e("当前角色:${MyUtils.getRole().ordinal}")
        tv_switch_role.text= if (MyUtils.getRole().ordinal==1) "切换为用户" else "切换为服务商"
        event(MyUtils.getRole())
        initRv()
    }

    override fun onResume() {
        super.onResume()
        if(MyUtils.isLogin()){
            tv_name.text=MyUtils.getLoginInfo()?.userName
            reqData()
        } else {
            tv_name.text="请登录"
        }
    }

    //切换角色
    @OnClick(R.id.tv_switch_role)
    fun switchRole(){
        if (MyUtils.isLogin()) {
            if(MyUtils.getRole().ordinal == 1){
                //服务商切换回用户不需要判断是否认证
                MyUtils.setRole(if (MyUtils.getRole().ordinal == 1) 0 else 1)
                tv_switch_role.text = if (MyUtils.getRole().ordinal == 1) "切换为用户" else "切换为服务商"
                EventBus.getDefault().post(MyUtils.getRole())
            }else{
                //0认证中 1认证成功 -1认证失败
                if(!::settleStatus.isInitialized){
                    ToastUtils.showShort("请先认证")
                }else{
                    if (settleStatus.companySettle =="1" || settleStatus.personalSettle=="1"){
                        MyUtils.setRole(if (MyUtils.getRole().ordinal == 1) 0 else 1)
                        tv_switch_role.text = if (MyUtils.getRole().ordinal == 1) "切换为用户" else "切换为服务商"
                        EventBus.getDefault().post(MyUtils.getRole())
                    } else {
                        ToastUtils.showShort("请先认证")
                    }
                }
            }



            //Router.newIntent(mActivity).putInt("tab",0).to(MainActivity::class.java).launch()
        } else {
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }

    @OnClick(R.id.iv_avater,R.id.ll_person)
    fun toLogin(){
        if (MyUtils.isLogin()){

        } else {
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun event(event: Role){
        tv_account.visibility=if (event.ordinal==1) View.VISIBLE else View.GONE
        //ll_my_favor.visibility=if (event.ordinal==1) View.GONE else View.VISIBLE
        rl_sp_manage.visibility=if (event.ordinal==1) View.VISIBLE else View.GONE
        rl_settle.visibility=if (event.ordinal==1) View.GONE else View.VISIBLE
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    fun setdefault(event: SetDefaultBean){
        MyUtils.setRole(0)
        tv_switch_role.text= if (MyUtils.getRole().ordinal==1) "切换为用户" else "切换为服务商"
        event(MyUtils.getRole())
    }


    @OnClick(R.id.rl_order_head)
    fun orderList(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(NewOrderActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }

    @OnClick(R.id.tv_invite)
    fun invite(){
        if (MyUtils.isLogin()) {
            Router.newIntent(mActivity).to(InviteFriendActivity::class.java).launch()
        } else {
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }

    @OnClick(R.id.iv_setting)
    fun setting(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(SettingActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }

    @OnClick(R.id.tv_feedback)
    fun feedback(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(SuggestActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }

    @OnClick(R.id.tv_account)
    fun account(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(WalletActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.rl_my_fav)
    fun myFav(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).putSerializable("data",EnumSingleRv.MY_FAV).to(SingleRvActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.rl_my_focus)
    fun myFocus(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).putSerializable("data",EnumSingleRv.MY_FOCUS).to(SingleRvActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.rl_my_footprint)
    fun myFootprint(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).putSerializable("data",EnumSingleRv.MY_FOOTPRINT).to(SingleRvActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.rl_coupon)
    fun myCoupon(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(MyCouponActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //服务商入驻
    @OnClick(R.id.rl_settle_service)
    fun settleService(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(SettleServiceActivity1::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //直播达人入驻
    @OnClick(R.id.rl_settle_dresser)
    fun settleDresser(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(SettleDresserActivity1::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //短视频推广达人入驻
    @OnClick(R.id.rl_settle_ext)
    fun settleExt(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(SettleVideoExtActivity1::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //演员/模特入驻
    @OnClick(R.id.rl_settle_model_actor)
    fun settleModel(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(SettleActorModelActivity1::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //服务管理
    @OnClick(R.id.rl_service_manage)
    fun serviceManage(){
        if (MyUtils.isLogin()) {
            Router.newIntent(mActivity).to(ServiceManageActivity::class.java).launch()
        } else {
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //待付款
    @OnClick(R.id.rl_will_pay)
    fun willpay(){
//        Router.newIntent(mActivity).putInt("index",1).to(NewOrderActivity::class.java).launch()
        if (MyUtils.isLogin()){
//            Router.newIntent(mActivity).putInt("index",1).to(NewOrderActivity::class.java).launch()
            Router.newIntent(mActivity).putInt("index",1).to(ServiceOrderActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //待发货
    @OnClick(R.id.rl_will_send)
    fun willSend(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).putInt("index",2).to(NewOrderActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //待收货
    @OnClick(R.id.rl_will_receive)
    fun willReceive(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).putInt("index",3).to(NewOrderActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //待评价
    @OnClick(R.id.rl_will_evalute)
    fun willEvalute(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).putInt("index",5).to(NewOrderActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //待退款
    @OnClick(R.id.rl_will_refund)
    fun willRefund(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).putInt("index",8).to(NewOrderActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.tv_address_manage)
    fun address(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(AddressManageActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.tv_bond)
    fun bond(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(BondActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.tv_dynaimc)
    fun dynaimc(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(DynamicManageActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    @OnClick(R.id.tv_xuqiu)
    fun xuqiu(){
        if (MyUtils.isLogin()){
            Router.newIntent(mActivity).to(XuqiuManageActivity::class.java).launch()
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }

    /**
     * 联系客服
     */
    @OnClick(R.id.tv_Contect)
    fun click(){
        if (MyUtils.isLogin()){
            //填入IM账号
            MyUtils.chat(mActivity,"388767835192561664","客服")
        }else{
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }

    }


    //企业入驻, 就是服务商入驻
    @OnClick(R.id.rl_settle_qiye)
    fun qiye(){
        if (MyUtils.isLogin()) {
            //0认证中 1认证成功 -1认证失败  2未认证
            if(!::settleStatus.isInitialized){
                Router.newIntent(mActivity).to(SettleServiceActivity1::class.java).launch()
            }else{
                if (::settleStatus.isInitialized && settleStatus.companySettle =="0") {
                    ToastUtils.showShort("正在审核中")
                } else if (::settleStatus.isInitialized && settleStatus.companySettle =="1"){
                    ToastUtils.showShort("您已完成企业入驻，无须重复入驻")
                } else if(::settleStatus.isInitialized && settleStatus.companySettle =="-1"){
                    ToastUtils.showShort("认证失败请重新认证")
                    Router.newIntent(mActivity).to(SettleServiceActivity1::class.java).launch()
                }else {
                    Router.newIntent(mActivity).to(SettleServiceActivity1::class.java).launch()
                }
            }


        } else {
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }
    //个人入驻, 就是直播达人入驻
    @OnClick(R.id.rl_settle_person)
    fun person(){
//        if (MyUtils.isLogin()){
//            Router.newIntent(mActivity).to(SettleDresserActivity1::class.java).launch()
//        }else{
//            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
//        }
        if (MyUtils.isLogin()) {
            //0认证中 1认证成功 -1认证失败 2未认证
            if(!::settleStatus.isInitialized){
                Router.newIntent(mActivity).to(SettleDresserActivity1::class.java).launch()
            }else{
                if (::settleStatus.isInitialized && settleStatus.personalSettle =="0") {
                    ToastUtils.showShort("正在审核中")
                } else if (::settleStatus.isInitialized && settleStatus.personalSettle =="1"){
                    ToastUtils.showShort("您已完成个人入驻，无须重复入驻")
                } else if(::settleStatus.isInitialized && settleStatus.personalSettle =="-1"){
                    ToastUtils.showShort("认证失败请重新认证")
                    Router.newIntent(mActivity).to(SettleDresserActivity1::class.java).launch()
                }else {
                    Router.newIntent(mActivity).to(SettleDresserActivity1::class.java).launch()
                }
            }


        } else {
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }

    override fun reqData(){
        RxHttpScope(mActivity,refreshLayout).launch {
            var req=RxHttp.get("center/findByUserId").add("userId",MyUtils.getUserid()).toResponse<CatoeryNums>().await()
            mBinding.item=req

            //验证是否可申请
            var req2= RxHttp.get("talent/validateRepeat").add("userId",MyUtils.getUserid()).toResponse<SettleStatus>().await()
            settleStatus = req2
        }
    }
}