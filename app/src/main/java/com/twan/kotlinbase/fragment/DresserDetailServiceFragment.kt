package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.tencent.qcloud.tim.demo.utils.Constants
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.app.getNeedByDesc
import com.twan.kotlinbase.bean.DresserDetail
import com.twan.kotlinbase.bean.DresserService
import com.twan.kotlinbase.bean.Mycoupon
import com.twan.kotlinbase.bean.ServiceBean
import com.twan.kotlinbase.databinding.*
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.DresserDetailActivity
import com.twan.kotlinbase.ui.ServiceDetailActivity
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_single_rv.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class DresserDetailServiceFragment(var talentId:String, var userId:String,var nickName:String): BaseDataBindingFragment<ActivitySingleRvBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_single_rv
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        include_head.visibility=View.GONE
        initRv()
    }

    lateinit var mAdapter: BaseQuickAdapter<DresserService, BaseDataBindingHolder<ItemDresserServiceBinding>>
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableLoadMore(false)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        //single_rv.addItemDecoration(DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL))
        single_rv.adapter=object : BaseQuickAdapter<DresserService, BaseDataBindingHolder<ItemDresserServiceBinding>>(R.layout.item_dresser_service){
            override fun convert(holder: BaseDataBindingHolder<ItemDresserServiceBinding>, item: DresserService) {
                holder.dataBinding!!.item=item
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.imageUrl)
                holder.dataBinding!!.btnTalk.setOnClickListener {
                    MyUtils.chat(mActivity,userId,nickName)
                }
            }

        }.also {
            mAdapter=it
            it.setOnItemClickListener { adapter, view, position ->
                var item = it.data[position]
                Router.newIntent(mActivity)
                        .putSerializable("ServiceBean",ServiceBean(id = item.serviceId,serviceTypeValue=item.serviceType))
                        .putSerializable("need", getNeedByDesc(item.serviceType))
                        .to(ServiceDetailActivity::class.java).launch()
            }
            getData()
        }
    }

    fun getData(){
        RxHttpScope(mActivity, refreshLayout).launch {
            var req = RxHttp.get("talent/findAllServiceByUserId")
                    .add("userId",userId)
                    .toResponse<List<DresserService>>().await()
            mAdapter.setList(req)
        }
    }
}