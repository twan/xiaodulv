package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Discovery
import com.twan.kotlinbase.bean.HotTuigaung
import com.twan.kotlinbase.bean.HotZhibo
import com.twan.kotlinbase.bean.ServiceBean
import com.twan.kotlinbase.databinding.FragmentOrderAllBinding
import com.twan.kotlinbase.databinding.FragmentSuggest1Binding
import com.twan.kotlinbase.databinding.ItemDiscoveryBinding
import com.twan.kotlinbase.databinding.ItemHomeHotBinding
import com.twan.kotlinbase.event.HotRefresh
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.LoginActivity
import com.twan.kotlinbase.ui.NewOrderActivity
import com.twan.kotlinbase.ui.ServiceDetailActivity
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.firstImgUrl
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.fragment_home_hot.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class HomtHotFragment(var mNeed:Need?): BaseDataBindingFragment<FragmentSuggest1Binding>() {
    var dataUrl="service/shopLive/findTopTen"
    override fun getLayoutId(): Int {
        return R.layout.fragment_home_hot
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        whiteStatusBar()
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        initDataSource()
        initRv()
    }

    fun initDataSource(){
        if (mNeed == Need.SHOP_LIVE){
            dataUrl="service/shopLive/findTopTen"
        } else if (mNeed==Need.SHORT_VIDEO_MAKE){
            dataUrl="service/shortVideoMake/findTopTen"
        } else if (mNeed==Need.SHORT_VIDEO_PROMOTE){
            dataUrl="service/shortVideoPromote/findTopTen"
        } else {
            dataUrl="service/actorModel/findTopTen"
        }
    }

    fun initRv(){
        rv_hot.layoutManager= GridLayoutManager(mActivity,2)
        rv_hot.adapter=object :BaseQuickAdapter<ServiceBean,BaseDataBindingHolder<ItemHomeHotBinding>>(R.layout.item_home_hot){
            override fun convert(holder: BaseDataBindingHolder<ItemHomeHotBinding>, item: ServiceBean) {
                holder.dataBinding!!.item=item
                holder.dataBinding!!.btnChat.setOnClickListener {
                    if (MyUtils.isLogin()){
                        MyUtils.chat(mActivity,item.userId,"达人")
                    }else{
                        Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
                    }



                }
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.imageUrl.firstImgUrl())
            }
        }.also {
            it.setOnItemClickListener { adapter, view, position ->
                Router.newIntent(mActivity).putSerializable("ServiceBean",it.data[position])
                    .putSerializable("need", mNeed).to(ServiceDetailActivity::class.java).launch()
            }
            getData(it)
        }
    }


    @Subscribe
    fun refresh(event: HotRefresh){
        getData(rv_hot.adapter as BaseQuickAdapter<ServiceBean, BaseDataBindingHolder<ItemHomeHotBinding>>)
    }

    fun getData(adapter: BaseQuickAdapter<ServiceBean,BaseDataBindingHolder<ItemHomeHotBinding>>){
        RxHttpScope().launch {
            var req=RxHttp.get(dataUrl).toResponse<List<ServiceBean>>().await()
            adapter.setList(req)
        }
    }

}