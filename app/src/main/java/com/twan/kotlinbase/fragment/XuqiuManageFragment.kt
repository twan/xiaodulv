package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.EnumSaleStatus
import com.twan.kotlinbase.bean.OrderDTO
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.ServiceManageItem
import com.twan.kotlinbase.bean.XuqiuManageItem
import com.twan.kotlinbase.databinding.ActivitySingleRvBinding
import com.twan.kotlinbase.databinding.ItemOrderBinding
import com.twan.kotlinbase.databinding.ItemServiceManageBinding
import com.twan.kotlinbase.databinding.ItemXuqiuManageBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.OrderDetailActivity
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_single_rv.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//需求管理
class XuqiuManageFragment(var mSaleStatus: EnumSaleStatus): BaseDataBindingFragment<ActivitySingleRvBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_single_rv
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE

        include_head.visibility=View.GONE
        initRv()
    }

    lateinit var mAdapter: BaseQuickAdapter<XuqiuManageItem, BaseDataBindingHolder<ItemXuqiuManageBinding>>
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener { getData(true) }
        refreshLayout.setEnableLoadMore(false)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        single_rv.adapter=object : BaseQuickAdapter<XuqiuManageItem, BaseDataBindingHolder<ItemXuqiuManageBinding>>(R.layout.item_xuqiu_manage){
            override fun convert(holder: BaseDataBindingHolder<ItemXuqiuManageBinding>, item: XuqiuManageItem) {
                holder.dataBinding!!.item=item
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.imageUrl)
            }

        }.also {
            mAdapter=it
            it.setOnItemClickListener { adapter, view, position ->
                //Router.newIntent(mActivity).putSerializable("data",it.data[position]).to(OrderDetailActivity::class.java).launch()
            }
            getData(true)
        }
    }

    var currentPage=1
    fun getData(isRefresh:Boolean=false){
        if (isRefresh) currentPage=1
        RxHttpScope(mActivity,refreshLayout).launch {
            var req= RxHttp.get("demand/manager/findAllByUserId")
                    .add("userId", MyUtils.getUserid())
                    //.add("pageSize",20)
                    //.add("status",mSaleStatus.id)
                    //.add("currentPage",currentPage++)
                    .toResponse<List<XuqiuManageItem>>().await()

            mAdapter.setList(req)
            //if (currentPage==2) mAdapter.setList(req)
            //else  mAdapter.addData(req)
        }
    }
}