package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.SPUtils
import com.blankj.utilcode.util.SpanUtils
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.LoginBean
import com.twan.kotlinbase.bean.Register
import com.twan.kotlinbase.databinding.FragmentLoginBinding
import com.twan.kotlinbase.databinding.FragmentRegisterBinding
import com.twan.kotlinbase.network.Api
import com.twan.kotlinbase.network.Response
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.MainActivity
import com.twan.kotlinbase.ui.WebActivity
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.JsonUtil
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.pop_statement.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.await
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class RegisterFragment: BaseDataBindingFragment<FragmentRegisterBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_register
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        mBinding.register= Register()

        SpanUtils.with(chb_privacy).append("我已阅读并同意")
            .setFontSize(12,true)
            .append("《服务协议》")
            .setFontSize(12,true)
            .setClickSpan(resources.getColor(R.color.text_orange),false){
                Router.newIntent(mActivity)
                    .putString("url", "https://api.xiaodulv6.com/protocol/p1")
                    .putString("title","服务协议")
                    .to(WebActivity::class.java).launch()
            }
            .append("及")
            .setFontSize(12,true)
            .append("《隐私政策》")
            .setFontSize(12,true)
            .setClickSpan(resources.getColor(R.color.text_orange),false){
                Router.newIntent(mActivity)
                    .putString("url", "https://api.xiaodulv6.com/protocol/p2")
                    .putString("title","隐私政策")
                    .to(WebActivity::class.java).launch()
            }.create()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun event(event: String){

    }

    @OnClick(R.id.btn_click)
    fun register(){
        if (!InputUtils.checkEmpty(edt_mobile,edt_password,edt_password_again,edt_code))return
        if (mBinding.register!!.password != mBinding.register!!.confirmPassword){
            edt_password_again.requestFocus()
            edt_password_again.error = "请输入一致的密码"
            return
        }
        var tag=""
        if (chb_1.isChecked){ tag += "1," }
        if (chb_2.isChecked){ tag += "2," }
        if (chb_3.isChecked){ tag += "3," }
        if (chb_4.isChecked){ tag += "4," }
        if (chb_5.isChecked){ tag += "5," }

        if(tag.isEmpty()){
            ToastUtils.showShort("请至少选择一种角色")
            return
        }
        if (!chb_privacy.isChecked){
            ToastUtils.showShort("请勾选用户协议")
            return
        }
        RxHttpScope().launch {
            var req = RxHttp.postJson("/users/register")
                    .add("code",mBinding.register!!.code)
                    .add("confirmPassword",mBinding.register!!.password)
                    .add("invitationCode", mBinding.register!!.invitationCode)
                    .add("mobile", mBinding.register!!.mobile)
                    .add("password", mBinding.register!!.password)
                    .add("tag", tag)
                    .setAssemblyEnabled(false)
                    .toResponse<LoginBean>().await()
            SPUtils.getInstance().put("token",req.token)
            SPUtils.getInstance().put("personinfo",JsonUtil.objectToJson(req))
            Router.newIntent(mActivity).to(MainActivity::class.java).launch()
            mActivity.finish()
        }
    }


    @OnClick(R.id.tv_verfiy)
    fun sendSms(){
        timer()
    }

    private var timer: CountDownTimer? = null
    private fun timer() {
        if (!InputUtils.checkEmpty(edt_mobile)){ return }
        RxHttpScope().launch {
            val req = RxHttp.postJson("/sms/send")
                    .add("mobile",mBinding.register?.mobile)
                    .add("type", 2)//1登录验证码，2注册验证码
                    .setAssemblyEnabled(false)
                    .toResponse<Any>().await()

            ToastUtils.showShort("验证码发送成功")
            timer = object : CountDownTimer(60000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    tv_verfiy.isClickable = false
                    tv_verfiy.text = millisUntilFinished.div(1000).toString() + "s"
                }

                override fun onFinish() {
                    tv_verfiy.isClickable = true
                    tv_verfiy.text = "获取验证码"
                }
            }
            timer!!.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (timer != null) {
            timer!!.cancel()
        }
    }
}