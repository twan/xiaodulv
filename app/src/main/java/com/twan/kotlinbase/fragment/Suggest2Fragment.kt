package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.Mycoupon
import com.twan.kotlinbase.bean.Myfeedback
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.databinding.*
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.XpopupUtils
import kotlinx.android.synthetic.main.activity_single_rv.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//我的反馈
class Suggest2Fragment: BaseDataBindingFragment<ActivitySingleRvBinding>() {
    lateinit var mAdapter:BaseQuickAdapter<Myfeedback, BaseDataBindingHolder<ItemMyFeedbackBinding>>
    override fun getLayoutId(): Int {
        return R.layout.activity_single_rv
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        whiteStatusBar()
        toolbar?.visibility=View.GONE
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        initRv()
    }

    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        //single_rv.addItemDecoration(DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL))
        single_rv.adapter=object : BaseQuickAdapter<Myfeedback, BaseDataBindingHolder<ItemMyFeedbackBinding>>(R.layout.item_my_feedback){
            override fun convert(holder: BaseDataBindingHolder<ItemMyFeedbackBinding>, item: Myfeedback) {
                holder.dataBinding!!.item=item

                var rv_pic = holder.getView<RecyclerView>(R.id.rv_pic)
                rv_pic.layoutManager= GridLayoutManager(mActivity,3)
                var datas =item.feedbackImages.split(",").toMutableList()
                rv_pic.adapter = object : BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_pic, datas){
                    override fun convert(holder: BaseViewHolder, url: String) {
                        var iv_pic =holder.getView<ImageView>(R.id.iv_pic)
                        if (url.endsWith("mp4")){
                            GlideUtils.loadVideo0Frame(mActivity,iv_pic!!,url)
                        } else {
                            GlideUtils.loadCorner(mActivity, iv_pic, url.toString())
                        }
                        iv_pic.setOnClickListener {
                            XpopupUtils.previewMultiImgs(mActivity,holder.adapterPosition,iv_pic,datas as MutableList<Any>)
                        }

                    }
                }



            }

        }.also {
            mAdapter=it
            getData()
        }
    }

    var currentPage=1
    fun getData(isRefresh:Boolean=true){
        if (isRefresh) currentPage=1
        RxHttpScope(mActivity, refreshLayout).launch {
            var req= RxHttp.get("feedback/getByCondition")
                    .add("pageSize",20)
                    .add("currentPage",currentPage++)
                    .add("userId", MyUtils.getUserid())
                .toResponse<PageData<Myfeedback>>().await()
            if (currentPage==2) mAdapter.setList(req.content)
            else  mAdapter.addData(req.content)
        }
    }

}