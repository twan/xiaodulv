package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.lxj.xpopup.XPopup
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.EnumSaleStatus
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.OrderDTO
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.ServiceManageItem
import com.twan.kotlinbase.databinding.ActivitySingleRvBinding
import com.twan.kotlinbase.databinding.ItemOrderBinding
import com.twan.kotlinbase.databinding.ItemServiceManageBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.OrderDetailActivity
import com.twan.kotlinbase.ui.SendActivity
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.firstImgUrl
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_single_rv.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//服务管理
class ServiceManageFragment(var mSaleStatus: EnumSaleStatus): BaseDataBindingFragment<ActivitySingleRvBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_single_rv
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE

        include_head.visibility=View.GONE
        initRv()
    }

    override fun onResume() {
        super.onResume()
        getData(true)
    }

    lateinit var mAdapter: BaseQuickAdapter<ServiceManageItem, BaseDataBindingHolder<ItemServiceManageBinding>>
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener { getData(true) }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        single_rv.adapter=object : BaseQuickAdapter<ServiceManageItem, BaseDataBindingHolder<ItemServiceManageBinding>>(R.layout.item_service_manage){
            override fun convert(holder: BaseDataBindingHolder<ItemServiceManageBinding>, item: ServiceManageItem) {
                holder.dataBinding!!.item=item
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.imageUrl.firstImgUrl())
                if (mSaleStatus == EnumSaleStatus.SALE_UP){//出售中
                    holder.dataBinding!!.btnOk.text="下架"
                    holder.dataBinding!!.btnOk.background = resources.getDrawable(R.drawable.corner_gray_f7_25)
                }
                //编辑
                holder.dataBinding!!.tvEdit.setOnClickListener {
                    var index = when (item.serviceTypeValue) {
                        Need.SHORT_VIDEO_MAKE.desc -> 1
                        Need.SHORT_VIDEO_PROMOTE.desc -> 2
                        Need.SHOP_LIVE.desc -> 0
                        Need.ACTOR.desc -> 4
                        Need.MODEL.desc -> 3
                        else ->0
                    }
                    //Router.newIntent(mActivity).putInt("data",2).putInt("index",index).putString("serviceId",item.id).to(SendActivity::class.java).launch()
                }
                //删除
                holder.dataBinding!!.tvDel.setOnClickListener {
                    XPopup.Builder(mActivity).asConfirm("提示", "您确定要删除吗 ?", "取消", "确定",{
                        RxHttpScope().launch {
                            RxHttp.get("service/multiple/delete")
                                .add("serviceId",item.serviceId)
                                .add("serviceTypeValue",item.serviceTypeValue).toResponse<Any>().await()
                            ToastUtils.showShort("删除成功")
                            getData(true)
                        }
                    },null,false).show()
                }
                holder.dataBinding!!.btnOk.setOnClickListener {

                    if (mSaleStatus == EnumSaleStatus.SALE_UP) {//出售中, 下架
                        XPopup.Builder(mActivity).asConfirm("提示", "您确定要下架吗 ?", "取消", "确定",{
                            RxHttpScope().launch {
                                RxHttp.get("service/multiple/putOff").add("serviceId",item.serviceId).toResponse<Any>().await()
                                ToastUtils.showShort("下架成功")
                                getData(true)
                            }
                        },null,false).show()
                    }
                    if (mSaleStatus == EnumSaleStatus.SALE_DOWN) {//已下架, 上架
                        XPopup.Builder(mActivity).asConfirm("提示", "您确定要上架吗 ?", "取消", "确定",{
                            RxHttpScope().launch {
                                RxHttp.get("service/multiple/putOn").add("serviceId",item.serviceId).toResponse<Any>().await()
                                ToastUtils.showShort("上架成功")
                                getData(true)
                            }
                        },null,false).show()
                    }
                }
            }

        }.also {
            mAdapter=it
            it.setOnItemClickListener { adapter, view, position ->
                //Router.newIntent(mActivity).putSerializable("data",it.data[position]).to(OrderDetailActivity::class.java).launch()
            }
            getData(true)
        }
    }

    var currentPage=1
    fun getData(isRefresh:Boolean=false){
        if (isRefresh) currentPage=1
        RxHttpScope(mActivity,refreshLayout).launch {
            var req= RxHttp.get("service/multiple/getByCondition")
                    .add("userId", MyUtils.getUserid())
                    .add("pageSize",20)
                    .add("saleStatus",mSaleStatus.id)
                    .add("currentPage",currentPage++)
                    .toResponse<PageData<ServiceManageItem>>().await()

            if (currentPage==2) mAdapter.setList(req.content)
            else  mAdapter.addData(req.content)
        }
    }
}