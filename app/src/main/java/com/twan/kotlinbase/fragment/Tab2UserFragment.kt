package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.OnClick
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.Discovery
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.databinding.ItemDiscoveryBinding
import com.twan.kotlinbase.databinding.Tab2FragmentUserBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.DiscoveryDetailActivity
import com.twan.kotlinbase.ui.LoginActivity
import com.twan.kotlinbase.ui.SendDynamicActivity
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.wx.goodview.GoodView
import kotlinx.android.synthetic.main.tab2_fragment_user.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class Tab2UserFragment: BaseDataBindingFragment<Tab2FragmentUserBinding>() {
    private lateinit var mAdpater: BaseQuickAdapter<Discovery, BaseDataBindingHolder<ItemDiscoveryBinding>>
    lateinit var goodView:GoodView

    override fun getLayoutId(): Int {
        return R.layout.tab2_fragment_user
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.visibility=View.VISIBLE
        title?.text="发现"
        back?.visibility=View.GONE
        searchView?.visibility=View.GONE//一期隐藏
        tv_back_text?.visibility=View.GONE//一期隐藏
        tv_back_text?.text="请选择"
        tv_back_text?.textSize=16f
        goodView = GoodView(mActivity)
        initRv()
    }

    override fun onResume() {
        super.onResume()
        currentPage=1
        reqData()
    }

    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener {
            currentPage=1
            reqData()
        }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setOnLoadMoreListener { reqData() }
        rv_list.addItemDecoration(DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL))
        rv_list.adapter=object :BaseQuickAdapter<Discovery,BaseDataBindingHolder<ItemDiscoveryBinding>>(R.layout.item_discovery){
            override fun convert(holder: BaseDataBindingHolder<ItemDiscoveryBinding>, item: Discovery) {
                holder.dataBinding!!.item=item
                GlideUtils.loadCicle(mActivity,holder.dataBinding!!.ivAvater,item.avatarUrl)
                holder.dataBinding!!.tvShijian.text= RelativeDateFormat.format(DateUtils.convetDate2(item.releaseTime))
                holder.dataBinding!!.tvLike.setOnClickListener {
                    holder.dataBinding!!.tvLike.text=(item.likeCount+1).toString()
                    goodView.setText("+1")//goodView.setImage(getResources().getDrawable(R.mipmap.good_checked));
                    goodView.show(it)
                    dianzan(item.id)
                }
                //九宫格图片
                var rv_pic = holder.getView<RecyclerView>(R.id.rv_pic)
                rv_pic.layoutManager=GridLayoutManager(mActivity,3)
                rv_pic.adapter = object : BaseQuickAdapter<String,BaseViewHolder>(R.layout.item_pic, item.imagesUrl){
                    override fun convert(holder: BaseViewHolder, url: String) {
                        var iv_pic =holder.getView<ImageView>(R.id.iv_pic)
                        if (url.endsWith("mp4")){
                            GlideUtils.loadVideo0Frame(mActivity,iv_pic!!,url)
                        } else {
                            GlideUtils.loadCorner(mActivity, iv_pic, url.toString())
                        }
                        iv_pic.setOnClickListener {
                            XpopupUtils.previewMultiImgs(mActivity,holder.adapterPosition,iv_pic,item.imagesUrl as MutableList<Any>)
                        }

                    }
                }
            }

        }.also {
            mAdpater=it
            it.setOnItemClickListener { adapter, view, position ->
                Router.newIntent(mActivity).putSerializable("data",it.data[position]).to(DiscoveryDetailActivity::class.java).launch()
            }
        }
    }

    fun dianzan(momentsId:String){
        RxHttpScope().launch {
            RxHttp.postJson("moment/like").add("likedPostId",MyUtils.getUserid()).add("momentsId",momentsId).toResponse<Any>().await()
        }
    }

    var currentPage=1
    override fun reqData(){
        RxHttpScope(mActivity,refreshLayout).launch {
            var req = RxHttp.get("moment/getByCondition")
                    .add("pageSize",20)
                    .add("currentPage",currentPage++)
                    .toResponse<PageData<Discovery>>().await()
            if (currentPage==2) mAdpater.setList(req.content)
            else  mAdpater.addData(req.content)
        }
    }

    @OnClick(R.id.iv_send)
    fun send(){
        if (MyUtils.isLogin()) {
            Router.newIntent(mActivity).to(SendDynamicActivity::class.java).launch()
        } else {
            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
        }
    }

}