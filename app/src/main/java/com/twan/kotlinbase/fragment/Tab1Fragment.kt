package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import com.blankj.utilcode.util.FragmentUtils
import com.jaeger.library.StatusBarUtil
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Role
import com.twan.kotlinbase.databinding.Tab1FragmentBinding
import com.twan.kotlinbase.event.HotRefresh
import com.twan.kotlinbase.util.MyUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class Tab1Fragment: BaseDataBindingFragment<Tab1FragmentBinding>() {

    private var userFragment= Tab1UserFragment()
    private var servicePrivoderFragment= Tab1SpFragment()

    override fun getLayoutId(): Int {
        return R.layout.tab1_fragment
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        StatusBarUtil.setColor(mActivity, resources.getColor(R.color.head_bg), 0)
        swithRole()
    }

    fun swithRole(){
        if (MyUtils.getRole()== Role.USER) {
            FragmentUtils.replace(requireFragmentManager(),userFragment,R.id.fl_fragment)
        } else {
            FragmentUtils.replace(requireFragmentManager(),userFragment,R.id.fl_fragment)
            //FragmentUtils.replace(requireFragmentManager(),servicePrivoderFragment,R.id.fl_fragment)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        mIsVisible = isVisibleToUser
        if (isVisibleToUser) {
            EventBus.getDefault().post(HotRefresh())
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun event(event: Role){
        swithRole()
    }

}