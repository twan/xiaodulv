package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.databinding.FragmentOrderWillPayBinding

class OrderWillPayFragment: BaseDataBindingFragment<FragmentOrderWillPayBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_order_will_pay
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
    }


}