package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.blankj.utilcode.util.FragmentUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Role
import com.twan.kotlinbase.databinding.Tab2FragmentBinding
import com.twan.kotlinbase.util.MyUtils
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class Tab2Fragment: BaseDataBindingFragment<Tab2FragmentBinding>() {

    private var userFragment= Tab2UserFragment()
    private var servicePrivoderFragment= Tab2SpFragment()

    override fun getLayoutId(): Int {
        return R.layout.tab2_fragment
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        swithRole()
    }

    fun swithRole(){
        if (MyUtils.getRole()== Role.USER) {
            FragmentUtils.replace(requireFragmentManager(),userFragment,R.id.fl_fragment2)
        } else {
            FragmentUtils.replace(requireFragmentManager(),servicePrivoderFragment,R.id.fl_fragment2)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun event(event: Role){
        swithRole()
    }

}