package com.twan.kotlinbase.fragment;

import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.NewOrderAdapter;
import com.twan.kotlinbase.app.EnumOrderStatus;
import com.twan.kotlinbase.base.BaseFragment;
import com.twan.kotlinbase.bean.AliPayBean;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.bean.WeChatPayBean;
import com.twan.kotlinbase.pop.SelPayWayPop;
import com.twan.kotlinbase.ui.ActorOrModelDetailActivity;
import com.twan.kotlinbase.ui.AuditActivity;
import com.twan.kotlinbase.ui.EvaluateActivity;
import com.twan.kotlinbase.ui.FabricationVideoDetailActivity;
import com.twan.kotlinbase.ui.NewOrderActivity;
import com.twan.kotlinbase.ui.NewOrderDetailActivity;
import com.twan.kotlinbase.ui.OnLineServiceDetailActivity;
import com.twan.kotlinbase.ui.PromotionVideoDetailActivity;
import com.twan.kotlinbase.ui.SendSimpleActivity;
import com.twan.kotlinbase.ui.ServicingActivity;
import com.twan.kotlinbase.ui.presenter.NewOrderPresenter;
import com.twan.kotlinbase.ui.view.NewOrderView;
import com.twan.kotlinbase.util.UIUtils;
import com.twan.kotlinbase.widgets.RecycleViewDivider;

import butterknife.BindView;

/**
 * @anthor zhoujr
 * @time 2021/4/13 10:13
 * @describe
 */
public class NewOrderFragment extends BaseFragment<NewOrderView, NewOrderPresenter> implements NewOrderView {
    String status = "";
    @BindView(R.id.newOrderRecy)
    RecyclerView newOrderRecy;
    @BindView(R.id.newOrderRefresh)
    SmartRefreshLayout newOrderRefresh;
    NewOrderAdapter newOrderAdapter;
    int page = 1;
    SelPayWayPop selPayWayPop;

    public NewOrderFragment(EnumOrderStatus status) {
        this.status = status.getId();
    }

    @Override
    public void initView(View rootView) {
        super.initView(rootView);
//        newOrderRefresh.autoRefresh();
        newOrderRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                setReflush();
            }
        });
        newOrderRefresh.setEnableAutoLoadMore(true);
        newOrderRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page++;
                mPresenter.getOrderLst(page,status);
            }
        });
        newOrderAdapter = new NewOrderAdapter();
        View emptyView = LayoutInflater.from(getContext()).inflate(R.layout.view_emptyorder,null);
        newOrderAdapter.setEmptyView(emptyView);
        newOrderRecy.setLayoutManager(new LinearLayoutManager(getContext()));

        newOrderRecy.addItemDecoration(new RecycleViewDivider(
                getContext(), LinearLayoutManager.VERTICAL, 10, getResources().getColor(R.color.layout_bg)));
        newOrderRecy.setAdapter(newOrderAdapter);
        newOrderAdapter.setItemClick(new NewOrderAdapter.ItemClick() {
            @Override
            public void cancelOrder(int position) {
                mPresenter.cancelOrder(newOrderAdapter.getData().get(position).getOrderId());
            }

            @Override
            public void payOrder(int position) {
                if(selPayWayPop == null){
                    selPayWayPop = new SelPayWayPop(getContext());
                    selPayWayPop.setConfimPay(new SelPayWayPop.ConfimPay() {
                        @Override
                        public void ConfimSelPayWay(int p) {
                            if(p == 0){
                                //微信
                                mPresenter.getWeChatPayData(newOrderAdapter.getData().get(position).getOrderId(),newOrderAdapter.getData().get(position).getTotalFee());
                            }else{
                                //支付宝
                                mPresenter.getALiPayData(newOrderAdapter.getData().get(position).getOrderId(),newOrderAdapter.getData().get(position).getTotalFee());
                            }
//                            payFailPop.showPopupWindow();
                        }
                    });
                }
                selPayWayPop.showPopupWindow();
            }

            @Override
            public void sendOrder(int position) {
                bundle.putString("orderId",newOrderAdapter.getData().get(position).getOrderId());
                bundle.putInt("status",newOrderAdapter.getData().get(position).getStatus());
                jumpToActivityForBundle(NewOrderDetailActivity.class,bundle);
            }

            @Override
            public void evaluateOrder(int position) {

            }

            @Override
            public void deleteOrder(int position) {
                mPresenter.deleteOrder(newOrderAdapter.getData().get(position).getOrderId());
            }

            @Override
            public void goDetail(int position) {
                bundle.putString("orderId",newOrderAdapter.getData().get(position).getOrderId());
                bundle.putInt("status",newOrderAdapter.getData().get(position).getStatus());
                jumpToActivityForBundle(NewOrderDetailActivity.class,bundle);
//                jumpToActivity(ServicingActivity.class);
//                jumpToActivity(NewOrderDetailActivity.class);
//                jumpToActivity(EvaluateActivity.class);
//                jumpToActivity(SendSimpleActivity.class);
//                jumpToActivity(OnLineServiceDetailActivity.class);
//                jumpToActivity(PromotionVideoDetailActivity.class);
//                jumpToActivity(FabricationVideoDetailActivity.class);
//                jumpToActivity(ActorOrModelDetailActivity.class);
            }

            @Override
            public void receiveOrder(int position) {
                //确认收货
                mPresenter.confimreceive(newOrderAdapter.getData().get(position).getOrderId());
            }
        });
        mPresenter.getOrderLst(page,status);
    }

    @Override
    public void onResume() {
        super.onResume();
        setReflush();
    }

    @Override
    protected NewOrderPresenter createPresenter() {
        return new NewOrderPresenter((NewOrderActivity)getContext());
    }


    @Override
    protected int provideContentViewId() {
        return R.layout.fragment_neworder;
    }

    @Override
    public void getOrderLstSucc(OrderBean orderBean) {
        if(page == 1){
            newOrderRefresh.finishRefresh();
            newOrderAdapter.setNewData(orderBean.getResult().getContent());
        }else{
            newOrderRefresh.finishLoadMore(true);
            newOrderAdapter.addData(orderBean.getResult().getContent());
        }

    }

    @Override
    public void confimreceiveSucc() {
        setReflush();
    }

    @Override
    public void cancelSucc() {
        UIUtils.showToast("订单已取消");
        setReflush();
    }

    @Override
    public void deleteSucc() {
        UIUtils.showToast("订单已删除");
        setReflush();
    }

    @Override
    public void getAliPayDataSucc(AliPayBean aliPayBean) {

    }

    @Override
    public void getWeChatPayDataSucc(WeChatPayBean weChatPayBean) {

    }

    private void setReflush() {
        page = 1;
        mPresenter.getOrderLst(page,status);
    }
}
