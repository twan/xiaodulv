package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.MyFocus
import com.twan.kotlinbase.bean.Mycoupon
import com.twan.kotlinbase.databinding.ActivitySingleRvBinding
import com.twan.kotlinbase.databinding.ItemMyCouponBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.activity_single_rv.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class MycouponFragment(var type:Int): BaseDataBindingFragment<ActivitySingleRvBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.activity_single_rv
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        whiteStatusBar()
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        include_head.visibility=View.GONE
        initRv()
    }

    lateinit var mAdapter: BaseQuickAdapter<Mycoupon, BaseDataBindingHolder<ItemMyCouponBinding>>
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableLoadMore(false)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        //single_rv.addItemDecoration(DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL))
        single_rv.adapter=object : BaseQuickAdapter<Mycoupon, BaseDataBindingHolder<ItemMyCouponBinding>>(R.layout.item_my_coupon){
            override fun convert(holder: BaseDataBindingHolder<ItemMyCouponBinding>, item: Mycoupon) {
                holder.dataBinding!!.item=item
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.couponImage)
            }

        }.also {
            mAdapter=it
            getData()
        }
    }

    fun getData(){
        RxHttpScope(mActivity, refreshLayout).launch {
            var req = RxHttp.get("/coupon/findByUserId")
                    .add("userId", MyUtils.getUserid())
                    .add("status", type-1)  //0-未使用,1-已使用,2-已过期
                    .toResponse<List<Mycoupon>>().await()
            mAdapter.setList(req)
        }
    }


}