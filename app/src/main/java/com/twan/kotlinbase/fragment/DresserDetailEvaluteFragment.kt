package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.DresserComment
import com.twan.kotlinbase.bean.DresserService
import com.twan.kotlinbase.bean.OrderDTO
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.databinding.*
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.fragment_dresser_eva_detail.*
import kotlinx.android.synthetic.main.fragment_dresser_eva_detail.refreshLayout
import kotlinx.android.synthetic.main.fragment_order_all.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class DresserDetailEvaluteFragment: BaseDataBindingFragment<FragmentDresserEvaDetailBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_dresser_eva_detail
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
    }

    lateinit var mAdapter: BaseQuickAdapter<DresserComment, BaseDataBindingHolder<ItemDresserCommentBinding>>
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        rv_comment.addItemDecoration(DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL))
        rv_comment.adapter=object : BaseQuickAdapter<DresserComment, BaseDataBindingHolder<ItemDresserCommentBinding>>(R.layout.item_dresser_comment){
            override fun convert(holder: BaseDataBindingHolder<ItemDresserCommentBinding>, item: DresserComment) {
                holder.dataBinding!!.item=item
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.imageUrl)
            }

        }.also {
            mAdapter=it
            getData()
        }
    }

    var currentPage=1
    fun getData(isRefresh:Boolean=false){
        if (isRefresh) currentPage=1
        RxHttpScope(mActivity,refreshLayout).launch {
            var req= RxHttp.get("appraise/findAllAppraise")
                    .add("businessId",MyUtils.getUserid())
//                    .add("userId",MyUtils.getUserid())
//                    .add("pageSize",20)
//                    .add("status",orderStatus.id)
//                    .add("currentPage",currentPage++)
                    .toResponse<List<DresserComment>>().await()

            mAdapter.setList(req)
//            if (currentPage==2) mAdapter.setList(req.content)
//            else  mAdapter.addData(req.content)
        }
    }
}