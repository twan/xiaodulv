package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.FragmentSuggest1Binding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.InputUtils
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.fragment_suggest1.*
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class Suggest1Fragment: BaseDataBindingFragment<FragmentSuggest1Binding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_suggest1
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        whiteStatusBar()
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        initRvMultiPicOrVideo(rv_multi_pic,1)
    }

    override fun uploadCallback(url:String){
        if (uploadType == 1){
            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        }
    }

    @OnClick(R.id.btn_click)
    fun ckicl(){
        if (fjEdit.text.toString().isEmpty()){
            ToastUtils.showShort("请输入内容")
            return
        }
        var type = if (rb1.isChecked) 1 else if (rb2.isChecked) 2 else 3
        RxHttpScope().launch {
            RxHttp.postJson("feedback/save")
                .add("feedbackContent",fjEdit.text.toString())
                .add("feedbackImages",getPicsUrl())
                .add("feedbackType",type)
                //.add("id","")
                //.add("nickName","")
                //.add("replyContent","")
                //.add("status","")
                .add("userId",MyUtils.getUserid())
                .toResponse<Any>().await()
            ToastUtils.showShort("提交成功, 谢谢您的意见")
            mActivity.finish()
        }
    }
}