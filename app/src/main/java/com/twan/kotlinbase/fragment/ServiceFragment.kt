package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.enums.PopupPosition
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.ServiceBean
import com.twan.kotlinbase.databinding.*
import com.twan.kotlinbase.event.HotRefresh
import com.twan.kotlinbase.event.RefreshService
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.*
import com.twan.kotlinbase.ui.LoginActivity
import com.twan.kotlinbase.ui.ServiceDetailActivity
import com.twan.kotlinbase.util.DictUtils
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.util.firstImgUrl
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.fragment_home_hot.*
import kotlinx.android.synthetic.main.fragment_service.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//用户首页的服务综合列表

class ServiceFragment(var need: Need?): BaseDataBindingFragment<FragmentServiceBinding>() {
    var mCurrType=1//列表, 格
    var dataUrl="service/shopLive/getByCondition"
    var mOrder=""//排序方式 asc/desc
    var goodAtValue=""
    var talentSkillValue = ""
    override fun getLayoutId(): Int {
        return R.layout.fragment_service
    }



    override fun initView(view: View?, savedInstanceState: Bundle?) {
        mBinding.includeHead.visibility=View.GONE
        toolbar?.visibility=View.GONE
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        initDataSource()
        initLeimuDaren()
        initRv()
    }

    fun initDataSource(){
        if (need == Need.SHOP_LIVE){
            dataUrl="service/shopLive/getByCondition"
        } else if (need==Need.SHORT_VIDEO_MAKE){
            dataUrl="service/shortVideoMake/getByCondition"
        } else if (need==Need.SHORT_VIDEO_PROMOTE){
            dataUrl="service/shortVideoPromote/getByCondition"
        } else {
            dataUrl="service/actorModel/getByCondition"
        }
    }

    //不同的综合列表, 类目和才艺达人 这两处不一样,要分开处理
    fun initLeimuDaren(){
        if (need == Need.SHOP_LIVE){
            tv_type1.text="类目"
            tv_type2.text="才艺达人"
            tv_type1.setOnClickListener {
                XPopup.Builder(mActivity).atView(tv_type1).asCustom(SelectDictPopup(tv_zonghe,mActivity,DictUtils.category){ pos, spiBean ->
                    //ToastUtils.showShort("$pos,$spiBean")
                    goodAtValue="${spiBean.value}"
                    getData(true)
                }).show()
            }
            tv_type2.setOnClickListener {
                XPopup.Builder(mActivity).atView(tv_type2).asCustom(SelectDictPopup(tv_zonghe,mActivity, DictUtils.talented_person){ pos, dict ->
                    //ToastUtils.showShort("$pos,$dict")
                    talentSkillValue="${dict.value}"
                    getData(true)
                }).show()
            }
        } else if (need==Need.SHORT_VIDEO_MAKE){
            tv_type1.text="拍摄类目"
            tv_type2.text="拍摄类型"
            tv_type1.setOnClickListener {
                XPopup.Builder(mActivity).atView(tv_type1).asCustom(SelectDictPopup(tv_zonghe,mActivity,DictUtils.shooting_mode){ pos, spiBean ->
                    //ToastUtils.showShort("$pos,$spiBean")
                    goodAtValue="${spiBean.value}"
                    getData(true)
                }).show()
            }
            tv_type2.setOnClickListener {
                XPopup.Builder(mActivity).atView(tv_type2).asCustom(SelectDictPopup(tv_zonghe,mActivity, DictUtils.shooting_style){ pos, dict ->
                    //ToastUtils.showShort("$pos,$dict")
                    talentSkillValue="${dict.value}"
                    getData(true)
                }).show()
            }
        } else if (need==Need.SHORT_VIDEO_PROMOTE){
            tv_type1.text="平台"
            tv_type2.text="类目"
            tv_type1.setOnClickListener {
                XPopup.Builder(mActivity).atView(tv_type1).asCustom(SelectDictPopup(tv_zonghe,mActivity,DictUtils.short_video_platform){ pos, spiBean ->
                    //ToastUtils.showShort("$pos,$spiBean")
                    goodAtValue="${spiBean.value}"
                    getData(true)
                }).show()
            }
            tv_type2.setOnClickListener {
                XPopup.Builder(mActivity).atView(tv_type2).asCustom(SelectDictPopup(tv_zonghe,mActivity, DictUtils.category){ pos, dict ->
                    //ToastUtils.showShort("$pos,$dict")
                    talentSkillValue="${dict.value}"
                    getData(true)
                }).show()
            }
        } else {
            tv_type1.text="类型"
            tv_type2.text="标签"
            tv_type1.setOnClickListener {
                XPopup.Builder(mActivity).atView(tv_type1).asCustom(SelectDictPopup(tv_zonghe,mActivity,DictUtils.actor_type){ pos, spiBean ->
                    //ToastUtils.showShort("$pos,$spiBean")
                    goodAtValue="${spiBean.value}"
                    getData(true)
                }).show()
            }
            tv_type2.setOnClickListener {
                XPopup.Builder(mActivity).atView(tv_type2).asCustom(SelectDictPopup(tv_zonghe,mActivity, DictUtils.live_tag){ pos, dict ->
                    //ToastUtils.showShort("$pos,$dict")
                    talentSkillValue="${dict.value}"
                    getData(true)
                }).show()
            }
        }
    }



    @Subscribe
    fun refresh(event: RefreshService){
        getData(true)
    }



    //地址选择
    @OnClick(R.id.iv_addr)
    fun selectArea(){
        XPopup.Builder(mActivity).atView(iv_addr).asCustom(SelectCityPopup(mActivity){
            iv_addr.text=it
        }).show()
    }
    //销量
    @OnClick(R.id.tv_sales)
    fun sales(){
        if (tv_sales.tag==null||tv_sales.tag==false) {
            tv_sales.tag=true
            tv_sales.setTextColor(resources.getColor(R.color.text_orange))
        }else{
            tv_sales.tag=false
            tv_sales.setTextColor(resources.getColor(R.color.text_99))
        }
    }
    //item布局
    @OnClick(R.id.iv_layout)
    fun ivlay(){
        if (iv_layout.tag==null||iv_layout.tag==false) {
            iv_layout.tag=true
            iv_layout.setImageResource(R.mipmap.multi_pressed)
            gridLayout()
        }else{
            iv_layout.tag=false
            iv_layout.setImageResource(R.mipmap.multi)
            linearLayout()
        }
    }
    //综合
    @OnClick(R.id.tv_zonghe)
    fun zonghe(){
        XPopup.Builder(mActivity).atView(tv_zonghe).asCustom(ZonghePopup(tv_zonghe,mActivity){pos, spiBean ->
            //ToastUtils.showShort("$pos,$spiBean")
            if (pos==1||pos==3) mOrder="asc" else "desc"
//            if (pos==1||pos==2) mSort="score" else "price"
            getData(true)
        }).show()
    }

    //筛选
    @OnClick(R.id.tv_shuanxuan)
    fun shuaixuan(){
        XPopup.Builder(mActivity).popupPosition(PopupPosition.Right).hasStatusBarShadow(true) .asCustom(MyDrawerPopupView(mActivity){

        }).show()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun event(event: String){

    }

    lateinit var mAdapter: BaseQuickAdapter<ServiceBean, BaseDataBindingHolder<ItemTab1ServiceZhiboBinding>>
    lateinit var mAdapter1: BaseQuickAdapter<ServiceBean, BaseDataBindingHolder<ItemTab1ServiceMakeVideoBinding>>
    lateinit var mAdapter2: BaseQuickAdapter<ServiceBean, BaseDataBindingHolder<ItemTab1ServiceZhibo2Binding>>
    var currentPage=1
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener {
            currentPage=1
            getData(true)
        }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData(false) }
        linearLayout()
    }
    //格子布局
    fun gridLayout(){
        mCurrType=2
        rv_service.layoutManager=GridLayoutManager(mActivity,2)
        rv_service.adapter=object : BaseQuickAdapter<ServiceBean, BaseDataBindingHolder<ItemTab1ServiceZhibo2Binding>>(R.layout.item_tab1_service_zhibo_2){
            override fun convert(holder: BaseDataBindingHolder<ItemTab1ServiceZhibo2Binding>, item: ServiceBean) {
                holder.dataBinding!!.item=item
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.imageUrl)
            }

        }.also {
            mAdapter2=it
            it.setOnItemClickListener { adapter, view, position -> Router.newIntent(mActivity)
                .putSerializable("ServiceBean", it.data[position])
                .putSerializable("need", need)
                .to(ServiceDetailActivity::class.java).launch()  }
            getData(true)
        }
    }
    //线行布局
    fun linearLayout(){
        mCurrType=1
        rv_service.layoutManager=LinearLayoutManager(mActivity)
        if (need==Need.SHOP_LIVE) {
            rv_service.adapter = object : BaseQuickAdapter<ServiceBean, BaseDataBindingHolder<ItemTab1ServiceZhiboBinding>>(R.layout.item_tab1_service_zhibo) {
                override fun convert(holder: BaseDataBindingHolder<ItemTab1ServiceZhiboBinding>, item: ServiceBean) {
                    holder.dataBinding!!.item = item
                    GlideUtils.loadCicle(mActivity, holder.dataBinding!!.ivAvater, item.imageUrl.firstImgUrl())
                    if (need == Need.SHOP_LIVE) {
                        holder.dataBinding!!.tvIsviery.setOnClickListener {
                            XPopup.Builder(getContext())
                                .atView(holder.dataBinding!!.tvIsviery)
                                .hasShadowBg(false) // 去掉半透明背景
                                .popupPosition(PopupPosition.Top)
                                .asCustom(CustomAttachPopup2(mActivity))
                                .show();
                        }
                    }
                    holder.dataBinding!!.btnTalk.setOnClickListener {
                        if (MyUtils.isLogin()){
                            MyUtils.chat(mActivity,item.userId,item.nickName)
                        }else{
                            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
                        }
                    }
                }

            } .also {
                mAdapter = it
                it.setOnItemClickListener { adapter, view, position -> Router.newIntent(mActivity)
                    .putSerializable("ServiceBean", it.data[position])
                    .putSerializable("need", need)
                    .to(ServiceDetailActivity::class.java).launch() }
                getData(true)
            }
        } else{
            rv_service.adapter = object : BaseQuickAdapter<ServiceBean, BaseDataBindingHolder<ItemTab1ServiceMakeVideoBinding>>(R.layout.item_tab1_service_make_video) {
                override fun convert(holder: BaseDataBindingHolder<ItemTab1ServiceMakeVideoBinding>, item: ServiceBean) {
                    holder.dataBinding!!.item = item
                    holder.dataBinding!!.btnTalk.setOnClickListener {
                        if (MyUtils.isLogin()){
                            MyUtils.chat(mActivity,item.userId,item.nickName)
                        }else{
                            Router.newIntent(mActivity).to(LoginActivity::class.java).launch()
                        }
                    }
                    GlideUtils.loadCicle(mActivity, holder.dataBinding!!.ivAvater, item.imageUrl.firstImgUrl())
                }

            } .also {
                mAdapter1 = it
                it.setOnItemClickListener { adapter, view,  position -> Router.newIntent(mActivity)
                    .putSerializable("ServiceBean", it.data[position])
                    .putSerializable("need", need)
                    .to(ServiceDetailActivity::class.java).launch() }
                getData(true)
            }
        }
    }

    fun getData(isRefresh:Boolean=false){
        if (isRefresh) currentPage=1
        RxHttpScope(mActivity,refreshLayout).launch {
            var req= RxHttp.get(dataUrl)
                .add("order",mOrder)//排序方式 asc/desc
                .add("goodAtValue",goodAtValue)
                .add("talentSkillValue",talentSkillValue)
                .add("pageSize",20)
                .add("currentPage",currentPage++)
                .toResponse<PageData<ServiceBean>>().await()
            if (currentPage==2) {
                if (mCurrType==1) {
                    if (::mAdapter.isInitialized)mAdapter.setList(req.content)
                    if (::mAdapter1.isInitialized)mAdapter1.setList(req.content)
                } else {
                    mAdapter2.setList(req.content)
                }
            } else {
                if (mCurrType==1) {
                    if (::mAdapter.isInitialized)mAdapter.addData(req.content)
                    if (::mAdapter1.isInitialized)mAdapter1.addData(req.content)
                } else {
                    mAdapter2.addData(req.content)
                }
            }
        }
    }
}