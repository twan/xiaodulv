package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.EnumOrderStatus
import com.twan.kotlinbase.bean.*
import com.twan.kotlinbase.databinding.FragmentOrderAllBinding
import com.twan.kotlinbase.databinding.ItemOrderBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.ui.OrderDetailActivity
import com.twan.kotlinbase.util.MyUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_discovery_detail.*
import kotlinx.android.synthetic.main.fragment_order_all.*
import kotlinx.android.synthetic.main.fragment_order_all.refreshLayout
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class OrderAllFragment(var orderStatus: EnumOrderStatus): BaseDataBindingFragment<FragmentOrderAllBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_order_all
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        initRv()
    }

    lateinit var mAdapter:BaseQuickAdapter<OrderDTO, BaseDataBindingHolder<ItemOrderBinding>>
    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setRefreshFooter(ClassicsFooter(mActivity))
        refreshLayout.setOnRefreshListener { getData(true) }
        refreshLayout.setEnableLoadMore(true)
        refreshLayout.setEnableRefresh(true)
        refreshLayout.setOnLoadMoreListener { getData() }
        rv_order.adapter=object : BaseQuickAdapter<OrderDTO, BaseDataBindingHolder<ItemOrderBinding>>(R.layout.item_order){
            override fun convert(holder: BaseDataBindingHolder<ItemOrderBinding>, item: OrderDTO) {
                holder.dataBinding!!.item=item
            }

        }.also {
            mAdapter=it
            it.setOnItemClickListener { adapter, view, position ->
                Router.newIntent(mActivity).putSerializable("data",it.data[position]).to(OrderDetailActivity::class.java).launch()
            }
            getData(true)
        }
    }

    var currentPage=1
    fun getData(isRefresh:Boolean=false){
        if (isRefresh) currentPage=1
        RxHttpScope(mActivity,refreshLayout).launch {
            var req= RxHttp.get("order/findAllByUserId")
                    .add("userId",MyUtils.getUserid())
                    .add("pageSize",20)
                    .add("status",orderStatus.id)
                    .add("currentPage",currentPage++)
                    .toResponse<PageData<OrderDTO>>().await()

            if (currentPage==2) mAdapter.setList(req.content)
            else  mAdapter.addData(req.content)
        }
    }
}