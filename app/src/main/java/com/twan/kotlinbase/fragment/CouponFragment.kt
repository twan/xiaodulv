package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.Mycoupon
import com.twan.kotlinbase.databinding.FragmentCouponBinding
import com.twan.kotlinbase.databinding.ItemMyCouponBinding
import com.twan.kotlinbase.event.SelectCouponEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.CacheUtils
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import kotlinx.android.synthetic.main.fragment_coupon.*
import org.greenrobot.eventbus.EventBus
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class CouponFragment(var status:String): BaseDataBindingFragment<FragmentCouponBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_coupon
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        whiteStatusBar()
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        initRv()
    }

    fun getCouponData(adapter: BaseQuickAdapter<Mycoupon,BaseDataBindingHolder<ItemMyCouponBinding>>){
        RxHttpScope().launch {
            var req=RxHttp.get("coupon/findByUserId").add("status",status).add("userId",MyUtils.getUserid()).toResponse<List<Mycoupon>>().await()
            adapter.setList(req)
        }
    }

    fun initRv(){
        rv_coupon.adapter=object :BaseQuickAdapter<Mycoupon,BaseDataBindingHolder<ItemMyCouponBinding>>(R.layout.item_my_coupon){
            override fun convert(holder: BaseDataBindingHolder<ItemMyCouponBinding>, item: Mycoupon) {
                GlideUtils.load(mActivity,holder.dataBinding!!.ivAvater,item.couponImage)
                holder.dataBinding!!.item=item
                holder.dataBinding!!.btnOk.setOnClickListener {
                    EventBus.getDefault().post(SelectCouponEvent(item))
                    mActivity.finish()
                }
            }

        }.also {
            getCouponData(it)
        }
    }

}