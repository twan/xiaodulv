package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.jaeger.library.StatusBarUtil
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.databinding.Tab1FragmentSpBinding


class Tab1SpFragment: BaseDataBindingFragment<Tab1FragmentSpBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.tab1_fragment_sp
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        StatusBarUtil.setColor(mActivity, resources.getColor(R.color.head_bg), 0)
        title?.visibility=View.GONE
        back?.visibility=View.GONE
        searchView?.visibility=View.VISIBLE
        tv_back_text?.visibility=View.VISIBLE
        tv_back_text?.text="请选择"
        tv_back_text?.textSize=16f

    }



}