package com.twan.kotlinbase.fragment;

import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.twan.kotlinbase.R;
import com.twan.kotlinbase.adapter.ServiceOrderAdapter;
import com.twan.kotlinbase.app.EnumServiceOrderStatus;
import com.twan.kotlinbase.base.BaseFragment;
import com.twan.kotlinbase.bean.OrderBean;
import com.twan.kotlinbase.ui.ServiceOrderActivity;
import com.twan.kotlinbase.ui.ServiceOrderDetailActivity;
import com.twan.kotlinbase.ui.presenter.ServiceOrderPresenter;
import com.twan.kotlinbase.ui.view.ServiceOrderView;
import com.twan.kotlinbase.widgets.RecycleViewDivider;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * @anthor zhoujr
 * @time 2021/4/17 14:10
 * @describe
 */
public class ServiceOrderFragment extends BaseFragment<ServiceOrderView, ServiceOrderPresenter> implements ServiceOrderView {
    @BindView(R.id.serviceOrderRecy)
    RecyclerView serviceOrderRecy;
    @BindView(R.id.serviceOrderRefresh)
    SmartRefreshLayout serviceOrderRefresh;
    ServiceOrderAdapter serviceOrderAdapter;
    String status;
    int page =1;

    public ServiceOrderFragment(EnumServiceOrderStatus status) {
        this.status = status.getId();
    }

    @Override
    public void initView(View rootView) {
        super.initView(rootView);
        serviceOrderRefresh.autoRefresh();
        serviceOrderRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
//                setReflush();
            }
        });
        serviceOrderRefresh.setEnableAutoLoadMore(true);
        serviceOrderRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
//                page++;
//                mPresenter.getOrderLst(page,status);
            }
        });


        serviceOrderAdapter = new ServiceOrderAdapter();
        View emptyView = LayoutInflater.from(getContext()).inflate(R.layout.view_emptyorder,null);
        serviceOrderAdapter.setEmptyView(emptyView);
        serviceOrderRecy.setLayoutManager(new LinearLayoutManager(getContext()));
        serviceOrderRecy.addItemDecoration(new RecycleViewDivider(
                getContext(), LinearLayoutManager.VERTICAL, 10, getResources().getColor(R.color.layout_bg)));
        serviceOrderRecy.setAdapter(serviceOrderAdapter);
        serviceOrderAdapter.setItemClick(new ServiceOrderAdapter.ItemClick() {
            @Override
            public void clickDetail(int position) {
                jumpToActivity(ServiceOrderDetailActivity.class);
            }

            @Override
            public void confimReceived(int position) {
                mPresenter.confimReceiveSimple(serviceOrderAdapter.getData().get(position).getOrderId());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setReflush();
    }

    private void setReflush() {
        page = 1;
        mPresenter.getServiceOrderLst(page,status);
    }

    @Override
    protected ServiceOrderPresenter createPresenter() {
        return new ServiceOrderPresenter((ServiceOrderActivity)getContext());
    }

    @Override
    protected int provideContentViewId() {
        return R.layout.fragment_serviceorder;
    }

    @Override
    public void getServiceOrderLstSucc(OrderBean orderBean) {
        if(page == 1){
            serviceOrderRefresh.finishRefresh();
            serviceOrderAdapter.setNewData(orderBean.getResult().getContent());
        }else{
            serviceOrderRefresh.finishLoadMore(true);
            serviceOrderAdapter.addData(orderBean.getResult().getContent());
        }
    }

    @Override
    public void confimReceivedSimpleSucc() {
        setReflush();
    }
}
