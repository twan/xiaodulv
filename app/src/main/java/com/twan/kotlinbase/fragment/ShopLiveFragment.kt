package com.twan.kotlinbase.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.OnClick
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.enums.PopupPosition
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.MyFragmentPagerAdapter
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.PageData
import com.twan.kotlinbase.bean.ServiceBean
import com.twan.kotlinbase.databinding.*
import com.twan.kotlinbase.event.HotRefresh
import com.twan.kotlinbase.event.RefreshService
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.*
import com.twan.kotlinbase.ui.ServiceDetailActivity
import com.twan.kotlinbase.util.DictUtils
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.widgets.router.Router
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_service.*
import net.lucode.hackware.magicindicator.MagicIndicator
import net.lucode.hackware.magicindicator.ViewPagerHelper
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class ShopLiveFragment(): BaseDataBindingFragment<ActivityServiceBinding>() {
    private lateinit var CHANNELS:Array<String>
    private var need = Need.SHOP_LIVE
    override fun getLayoutId(): Int {
        return R.layout.activity_service
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.visibility=View.VISIBLE
        title?.text="店铺直播综合列表"
        back?.visibility=View.GONE
        searchView?.visibility=View.GONE//一期隐藏
        tv_back_text?.visibility=View.GONE

        CHANNELS = arrayOf("服务","店铺")
        initMagicIndicator3()
    }

    private fun initMagicIndicator3() {
        setupViewPager()
        val magicIndicator = mActivity.findViewById(R.id.magic_indicator3) as MagicIndicator
        magicIndicator.setBackgroundColor(resources.getColor((R.color.head_bg)))
        val commonNavigator = CommonNavigator(mActivity)
        commonNavigator.isAdjustMode = true
        commonNavigator.adapter = object : CommonNavigatorAdapter() {
            override fun getCount(): Int {
                return CHANNELS.size
            }

            override fun getTitleView(context: Context, index: Int): IPagerTitleView {
                val simplePagerTitleView: SimplePagerTitleView = ColorTransitionPagerTitleView(context)
                simplePagerTitleView.normalColor = resources.getColor(R.color.text_99)
                simplePagerTitleView.selectedColor = resources.getColor(R.color.text_black)
                simplePagerTitleView.text = CHANNELS[index]
                simplePagerTitleView.textSize=14f
                simplePagerTitleView.setOnClickListener { view_pager.currentItem = index }
                return simplePagerTitleView
            }

            override fun getIndicator(context: Context): IPagerIndicator {
                val linePagerIndicator = LinePagerIndicator(context)
                linePagerIndicator.mode = LinePagerIndicator.MODE_WRAP_CONTENT
                linePagerIndicator.setColors(resources.getColor(R.color.white))
                return linePagerIndicator
            }
        }
        magicIndicator.navigator = commonNavigator
        ViewPagerHelper.bind(magicIndicator, view_pager)
    }

    private fun setupViewPager() {
        var adapter = MyFragmentPagerAdapter(fragmentManager)
        adapter.addFragment(ServiceFragment(need))
        adapter.addFragment(ShopFragment(need))
        view_pager.adapter = adapter
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        mIsVisible = isVisibleToUser
        if (isVisibleToUser) {
            if (mIsPrepare && mIsVisible) {
//                getData(true)
                EventBus.getDefault().post(RefreshService())
            }

        }
    }
}