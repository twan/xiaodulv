package com.twan.kotlinbase.fragment

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.scwang.smart.refresh.footer.ClassicsFooter
import com.scwang.smart.refresh.header.ClassicsHeader
import com.twan.kotlinbase.R
import com.twan.kotlinbase.adapter.MyBannerImageAdapter
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.bean.BannerBean
import com.twan.kotlinbase.bean.DresserAnli
import com.twan.kotlinbase.bean.DresserService
import com.twan.kotlinbase.bean.Mycoupon
import com.twan.kotlinbase.databinding.FragmentDresserDetailBinding
import com.twan.kotlinbase.databinding.FragmentOrderAllBinding
import com.twan.kotlinbase.databinding.ItemMyCouponBinding
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.util.GlideUtils
import com.twan.kotlinbase.util.MyUtils
import com.youth.banner.config.IndicatorConfig
import com.youth.banner.indicator.CircleIndicator
import kotlinx.android.synthetic.main.activity_single_rv.*
import kotlinx.android.synthetic.main.fragment_dresser_detail.*
import kotlinx.android.synthetic.main.fragment_dresser_detail.refreshLayout
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

class DresserDetailAnliFragment(var userId:String): BaseDataBindingFragment<FragmentDresserDetailBinding>() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_dresser_detail
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        title?.text = ""//resources.getText(R.string.tab3)
        back?.visibility=View.GONE
        initRv()
        getData()
    }

    fun initRv(){
        refreshLayout.setRefreshHeader(ClassicsHeader(mActivity))
        refreshLayout.setOnRefreshListener { getData() }
        refreshLayout.setEnableLoadMore(false)
        refreshLayout.setEnableRefresh(true)
    }

    fun getData(){
        RxHttpScope().launch {
            var req= RxHttp.get("talent/findAllCaseByUserId").add("userId", userId)
                    .toResponse<DresserAnli>().await()
            initPicBanner(req.imageUrls)
            initVideoBanner(req.videoUrls)
        }
    }

    fun initPicBanner(datas:Array<String>) {
        var req = mutableListOf<BannerBean>()
        if (datas.isNotEmpty()){
            datas.forEach {
                req.add(BannerBean(imageUrl = it))
            }
        }
        var adapter = MyBannerImageAdapter(mActivity!!, req)
        you_banner_pic.let {
            it.setIndicatorGravity(IndicatorConfig.Direction.CENTER)
            it.indicator = CircleIndicator(mActivity)
            it.adapter = adapter
        }
    }

    fun initVideoBanner(datas:Array<String>) {
        var req = mutableListOf<BannerBean>()
        if (datas.isNotEmpty()){
            datas.forEach {
                req.add(BannerBean(imageUrl = it))
            }
        }
        var adapter = MyBannerImageAdapter(mActivity!!, req)
        you_banner_video.let {
            it.setIndicatorGravity(IndicatorConfig.Direction.CENTER)
            it.indicator = CircleIndicator(mActivity)
            it.adapter = adapter
        }
    }
}