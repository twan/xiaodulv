//package com.twan.kotlinbase.widgets;
//
//import android.content.Context;
//import android.graphics.Rect;
//import android.util.AttributeSet;
//import android.view.View;
//
//import androidx.core.view.ViewCompat;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//public class MyGridLayoutManager extends LinearLayoutManager {
//
//    public MyGridLayoutManager(Context context) {
//        super(context);
//    }
//
//    public MyGridLayoutManager(Context context, int orientation, boolean reverseLayout) {
//        super(context, orientation, reverseLayout);
//    }
//
//    public MyGridLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }
//
//
//    @Override
//    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
//        super.onLayoutChildren(recycler, state);
//        // 先把所有的View先从RecyclerView中detach掉，然后标记为"Scrap"状态，表示这些View处于可被重用状态(非显示中)。
//        // 实际就是把View放到了Recycler中的一个集合中。
//        detachAndScrapAttachedViews(recycler);
//        calculateChildrenSite(recycler);
//    }
//
//    int totalHeight = 0;
//
//    private void calculateChildrenSite(RecyclerView.Recycler recycler) {
//        totalHeight = 0;
//        for (int i = 0; i < getItemCount(); i++) {
//            // 遍历Recycler中保存的View取出来
//            View view = recycler.getViewForPosition(i);
//            addView(view); // 因为刚刚进行了detach操作，所以现在可以重新添加
//            measureChildWithMargins(view, 0, 0); // 通知测量view的margin值
//            int width = getDecoratedMeasuredWidth(view); // 计算view实际大小，包括了ItemDecorator中设置的偏移量。
//            int height = getDecoratedMeasuredHeight(view);
//
//            Rect mTmpRect = new Rect();
//            //调用这个方法能够调整ItemView的大小，以除去ItemDecorator。
//            calculateItemDecorationsForChild(view, mTmpRect);
//
//            // 调用这句我们指定了该View的显示区域，并将View显示上去，此时所有区域都用于显示View，
//            //包括ItemDecorator设置的距离。
//            layoutDecorated(view, 0, totalHeight, width, totalHeight + height);
//            totalHeight += height;
//        }
//    }
//
//    @Override
//    public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state, int widthSpec, int heightSpec) {
//        final int width = RecyclerView.LayoutManager.chooseSize(widthSpec, getPaddingLeft() + getPaddingRight(),
//                ViewCompat.getMinimumWidth(mList));
//        final int height = RecyclerView.LayoutManager.chooseSize(heightSpec, getPaddingTop() + getPaddingBottom(),
//                ViewCompat.getMinimumHeight(mList));
//        setMeasuredDimension(width, height * mTelList.size());
//    }
//
//}