package com.twan.kotlinbase.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by paopao on 2018/2/5.
 */

public class HttpStatus {

    @SerializedName("success")
    private boolean mSuccess;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("code")
    private int mCode;

    public int getmCode() {
        return mCode;
    }

    public void setmCode(int mCode) {
        this.mCode = mCode;
    }

    public boolean getSuccess() {
        return mSuccess;
    }

    public String getMessage() {
        return mMessage;
    }

    /**
     * API是否请求失败
     *
     * @return 失败返回true, 成功返回false
     */
    public boolean isCodeInvalid() {
        return mSuccess!=true;
    }


}
