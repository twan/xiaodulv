package com.twan.kotlinbase.bean

data class Register(
    var code:String="",//": "",
    var confirmPassword:String="",//": "",
    var invitationCode:String="",//": "",
    var mobile:String="",//": "",
    var password:String="",//": "",
    var tag:String=""//": ""
)

data class UploadPic(
    var isAddBtn:Boolean=false,//是否默认的第一个用于添加的图
    var filepath:String=""//选择的图片的路径

)