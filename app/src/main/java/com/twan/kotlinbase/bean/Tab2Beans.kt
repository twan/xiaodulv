package com.twan.kotlinbase.bean

import java.io.Serializable

data class Discovery(
    var avatarUrl:String,//": "",
    var content:String,//": "",
    var delFlag:String,//": "",
    var id:String,//": "",
    var imagesUrl:MutableList<String>,//": "",
    var isLike:String,//": "",
    var likeCount:Int,//": 0,
    var nickName:String,//": "",
    var releaseTime:String,//": "",
    var replyCount:Int,//": 0,
    var userId:String,//": ""
):Serializable

data class PageData<T>(
    var content: MutableList<T>
)

data class MyComment(
    var avatarUrl:String,//": "",
    var commentText:String,//": "",
    var commentTime:String,//": "",
    var id:String,//": "",
    var momentsId:String,//": "",
    var nickName:String,//": "",
    var userId:String,//": ""
    var replyList:MutableList<Reply>
)

data class Reply(
    var commentId:String,//": "",
    var id:String,//": "",
    var nickName:String,//": "",
    var replyContent:String,//": "",
    var replyTime:String,//": "",
    var replyType:String,//": "",
    var toUserId:String,//": "",
    var userId:String,//": ""
)

data class Tab2Dynaic(
    var actorType:String,//": "",
    var amount:String,//": "",
    var categoryLabel:String,//": "",
    var deleteFlag:String,//": "",
    var demandId:String,//": "",
    var demandType:String,//": "",
    var grabOrderPeopleNumber:Int,//": 0,
    var grabbedOrderPeopleNumber:Int,//": 0,
    var id:String,//": "",
    var imageUrl:String,//": "",
    var liveTime:String,//": "",
    var makeTime:String,//": "",
    var modelType:String,//": "",
    var payMoneyPeopleNumber:Int,//": 0,
    var platformName:String,//": "",
    var price:String,//": "",
    var shopUrl:String="",//": "",
    var title:String,//": ""
)

//用户的入驻状态
data class SettleStatus(
    var personalSettle:String,//个人 1认证中  0未认证
    var companySettle:String,//企业  1认证中  0未认证
)