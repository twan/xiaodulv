package com.twan.kotlinbase.bean;

/**
 * @anthor zhoujr
 * @time 2021/4/15 15:19
 * @describe
 */
public class SelPayWayBean {
    private int res;
    private String title;
    private boolean isSel;

    public SelPayWayBean(int res, String title, boolean isSel) {
        this.res = res;
        this.title = title;
        this.isSel = isSel;
    }

    public int getRes() {
        return res;
    }

    public void setRes(int res) {
        this.res = res;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSel() {
        return isSel;
    }

    public void setSel(boolean sel) {
        isSel = sel;
    }
}
