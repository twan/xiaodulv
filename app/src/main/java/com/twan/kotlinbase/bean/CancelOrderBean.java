package com.twan.kotlinbase.bean;

/**
 * @anthor zhoujr
 * @time 2021/4/19 11:02
 * @describe
 */
public class CancelOrderBean {

    /**
     * code : 0
     * message :
     * result : {"buyerPhone":"","closeTime":"","consignTime":"","deleteFlag":"","endTime":"","id":"","orderCode":"","orderType":"","paymentAmount":0,"paymentTime":"","paymentType":"","remarks":"","status":"","title":"","userId":""}
     * success : true
     * timestamp : 0
     */

    private int code;
    private String message;
    private ResultBean result;
    private boolean success;
    private int timestamp;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public static class ResultBean {
        /**
         * buyerPhone :
         * closeTime :
         * consignTime :
         * deleteFlag :
         * endTime :
         * id :
         * orderCode :
         * orderType :
         * paymentAmount : 0
         * paymentTime :
         * paymentType :
         * remarks :
         * status :
         * title :
         * userId :
         */

        private String buyerPhone;
        private String closeTime;
        private String consignTime;
        private String deleteFlag;
        private String endTime;
        private String id;
        private String orderCode;
        private String orderType;
        private int paymentAmount;
        private String paymentTime;
        private String paymentType;
        private String remarks;
        private String status;
        private String title;
        private String userId;

        public String getBuyerPhone() {
            return buyerPhone;
        }

        public void setBuyerPhone(String buyerPhone) {
            this.buyerPhone = buyerPhone;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        public String getConsignTime() {
            return consignTime;
        }

        public void setConsignTime(String consignTime) {
            this.consignTime = consignTime;
        }

        public String getDeleteFlag() {
            return deleteFlag;
        }

        public void setDeleteFlag(String deleteFlag) {
            this.deleteFlag = deleteFlag;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public int getPaymentAmount() {
            return paymentAmount;
        }

        public void setPaymentAmount(int paymentAmount) {
            this.paymentAmount = paymentAmount;
        }

        public String getPaymentTime() {
            return paymentTime;
        }

        public void setPaymentTime(String paymentTime) {
            this.paymentTime = paymentTime;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }
}
