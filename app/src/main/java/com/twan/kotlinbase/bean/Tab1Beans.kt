package com.twan.kotlinbase.bean

import com.twan.kotlinbase.app.Need
import java.io.Serializable
data class HotTuigaung(
        var id:String,//": "",
        var imageUrl:String,//": "",
        var price:Float,//": 0,
        var serviceType:String,//": "",
        var shootStyle:String,//": "",
        var talentSkill:String,//": "",
        var title:String,//": "",
        var userId:String,//": "",
        var videoUrl:String,//": ""
)

data class HotZhibo(
        var caseUrl:String,//": "",
        var commissionRate:String,//": "",
        var deleteFlag:String,//": "",
        var goodAtLabel:String,//": "",
        var goodAtValue:String,//": "",
        var id:String,//": "",
        var imageUrl:String,//": "",
        var liveDate:String,//": "",
        var liveSparkle:String,//": "",
        var price:String,//": "",
        var recommendFlag:String,//": "",
        var recommendTime:String,//": "",
        var serviceType:String,//": "",
        var talentSkill:String,//": "",
        var title:String,//": "",
        var userId:String,//": "",
        var videoUrl:String,//": ""
):Serializable

data class HotActor(
        var actorType:String,//": "",
        var age:Int,//": 0,
        var complexion:String,//": "",
        var country:String,//": "",
        var deleteFlag:String,//": "",
        var dieCardImages:String,//": "",
        var dieCardVideo:String,//": "",
        var figure:String,//": "",
        var height:Int,//": 0,
        var id:String,//": "",
        var mandarinLevel:String,//": "",
        var price:String,//": "",
        var remarks:String,//": "",
        var serviceType:String,//": "",
        var sex:String,//": "",
        var tag:String,//": "",
        var title:String,//": "",
        var userId:String,//": "",
        var weight:Int,//": 0
)

data class ServiceDetail(
    var need:Need,//种类的索引,自己加的
    var isActor:Boolean,//是否是演员,自己加的

    var actorModelItemDTO:ActorModelItemDTO,
    var videoItemDTO:VideoItemDTO,
    var popularizeItemDTO:PopularizeItemDTO,
    var appraiseDTO:AppraiseDTO,
    var baseItemDTO:BaseItemDTO,
    var detailImagesDTO:DetailImagesDTO,
    var liveItemDTO:LiveItemDTO,
    var talentDTO:TalentDTO,
    var actorItemDTO:ActorItemDTO,
    var modelItemDTO:ModelItemDTO,
)
data class ModelItemDTO(
    var age:Int,//": 0,
    var complexion:String,//": "",
    var country:String,//": "",
    var height:String,//": "",
    var mandarinLevel:String,//": "",
    var modelType:String,//": "",
    var number:Int,//": 0,
    var price:String,//": "",
    var remarks:String,//": "",
    var vitalStatistics:String,//": "",
    var weight:String,//": ""
)
data class ActorItemDTO(
    var actorType:String,//": "",
    var age:Int,//": 0,
    var complexion:String,//": "",
    var country:String,//": "",
    var figure:String,//": "",
    var height:String,//": "",
    var mandarinLevel:String,//": "",
    var remarks:String,//": "",
    var weight:String,//": ""
)
data class ActorModelItemDTO(
    var imagesUrl:MutableList<String>,//": [],
    var videoUrls:MutableList<String>,//": []
)
data class VideoItemDTO(
    var actorProvide:String,//": "",
    var placeAddress:String,//": "",
    var remakes:String,//": "",
    var shootNum:Int,//": 0,
    var teamNum:String,
    var shootSpecialtyLabel:String,//": "",
    var specialtyTypeLabel:String,
    var siteType:String,//": ""
)
data class AppraiseDTO(
    var appraiseNickName:String,//": "",
    var avatarUrl:String,//": "",
    var content:String,//": "",
    var createTime:String,//": ""
)
data class BaseItemDTO(
    var imageUrl:String,//": "",
    var price:String,//": "",
    var salesVolume:Int,//": 0,
    var title:String,//": ""
)
data class DetailImagesDTO(
    var imagesUrl:MutableList<String>,//": [],
    var videoUrls:MutableList<String>,//": []
)
data class LiveItemDTO(
    var commissionRate:String,//": "",
    var goodAt:String,//": "",
    var liveDate:String,//": "",
    var liveSparkle:String,//": "",
    var talentSkillLabel:String,//": ""
)
data class TalentDTO(
    var avatarUlr:String,//": "",
    var nickName:String,//": "",
    var userId:String,//": ""
    var talentId:String,
    var phone:String,
)
data class PopularizeItemDTO(
    var platformList:MutableList<Platform>,
    var shootSkill:String,//": "",
    var shootStyle:String,//": "",
    var talentSkill:String,//": ""

    //自己加
    var perPrices:String,// 各平台价格
    var perFans:String,//
    var perAccounts:String,//
)
data class Platform(
    var cooperationPrice:String="",//": "",
    var fansNum:String="",//": "",
    var id:String="",//": "",
    var platformAccount:String="",//": "",
    var platformLabel:String="",//": "",
    var platformValue:String="",//": "",
    var shortVideoPromoteId:String="",//": ""
):Serializable


//达人详情
data class DresserDetail(
    var assessScore:Int,//": 0,
    var avatarUrl:String,//": "",
    var businessVolume:Int,//": 0,
    var nickName:String,//": "",
    var selfIntroduction:String,//": "",
    var status:String,//": "",
    var tagLabel:String,//": "",
    var talentTypeLabel:String,//": "",
    var userId:String,//": ""
    var area:String,
    var fansNumber:String,
)
//动态添加 模特演员
data class ModelActor(
    var id:String,//": "",
    var modelActorName:String,//": "",
    var modelImageUrl:String,//": "",
    var modelVideoUrl:String,//": "",
    var parentId:String,//": "",
    var remark:String,//": ""
)
