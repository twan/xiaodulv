package com.twan.kotlinbase.bean

import java.io.Serializable

//我的收藏,我的足迹
data class MyFavorFoot(
    var favoritesId:String,//": "",
    var footprintId:String,//": "",
    var id:String,//": "",
    var imagesUrl:String,//": "",
    var price:Float,//": 0,
    var sales:Int,//": 0,
    var title:String,//": "",
    var userId:String,//": ""
)
//我的关注
data class MyFocus(
    var attentionUserId:String,//": "",
    var attentionUserName:String,//": "",
    var goodAt:String,//": "",
    var id:String,//": "",
    var performance:String,//": "",
    var tags:String,//": "",
    var timetable:String,//": "",
    var userId:String,//": ""
    var imagesUrl:String
)
//我的优惠券
data class Mycoupon(
    var amount:String,//": 0,
    var couponImage:String,//": "",
    var couponName:String,//": "",
    var fullReduction:String,//": "",
    var id:String,//": "",
    var nickName:String,//": "",
    var status:String,//": "",
    var userId:String,//": "",
    var validEndDate:String,//": "",
    var validStartDate:String,//": ""
)
//我的反馈
data class Myfeedback(
    var feedbackContent:String,//": "",
    var feedbackImages:String,//": "",
    var feedbackType:String,//": "",
    var id:String,//": "",
    var nickName:String,//": "",
    var replyContent:String,//": "",
    var status:String,//": "",
    var userId:String,//": ""
    var createTime:String
)
//个人账户
data class PersonalAccount(
    var accountBalance:String,//": 0,
    var accountName:String,//": "",
    var checkCode:String,//": "",
    var earnestMoney:String,//": "",
    var freezeBalance:String,//": 0,
    var id:String,//": "",
    var isPay:Int,//": 0,
    var userId:String,//": "",
    var withdrawalBalance:String,//": 0
):Serializable

//发布服务->短视频拍摄->规格
data class GuigeProperty(
    var attrName:String,//": "",
    var attrSort:String,//": "",
    var id:String,//": "",
    var serviceId:String,//": ""
)
data class ChidlGuige(
    var attrKeyId:String,//": "",
    var attrKeyName:String,//": "",
    var attrLabel:String,//": "",
    var attrSymbol:String,//": "",
    var id:String,//": "",
    var serviceId:String,//": ""
)
data class ServiceAttrValueDTO(
    var attrKeyId:String,//": "",
    var attrKeyName:String,//": "",
    var attrValueDTOS:MutableList<AttrValueDTO>,
    //var id:String,//": "",
    var serviceId:String,//": ""
):Serializable
data class AttrValueDTO(
    var attrLabel:String,//": "",
    var attrSymbol:String,//": ""
):Serializable
//规格组合
data class GuigeZuhe(
    var attrSymbolLabel:String,//": "",
    var attrSymbolPath:String,//": "",
    var id:String,//": "",
    var price:String,//": 0,
    var serviceId:String,//": ""
)
//能否预约
data class CanSchdule(
    var desc:String,
    var isCheck:Boolean=false,
    var isCan:Boolean=true,

)
//下单时填写的信息
data class OrderBaseDTO(
    var buyerPhone:String="",//": "",
    var couponIds:String="",//": "",
    var paymentAmount:String="",//": 0,
    var paymentType:String="",//": "",
    var remarks:String="",//": "",
    var serviceId:String="",//": "",
    var title:String?="",//": "",
    var userId:String="",//": ""
):Serializable
//下单时填写的预约信息
data class ShopLiveFormat(
    var buyNumber:String="",//": 0,
    var reservationDate:String="",//": "",
    var timeLengthLabel:String="",//": "",
    var timeLengthValue:Int=0,//": "",
    var times:String="",//": ""
):Serializable
//店铺直播下单
data class CommitShopLiveOrder(
    var orderBaseDTO:OrderBaseDTO,
    var shopLiveFormat:ShopLiveFormat
)
//模特演员下单
data class CommitActorModelOrder(
    var orderBaseDTO:OrderBaseDTO,
    var modelAndActorFormat:ModelAndActorFormat
)
//短视频推广下单
data class CommitVideoExtOrder(
    var orderBaseDTO:OrderBaseDTO,
    var shortVideoPromoteFormat:ShortVideoPromoteFormat
)
//短视频制作下单
data class CommitVideoMakeOrder(
        var orderBaseDTO:OrderBaseDTO,
        var shortVideoMakeFormat:ShortVideoMakeFormat
)
//短视频制作下单
data class ShortVideoMakeFormat(
    var buyNumber:String,//": 0,
    var latestCompletionDay:Int,//": 0,
    var serviceSkuId:String,//": ""
):Serializable
data class ShortVideoPromoteFormat(
    var buyNumber:Int,//": 0,
    var latestCompletionDay:Int,//": 0
):Serializable
data class ModelAndActorFormat(
    var buyNumber:Int
)
//下单成功, 订单信息
data class OrderDTO(
    var buyerPhone:String="",//": "",
    var closeTime:String="",//": "",
    var consignTime:String="",//": "",
    var endTime:String="",//": "",
    var id:String="",//": "",
    var orderCode:String="",//": "",
    var paymentAmount:String="",//": 0,
    var paymentTime:String="",//": "",
    var paymentType:String="",//": "",
    var remarks:String="",//": "",
    var status:String="",//": "",
    var title:String="",//": "",
    var userId:String="",//": ""
):Serializable

data class Recharge(
    var id:String,//": "",
    var sellingPrice:String,//": 0,
    var showPrice:String,//": 0,
    var sort:String,//": 0,
    var status:String,//": ""
)
//tab4 9项数字
data class CatoeryNums(
    var pendingEvaluateSum:String,//": 0,
    var pendingPaymentSum:String,//": 0,
    var pendingReceivedSum:String,//": 0,
    var pendingShippedSum:String,//": 0,
    var personalAttentionSum:String,//": 0,
    var personalCouponSum:String,//": 0,
    var personalFavoritesSum:String,//": 0,
    var personalFootprintSum:String,//": 0,
    var refundSum:String,//": 0
)

data class RechaegeHistory(
    var id:String,//": "",
    var rechargeAmount:String,//": 0,
    var status:String,//": "",
    var userId:String,//": ""
    var createTime:String,//
)

data class TradeHistory(
    var buyerLogonId:String,//": "",
    var id:String,//": "",
    var orderCode:String,//": "",
    var orderId:String,//": "",
    var orderStatus:String,//": "",
    var payTime:String,//": "",
    var payType:String,//": "",
    var sellerId:String,//": "",
    var signType:String,//": "",
    var totalAmount:String,//": 0,
    var tradeNo:String,//": "",
    var userId:String,//": ""

)
data class BankItem(
    var accountBank:String,//": "",
    var accountBranch:String,//": "",
    var accountType:String,//": "",
    var bankCardName:String,//": "",
    var cardNumber:String,//": "",
    var id:String,//": "",
    var paymentPassword:String,//": "",
    var realName:String,//": "",
    var userId:String,//": ""
)

data class WithdrawHistory(
    var id:String,//": "",
    var status:String,//": "",
    var statusDesc:String,//自己加的
    var userId:String,//": "",
    var withdrawalAmount:String,//": 0
    var createTime:String,//
)

//订单详情
data class OrderDetail(
    var orderContentDTO:OrderContentDTO,
    var orderHearderInfo:OrderHearderInfo,
    var orderServiceDTO:OrderServiceDTO
)
data class OrderContentDTO(
    var couponAmount:String,//": "",
    var createTime:String,//": "",
    var orderAmount:String,//": "",
    var orderCode:String,//": "",
    var payTypeLabel:String,//": "",
    var paymentAmount:String,//": 0,
    var paymentTime:String,//": ""
)
data class OrderHearderInfo(
    var description:String,//": "",
    var statusLabel:String,//": ""
)
data class OrderServiceDTO(
    var avatarUrl:String,//": "",
    var createTime:String,//": "",
    var imageUrl:String,//": "",
    var nickName:String,//": "",
    var price:String,//": "",
    var title:String,//": ""
)

///邀请好友
data class InviteFried(
    var id:String,//": "",
    var toUserId:String,//": "",
    var toUserName:String,//": "",
    var userId:String,//": ""
)
//个人信息
data class PersonInfo(
    var id:String,//": "389131601201598464",
    var createBy:String,//": null,
    var updatedBy:String,//": null,
    var createTime:String,//": "2021-03-30 09:22:24",
    var updateTime:String,//": "2021-03-30 09:22:24",
    var userName:String,//": null,
    var password:String,//": null,
    var nickName:String,//": null,
    var tag:String,//": null,
    var gender:String,//": null,
    var phone:String,//": null,
    var email:String,//": null,
    var avatarUrl:String,//": "https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png",
    var invitationCode:String,//": null,
    var enabled:String,//": 1,
    var userType:String,//": "0",
    var attestationStatus:String,//": "0",
    var talentType:String,//": null
)

//达人直播入驻提交
data class DresserParam(
    var actualName:String="",//": "",
    var area:String="",//": "",
    var avatarUlr:String="",//": "",
    var backUrl:String="",//": "",
    var cardId:String="",//": "",
    var caseUrl:String="",//": "",
    var frontUrl:String="",//": "",
    var goodAtLabel:String="",//": "",
    var goodAtValue:String="",//": "",
    var handHeldUrl:String="",//": "",
    var id:String="",//": "",
    var imageUrl:String="",//": "",
    var nickName:String="",//": "",
    var selfIntroduction:String="",//": "",
    var status:String="",//": "",
    var tagLabel:String="",//": "",
    var tagValue:String="",//": "",
    var userId:String="",//": "",
    var videoUrl:String="",//": ""
):Serializable
//服务商入驻参数
data class ServiceProParam(
    var area:String="",//
    var avatarUlr:String="",//
    var backUrl:String="",//
    var businessLicenseUrl:String="",//
    var businessRegistrationCode:String="",//
    var caseUrl:String="",//
    var companyName:String="",//
    var enterpriseName:String="",//
    var frontUrl:String="",//
    var goodAtLabel:String="",//
    var goodAtValue:String="",//
    var handHeldBackUrl:String="",//
    var handHeldFrontUrl:String="",//
    var id:String="",//
    var imageUrl:String="",//
    var selfIntroduction:String="",//
    var status:String="",//
    var userId:String="",//
    var videoUrl:String="",//
):Serializable
//短视频推广入驻
data class ShortVideoExtParam(
    var actualName:String="",//": "",
    var area:String="",//
    var avatarUlr:String="",//
    var backUrl:String="",//
    var frontUrl:String="",//
    var goodAtLabel:String="",//
    var goodAtValue:String="",//
    var handHeldUrl:String="",//
    var id:String="",//
    var idCard:String="",//
    var imageUrl:String="",//
    var nickName:String="",//
    var platforms:MutableList<Platform>?=null,
    var selfIntroduction:String="",//
    var shootStyleLabel:String="",//
    var shootStyleValue:String="",//
    var status:String="",//
    var userId:String="",//
    var videoUrl:String="",//
):Serializable
//演员模特入驻
data class ActorModelParam(
    var acceptAreaTypeLabel:String="",
    var acceptAreaTypeValue:String="",
    var actualName:String="",
    var age:String="",
    var area:String="",
    var avatarUlr:String="",
    var backUrl:String="",
    var cardId:String="",
    var caseUrl:String="",
    var complexion:String="",
    var country:String="",
    var frontUrl:String="",
    var handHeldUrl:String="",
    var height:String="",
    var id:String="",
    var imageUrl:String="",
    var mandarinLevel:String="",
    var modelTypeLabel:String="",
    var modelTypeValue:String="",
    var nickName:String="",
    var selfIntroduction:String="",
    var sex:String="",
    var status:String="",
    var tagLabel:String="",
    var tagValue:String="",
    var userId:String="",
    var videoUrl:String="",
    var vitalStatistics:String="",
    var weight:String="",
):Serializable

//服务管理列表
data class ServiceManageItem(
    var area:String,//
    var attestationStatus:String,//
    var deleteFlag:String,//
    var evaluationContent:String,//
    var evaluationScore:String,//": 0,
    var id:String,//
    var imageUrl:String,//
    var price:String,//": 0,
    var recommendFlag:String,//
    var recommendTime:String,//
    var saleStatus:String,//
    var salesAmount:String,//
    var salesSum:String,//": 0,
    var serviceId:String,//
    var serviceTypeLabel:String,//
    var serviceTypeValue:String,//
    var title:String,//
    var userId:String,//
    var userNick:String,//": ""
)
//需求管理列表
data class XuqiuManageItem(
    var actorProvide:String,//
    var actorType:String,//
    var adName:String,//
    var age:String,//": 0,
    var categoryName:String,//
    var commodityPrice:String,//": 0,
    var complexion:String,//
    var cooperationPrice:String,//": 0,
    var country:String,//
    var demandId:String,//
    var demandType:String,//
    var directionalUrl:String,//
    var expertNum:String,//": 0,
    var height:String,//": 0,
    var hourlyRate:String,//
    var imageUrl:String,//
    var liveDate:String,//
    var mandarinLevel:String,//
    var modelType:String,//
    var number:String,//": 0,
    var perPrice:String,//": 0,
    var placeAddress:String,//
    var placeType:String,//
    var platformId:String,//
    var platformName:String,//
    var popularizeUrl:String,//
    var price:String,//": 0,
    var productSellingPoint:String,//
    var sex:String,//
    var shootDate:String,//
    var shopUrl:String,//
    var siteType:String,//
    var title:String,//
    var vitalStatistics:String,//
    var weight:String,//": 0
)
//达人详情->案例列表
data class DresserAnli(
    var imageUrls:Array<String>,
    var videoUrls:Array<String>
)
//达人详情->服务列表
data class DresserService(
    var goodAt:String,//": "",
    var imageUrl:String,//": "",
    var liveDate:String,//": "",
    var price:String,//": 0,
    var serviceId:String,//": "",
    var serviceType:String,//": "",
    var title:String,//": ""
)
//达人详情->评价列表
data class DresserComment(
    var appraiseNickName:String,//": "",
    var appraiseUserId:String,//": "",
    var businessId:String,//": "",
    var content:String,//": "",
    var id:String,//": "",
    var replyContent:String,//": "",
    var status:String,//": "",
    var type:String,//": ""
    var createTime: String,
    var imageUrl:String
)