package com.twan.kotlinbase.bean;

import java.util.List;

/**
 * @anthor zhoujr
 * @time 2021/4/13 14:20
 * @describe
 */
public class OrderBean {

    /**
     * code : 0
     * message :
     * result : {"content":[{"buyerAvatarUrl":"","buyerId":"","buyerNick":"","buyerRate":"","buyerReceipt":"","couponIds":"","deleteFlag":"","id":"","orderId":"","picUrl":"","sampleImageUrls":"","sampleVideoUrls":"","sellerAvatarUrl":"","sellerId":"","sellerNick":"","serviceId":"","serviceTypeValue":"","shippingCode":"","shippingName":"","specificationLive":{"buyNumber":0,"id":"","orderId":"","reservationDate":"","serviceId":"","timeLengthLabel":"","timeLengthValue":"","times":""},"specificationPopularize":{"buyNumber":0,"id":"","latestCompletionDay":0,"orderId":""},"specificationVideo":{"buyNumber":0,"id":"","latestCompletionDay":0,"orderId":"","serviceSku":{"attrSymbolLabel":"","attrSymbolPath":"","id":"","price":0,"serviceId":""},"serviceSkuId":""},"status":"","title":"","totalFee":0}],"empty":true,"first":true,"last":true,"number":0,"numberOfElements":0,"pageable":{"offset":0,"pageNumber":0,"pageSize":0,"paged":true,"sort":{"empty":true,"sorted":true,"unsorted":true},"unpaged":true},"size":0,"sort":{"empty":true,"sorted":true,"unsorted":true},"totalElements":0,"totalPages":0}
     * success : true
     * timestamp : 0
     */

    private int code;
    private String message;
    private ResultBean result;
    private boolean success;
    private int timestamp;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public static class ResultBean {
        /**
         * content : [{"buyerAvatarUrl":"","buyerId":"","buyerNick":"","buyerRate":"","buyerReceipt":"","couponIds":"","deleteFlag":"","id":"","orderId":"","picUrl":"","sampleImageUrls":"","sampleVideoUrls":"","sellerAvatarUrl":"","sellerId":"","sellerNick":"","serviceId":"","serviceTypeValue":"","shippingCode":"","shippingName":"","specificationLive":{"buyNumber":0,"id":"","orderId":"","reservationDate":"","serviceId":"","timeLengthLabel":"","timeLengthValue":"","times":""},"specificationPopularize":{"buyNumber":0,"id":"","latestCompletionDay":0,"orderId":""},"specificationVideo":{"buyNumber":0,"id":"","latestCompletionDay":0,"orderId":"","serviceSku":{"attrSymbolLabel":"","attrSymbolPath":"","id":"","price":0,"serviceId":""},"serviceSkuId":""},"status":"","title":"","totalFee":0}]
         * empty : true
         * first : true
         * last : true
         * number : 0
         * numberOfElements : 0
         * pageable : {"offset":0,"pageNumber":0,"pageSize":0,"paged":true,"sort":{"empty":true,"sorted":true,"unsorted":true},"unpaged":true}
         * size : 0
         * sort : {"empty":true,"sorted":true,"unsorted":true}
         * totalElements : 0
         * totalPages : 0
         */

        private boolean empty;
        private boolean first;
        private boolean last;
        private int number;
        private int numberOfElements;
        private PageableBean pageable;
        private int size;
        private SortBeanX sort;
        private int totalElements;
        private int totalPages;
        private List<ContentBean> content;

        public boolean isEmpty() {
            return empty;
        }

        public void setEmpty(boolean empty) {
            this.empty = empty;
        }

        public boolean isFirst() {
            return first;
        }

        public void setFirst(boolean first) {
            this.first = first;
        }

        public boolean isLast() {
            return last;
        }

        public void setLast(boolean last) {
            this.last = last;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public int getNumberOfElements() {
            return numberOfElements;
        }

        public void setNumberOfElements(int numberOfElements) {
            this.numberOfElements = numberOfElements;
        }

        public PageableBean getPageable() {
            return pageable;
        }

        public void setPageable(PageableBean pageable) {
            this.pageable = pageable;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public SortBeanX getSort() {
            return sort;
        }

        public void setSort(SortBeanX sort) {
            this.sort = sort;
        }

        public int getTotalElements() {
            return totalElements;
        }

        public void setTotalElements(int totalElements) {
            this.totalElements = totalElements;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public List<ContentBean> getContent() {
            return content;
        }

        public void setContent(List<ContentBean> content) {
            this.content = content;
        }

        public static class PageableBean {
            /**
             * offset : 0
             * pageNumber : 0
             * pageSize : 0
             * paged : true
             * sort : {"empty":true,"sorted":true,"unsorted":true}
             * unpaged : true
             */

            private int offset;
            private int pageNumber;
            private int pageSize;
            private boolean paged;
            private SortBean sort;
            private boolean unpaged;

            public int getOffset() {
                return offset;
            }

            public void setOffset(int offset) {
                this.offset = offset;
            }

            public int getPageNumber() {
                return pageNumber;
            }

            public void setPageNumber(int pageNumber) {
                this.pageNumber = pageNumber;
            }

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public boolean isPaged() {
                return paged;
            }

            public void setPaged(boolean paged) {
                this.paged = paged;
            }

            public SortBean getSort() {
                return sort;
            }

            public void setSort(SortBean sort) {
                this.sort = sort;
            }

            public boolean isUnpaged() {
                return unpaged;
            }

            public void setUnpaged(boolean unpaged) {
                this.unpaged = unpaged;
            }

            public static class SortBean {
                /**
                 * empty : true
                 * sorted : true
                 * unsorted : true
                 */

                private boolean empty;
                private boolean sorted;
                private boolean unsorted;

                public boolean isEmpty() {
                    return empty;
                }

                public void setEmpty(boolean empty) {
                    this.empty = empty;
                }

                public boolean isSorted() {
                    return sorted;
                }

                public void setSorted(boolean sorted) {
                    this.sorted = sorted;
                }

                public boolean isUnsorted() {
                    return unsorted;
                }

                public void setUnsorted(boolean unsorted) {
                    this.unsorted = unsorted;
                }
            }
        }

        public static class SortBeanX {
            /**
             * empty : true
             * sorted : true
             * unsorted : true
             */

            private boolean empty;
            private boolean sorted;
            private boolean unsorted;

            public boolean isEmpty() {
                return empty;
            }

            public void setEmpty(boolean empty) {
                this.empty = empty;
            }

            public boolean isSorted() {
                return sorted;
            }

            public void setSorted(boolean sorted) {
                this.sorted = sorted;
            }

            public boolean isUnsorted() {
                return unsorted;
            }

            public void setUnsorted(boolean unsorted) {
                this.unsorted = unsorted;
            }
        }

        public static class ContentBean {
            /**
             * buyerAvatarUrl :
             * buyerId :
             * buyerNick :
             * buyerRate :
             * buyerReceipt :
             * couponIds :
             * deleteFlag :
             * id :
             * orderId :
             * picUrl :
             * sampleImageUrls :
             * sampleVideoUrls :
             * sellerAvatarUrl :
             * sellerId :
             * sellerNick :
             * serviceId :
             * serviceTypeValue :
             * shippingCode :
             * shippingName :
             * specificationLive : {"buyNumber":0,"id":"","orderId":"","reservationDate":"","serviceId":"","timeLengthLabel":"","timeLengthValue":"","times":""}
             * specificationPopularize : {"buyNumber":0,"id":"","latestCompletionDay":0,"orderId":""}
             * specificationVideo : {"buyNumber":0,"id":"","latestCompletionDay":0,"orderId":"","serviceSku":{"attrSymbolLabel":"","attrSymbolPath":"","id":"","price":0,"serviceId":""},"serviceSkuId":""}
             * status :
             * title :
             * totalFee : 0
             */

            private String buyerAvatarUrl;
            private String buyerId;
            private String buyerNick;
            private String buyerRate;
            private String buyerReceipt;
            private String couponIds;
            private String deleteFlag;
            private String id;
            private String orderId;
            private String picUrl;
            private String sampleImageUrls;
            private String sampleVideoUrls;
            private String sellerAvatarUrl;
            private String sellerId;
            private String sellerNick;
            private String serviceId;
            private String serviceTypeValue;
            private String shippingCode;
            private String shippingName;
            private SpecificationLiveBean specificationLive;
            private SpecificationPopularizeBean specificationPopularize;
            private SpecificationVideoBean specificationVideo;
            private int status;
            private String title;
            private String totalFee;

            public String getBuyerAvatarUrl() {
                return buyerAvatarUrl;
            }

            public void setBuyerAvatarUrl(String buyerAvatarUrl) {
                this.buyerAvatarUrl = buyerAvatarUrl;
            }

            public String getBuyerId() {
                return buyerId;
            }

            public void setBuyerId(String buyerId) {
                this.buyerId = buyerId;
            }

            public String getBuyerNick() {
                return buyerNick;
            }

            public void setBuyerNick(String buyerNick) {
                this.buyerNick = buyerNick;
            }

            public String getBuyerRate() {
                return buyerRate;
            }

            public void setBuyerRate(String buyerRate) {
                this.buyerRate = buyerRate;
            }

            public String getBuyerReceipt() {
                return buyerReceipt;
            }

            public void setBuyerReceipt(String buyerReceipt) {
                this.buyerReceipt = buyerReceipt;
            }

            public String getCouponIds() {
                return couponIds;
            }

            public void setCouponIds(String couponIds) {
                this.couponIds = couponIds;
            }

            public String getDeleteFlag() {
                return deleteFlag;
            }

            public void setDeleteFlag(String deleteFlag) {
                this.deleteFlag = deleteFlag;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getOrderId() {
                return orderId;
            }

            public void setOrderId(String orderId) {
                this.orderId = orderId;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getSampleImageUrls() {
                return sampleImageUrls;
            }

            public void setSampleImageUrls(String sampleImageUrls) {
                this.sampleImageUrls = sampleImageUrls;
            }

            public String getSampleVideoUrls() {
                return sampleVideoUrls;
            }

            public void setSampleVideoUrls(String sampleVideoUrls) {
                this.sampleVideoUrls = sampleVideoUrls;
            }

            public String getSellerAvatarUrl() {
                return sellerAvatarUrl;
            }

            public void setSellerAvatarUrl(String sellerAvatarUrl) {
                this.sellerAvatarUrl = sellerAvatarUrl;
            }

            public String getSellerId() {
                return sellerId;
            }

            public void setSellerId(String sellerId) {
                this.sellerId = sellerId;
            }

            public String getSellerNick() {
                return sellerNick;
            }

            public void setSellerNick(String sellerNick) {
                this.sellerNick = sellerNick;
            }

            public String getServiceId() {
                return serviceId;
            }

            public void setServiceId(String serviceId) {
                this.serviceId = serviceId;
            }

            public String getServiceTypeValue() {
                return serviceTypeValue;
            }

            public void setServiceTypeValue(String serviceTypeValue) {
                this.serviceTypeValue = serviceTypeValue;
            }

            public String getShippingCode() {
                return shippingCode;
            }

            public void setShippingCode(String shippingCode) {
                this.shippingCode = shippingCode;
            }

            public String getShippingName() {
                return shippingName;
            }

            public void setShippingName(String shippingName) {
                this.shippingName = shippingName;
            }

            public SpecificationLiveBean getSpecificationLive() {
                return specificationLive;
            }

            public void setSpecificationLive(SpecificationLiveBean specificationLive) {
                this.specificationLive = specificationLive;
            }

            public SpecificationPopularizeBean getSpecificationPopularize() {
                return specificationPopularize;
            }

            public void setSpecificationPopularize(SpecificationPopularizeBean specificationPopularize) {
                this.specificationPopularize = specificationPopularize;
            }

            public SpecificationVideoBean getSpecificationVideo() {
                return specificationVideo;
            }

            public void setSpecificationVideo(SpecificationVideoBean specificationVideo) {
                this.specificationVideo = specificationVideo;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getTotalFee() {
                return totalFee;
            }

            public void setTotalFee(String totalFee) {
                this.totalFee = totalFee;
            }

            public static class SpecificationLiveBean {
                /**
                 * buyNumber : 0
                 * id :
                 * orderId :
                 * reservationDate :
                 * serviceId :
                 * timeLengthLabel :
                 * timeLengthValue :
                 * times :
                 */

                private int buyNumber;
                private String id;
                private String orderId;
                private String reservationDate;
                private String serviceId;
                private String timeLengthLabel;
                private String timeLengthValue;
                private String times;

                public int getBuyNumber() {
                    return buyNumber;
                }

                public void setBuyNumber(int buyNumber) {
                    this.buyNumber = buyNumber;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getOrderId() {
                    return orderId;
                }

                public void setOrderId(String orderId) {
                    this.orderId = orderId;
                }

                public String getReservationDate() {
                    return reservationDate;
                }

                public void setReservationDate(String reservationDate) {
                    this.reservationDate = reservationDate;
                }

                public String getServiceId() {
                    return serviceId;
                }

                public void setServiceId(String serviceId) {
                    this.serviceId = serviceId;
                }

                public String getTimeLengthLabel() {
                    return timeLengthLabel;
                }

                public void setTimeLengthLabel(String timeLengthLabel) {
                    this.timeLengthLabel = timeLengthLabel;
                }

                public String getTimeLengthValue() {
                    return timeLengthValue;
                }

                public void setTimeLengthValue(String timeLengthValue) {
                    this.timeLengthValue = timeLengthValue;
                }

                public String getTimes() {
                    return times;
                }

                public void setTimes(String times) {
                    this.times = times;
                }
            }

            public static class SpecificationPopularizeBean {
                /**
                 * buyNumber : 0
                 * id :
                 * latestCompletionDay : 0
                 * orderId :
                 */

                private int buyNumber;
                private String id;
                private int latestCompletionDay;
                private String orderId;

                public int getBuyNumber() {
                    return buyNumber;
                }

                public void setBuyNumber(int buyNumber) {
                    this.buyNumber = buyNumber;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public int getLatestCompletionDay() {
                    return latestCompletionDay;
                }

                public void setLatestCompletionDay(int latestCompletionDay) {
                    this.latestCompletionDay = latestCompletionDay;
                }

                public String getOrderId() {
                    return orderId;
                }

                public void setOrderId(String orderId) {
                    this.orderId = orderId;
                }
            }

            public static class SpecificationVideoBean {
                /**
                 * buyNumber : 0
                 * id :
                 * latestCompletionDay : 0
                 * orderId :
                 * serviceSku : {"attrSymbolLabel":"","attrSymbolPath":"","id":"","price":0,"serviceId":""}
                 * serviceSkuId :
                 */

                private int buyNumber;
                private String id;
                private int latestCompletionDay;
                private String orderId;
                private ServiceSkuBean serviceSku;
                private String serviceSkuId;

                public int getBuyNumber() {
                    return buyNumber;
                }

                public void setBuyNumber(int buyNumber) {
                    this.buyNumber = buyNumber;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public int getLatestCompletionDay() {
                    return latestCompletionDay;
                }

                public void setLatestCompletionDay(int latestCompletionDay) {
                    this.latestCompletionDay = latestCompletionDay;
                }

                public String getOrderId() {
                    return orderId;
                }

                public void setOrderId(String orderId) {
                    this.orderId = orderId;
                }

                public ServiceSkuBean getServiceSku() {
                    return serviceSku;
                }

                public void setServiceSku(ServiceSkuBean serviceSku) {
                    this.serviceSku = serviceSku;
                }

                public String getServiceSkuId() {
                    return serviceSkuId;
                }

                public void setServiceSkuId(String serviceSkuId) {
                    this.serviceSkuId = serviceSkuId;
                }

                public static class ServiceSkuBean {
                    /**
                     * attrSymbolLabel :
                     * attrSymbolPath :
                     * id :
                     * price : 0
                     * serviceId :
                     */

                    private String attrSymbolLabel;
                    private String attrSymbolPath;
                    private String id;
                    private int price;
                    private String serviceId;

                    public String getAttrSymbolLabel() {
                        return attrSymbolLabel;
                    }

                    public void setAttrSymbolLabel(String attrSymbolLabel) {
                        this.attrSymbolLabel = attrSymbolLabel;
                    }

                    public String getAttrSymbolPath() {
                        return attrSymbolPath;
                    }

                    public void setAttrSymbolPath(String attrSymbolPath) {
                        this.attrSymbolPath = attrSymbolPath;
                    }

                    public String getId() {
                        return id;
                    }

                    public void setId(String id) {
                        this.id = id;
                    }

                    public int getPrice() {
                        return price;
                    }

                    public void setPrice(int price) {
                        this.price = price;
                    }

                    public String getServiceId() {
                        return serviceId;
                    }

                    public void setServiceId(String serviceId) {
                        this.serviceId = serviceId;
                    }
                }
            }
        }
    }
}
