package com.twan.kotlinbase.bean

import android.widget.ImageView
import java.io.Serializable

//公共字段
//`create_by` varchar(64) DEFAULT NULL,
//`create_time` datetime DEFAULT NULL,
//`update_by` varchar(64) DEFAULT NULL,
//`update_time` datetime DEFAULT NULL,
//声明你的bean
enum class EnumSingleRv(var id:Int,var desc:String){
    MY_FAV(1,"我的收藏"),//我的收藏
    MY_FOCUS(2,"我的关注"),
    MY_FOOTPRINT(3,"我的足迹"),
}



data class MyPic(
    var imgView:ImageView?=null
)

data class BannerBean(
    var enable:String="",//": "",
    var gotoUrl:String="",//": "",
    var id:String="",//": "",
    var imageUrl:String="",//": ""
)

data class SpiBean(
    var name: String
){
    override fun toString(): String {
        return name
    }
}

data class LoginBean(
    var userId:String,//":"1",
    var userName:String,//":"admin",
    var nickName:String,//":"管理员",
    var token:String,//":""
)

data class Dict(
    var id:Int,//: 94,
    var dictId:Int,//: 16,
    var label:String,//": "服装鞋包",
    var value:String,//": "1",
    var dictSort:String,//": 1,
){
    override fun toString(): String {
        return label
    }
}

//服务列表    直播和短视频制作和短视频推广,
data class ServiceBean(
    var totalAmount: String="",//自己加的,总价格
    var attestationStatus:String="",//": "",
    var evaluationContent:String="",//": "",
    var evaluationScore:String="",//": "",
    var id:String="",//": "",
    var imageUrl:String="",//": "",
    var nickName:String="",//": "",
    var place:String="",//": "",
    var price:String="",//": "",
    var studioName:String="",//": "",
    var title:String="",//": "",
    var totalSales:String="",//": ""
    var serviceTypeLabel: String="",
    var serviceTypeValue: String="",
    var userId: String="",
    var talentId: String=""
):Serializable
//模特演员的bean
data class ServiceActorModelBean(
    var age:Int,//": 0,
    var complexion:String,//": "",
    var country:String,//": "",
    var deleteFlag:String,//": "",
    var dieCardImages:String,//": "",
    var dieCardVideo:String,//": "",
    var height:String,//": "",
    var id:String,//": "",
    var mandarinLevel:String,//": "",
    var modelTypeLabel:String,//": "",
    var modelTypeValue:String,//": "",
    var price:String,//": "",
    var remarks:String,//": "",
    var score:Int,//": 0,
    var serviceTypeLabel:String,//": "",
    var serviceTypeValue:String,//": "",
    var sex:String,//": "",
    var title:String,//": "",
    var userId:String,//": "",
    var vitalStatistics:String,//": "",
    var weight:String,//": ""
)

//达人列表
data class SettleBean(
    var area:String,//": "",
    var avatarUlr:String,//": "",
    var evaluationContent:String,//": "",
    var goodAtLabel:String,//": "",
    var goodAtValue:String,//": "",
    var id:String,//": "",
    var nickName:String,//": "",
    var price:String,//": "",
    var salesAchievement:String,//": "",
    var scheduleDisplay:String,//": "",
    var studioName:String,//": "",
    var tagLabel:String,//": "",
    var tagValue:String,//": "",
    var userId:String,//": ""
    var talentId:String,//
)

data class ShareData(
    var title: String,
    var desc:String,
    var link:String,
    var data_url:String,
    var img_url:String,
    var type:String,
)