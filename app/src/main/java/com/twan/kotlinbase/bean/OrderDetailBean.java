package com.twan.kotlinbase.bean;

/**
 * @anthor zhoujr
 * @time 2021/4/19 11:09
 * @describe
 */
public class OrderDetailBean {

    /**
     * code : 0
     * message :
     * result : {"orderContentDTO":{"couponAmount":0,"createTime":"","orderAmount":"","orderCode":"","payTypeLabel":"","paymentAmount":0,"paymentTime":""},"orderHearderInfo":{"description":"","statusLabel":""},"orderServiceDTO":{"avatarUrl":"","createTime":"","imageUrl":"","nickName":"","price":0,"title":""}}
     * success : true
     * timestamp : 0
     */

    private int code;
    private String message;
    private ResultBean result;
    private boolean success;
    private int timestamp;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public static class ResultBean {
        /**
         * orderContentDTO : {"couponAmount":0,"createTime":"","orderAmount":"","orderCode":"","payTypeLabel":"","paymentAmount":0,"paymentTime":""}
         * orderHearderInfo : {"description":"","statusLabel":""}
         * orderServiceDTO : {"avatarUrl":"","createTime":"","imageUrl":"","nickName":"","price":0,"title":""}
         */

        private OrderContentDTOBean orderContentDTO;
        private OrderHearderInfoBean orderHearderInfo;
        private OrderServiceDTOBean orderServiceDTO;

        public OrderContentDTOBean getOrderContentDTO() {
            return orderContentDTO;
        }

        public void setOrderContentDTO(OrderContentDTOBean orderContentDTO) {
            this.orderContentDTO = orderContentDTO;
        }

        public OrderHearderInfoBean getOrderHearderInfo() {
            return orderHearderInfo;
        }

        public void setOrderHearderInfo(OrderHearderInfoBean orderHearderInfo) {
            this.orderHearderInfo = orderHearderInfo;
        }

        public OrderServiceDTOBean getOrderServiceDTO() {
            return orderServiceDTO;
        }

        public void setOrderServiceDTO(OrderServiceDTOBean orderServiceDTO) {
            this.orderServiceDTO = orderServiceDTO;
        }

        public static class OrderContentDTOBean {
            /**
             * couponAmount : 0
             * createTime :
             * orderAmount :
             * orderCode :
             * payTypeLabel :
             * paymentAmount : 0
             * paymentTime :
             */

            private int couponAmount;
            private String createTime;
            private String orderAmount;
            private String orderCode;
            private String payTypeLabel;
            private int paymentAmount;
            private String paymentTime;

            public int getCouponAmount() {
                return couponAmount;
            }

            public void setCouponAmount(int couponAmount) {
                this.couponAmount = couponAmount;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getOrderAmount() {
                return orderAmount;
            }

            public void setOrderAmount(String orderAmount) {
                this.orderAmount = orderAmount;
            }

            public String getOrderCode() {
                return orderCode;
            }

            public void setOrderCode(String orderCode) {
                this.orderCode = orderCode;
            }

            public String getPayTypeLabel() {
                return payTypeLabel;
            }

            public void setPayTypeLabel(String payTypeLabel) {
                this.payTypeLabel = payTypeLabel;
            }

            public int getPaymentAmount() {
                return paymentAmount;
            }

            public void setPaymentAmount(int paymentAmount) {
                this.paymentAmount = paymentAmount;
            }

            public String getPaymentTime() {
                return paymentTime;
            }

            public void setPaymentTime(String paymentTime) {
                this.paymentTime = paymentTime;
            }
        }

        public static class OrderHearderInfoBean {
            /**
             * description :
             * statusLabel :
             */

            private String description;
            private String statusLabel;

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getStatusLabel() {
                return statusLabel;
            }

            public void setStatusLabel(String statusLabel) {
                this.statusLabel = statusLabel;
            }
        }

        public static class OrderServiceDTOBean {
            /**
             * avatarUrl :
             * createTime :
             * imageUrl :
             * nickName :
             * price : 0
             * title :
             */

            private String avatarUrl;
            private String createTime;
            private String imageUrl;
            private String nickName;
            private String price;
            private String title;

            public String getAvatarUrl() {
                return avatarUrl;
            }

            public void setAvatarUrl(String avatarUrl) {
                this.avatarUrl = avatarUrl;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getImageUrl() {
                return imageUrl;
            }

            public void setImageUrl(String imageUrl) {
                this.imageUrl = imageUrl;
            }

            public String getNickName() {
                return nickName;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }
    }
}
