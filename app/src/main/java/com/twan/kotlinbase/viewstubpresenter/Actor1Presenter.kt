package com.twan.kotlinbase.viewstubpresenter

import android.os.Bundle
import android.view.View
import android.widget.*
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.skydoves.powerspinner.PowerSpinnerView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.databinding.Viewstub1ActorBinding
import com.twan.kotlinbase.databinding.Viewstub2ActorBinding
import com.twan.kotlinbase.event.TakePhotoEvent
import com.twan.kotlinbase.event.TichengEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.ui.SendActivity
import com.twan.kotlinbase.ui.TichengRateActivity
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.viewstub1_actor.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//1-需求,2-服务
//发需求:演员
class Actor1Presenter: BaseDataBindingFragment<Viewstub1ActorBinding>() {
    var mActorType:Dict?=null
    var mSex:Dict?=null
    var mSkin:Dict?=null
    var mShencai:Dict?=null
    var mLanuageLevel:Dict?=null
    override fun getLayoutId(): Int {
        return R.layout.viewstub1_actor
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        //spi_model_type
        SpiUtils.setSpi(spi_actor_type, DictUtils.getDictCache(DictUtils.actor_type)){ pos,item->
            mActorType=item
        }
        SpiUtils.setSpi(spi_sex, DictUtils.getDictCache(DictUtils.sex)){ pos,item->
            mSex=item
        }
        //spi_shencai
        SpiUtils.setSpi(spi_shencai, DictUtils.getDictCache(DictUtils.figure)){ pos,item->
            mShencai=item
        }
        //spi_lanuage_level
        SpiUtils.setSpi(spi_lanuage_level, DictUtils.getDictCache(DictUtils.Mandarin_level)){ pos,item->
            mLanuageLevel=item
        }
        //spi_skin
        SpiUtils.setSpi(spi_skin, DictUtils.getDictCache(DictUtils.color)){ pos,item->
            mSkin=item
        }

        btn_ok.setOnClickListener {
            if (!InputUtils.checkEmpty(edt_title,edt_height,edt_weight,edt_age,edt_cnt,edt_price)) return@setOnClickListener
            if (!chb_privacy.isChecked){
                ToastUtils.showShort("请勾选交易规则")
                return@setOnClickListener
            }

            RxHttpScope().launch {
                RxHttp.postJson("demand/actor/save")
                .add("actorTypeLabel",mActorType!!.label)
                .add("actorTypeValue",mActorType!!.id)
                .add("age",edt_age.text.toString())//: 0,
                .add("complexion",mSkin!!.id)
                .add("deleteFlag","0")
                .add("demandTypeLabel", Need.ACTOR.title)
                .add("demandTypeValue",Need.ACTOR.desc)
                .add("figureLabel","")
                .add("height",edt_height.text.toString())
                //.add("id","")
                .add("mandarinLevel",mLanuageLevel!!.id)
                .add("number",edt_cnt.text.toString())//: 0,
                .add("price",edt_price.text.toString())//: 0,
                .add("remarks",edt_mark.text.toString())
                .add("sex",mSex?.id)
                .add("status","")
                .add("title","发布演员需求-"+DateUtils.today2())
                .add("userId",MyUtils.getLoginInfo()!!.userId)
                .add("weight",edt_weight.text.toString())
                .toResponse<Any>().await()
                ToastUtils.showShort("发布演员需求成功")
                mActivity.finish()
            }
        }
    }



}