package com.twan.kotlinbase.viewstubpresenter

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.skydoves.powerspinner.PowerSpinnerView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.bean.ModelActor
import com.twan.kotlinbase.bean.Platform
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.Viewstub1ZhiboBinding
import com.twan.kotlinbase.databinding.Viewstub2VideoExtBinding
import com.twan.kotlinbase.event.TakePhotoEvent
import com.twan.kotlinbase.event.TichengEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.ui.SendActivity
import com.twan.kotlinbase.ui.TichengRateActivity
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.viewstub2_video_ext.*
import kotlinx.android.synthetic.main.viewstub2_video_ext.btn_ok
import kotlinx.android.synthetic.main.viewstub2_video_ext.chb_privacy
import kotlinx.android.synthetic.main.viewstub2_video_ext.edt_price
import kotlinx.android.synthetic.main.viewstub2_video_ext.rv_multi_pic
import kotlinx.android.synthetic.main.viewstub2_video_ext.rv_multi_video
import kotlinx.android.synthetic.main.viewstub2_video_ext.spi_caiyi
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//1-需求,2-服务
//发需求:短视频推广
class VideoExt2Presenter: BaseDataBindingFragment<Viewstub2VideoExtBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.viewstub2_video_ext
    }
    var mCaiyi:Dict?=null
    var mTakeStyle:Dict?=null
    var platformList= mutableListOf<Platform>()
    override fun initView(view: View?, savedInstanceState: Bundle?) {
        //添加平台
        tv_add.setOnClickListener { addPaltform() }
        initRvMultiPicOrVideo(rv_multi_pic,1)
        initRvMultiPicOrVideo(rv_multi_video,2)
        //才艺特长
        SpiUtils.setSpi(spi_caiyi, DictUtils.getDictCache(DictUtils.talent_expertise)){ pos,item->
            mCaiyi=item
        }
        //拍摄风格
        SpiUtils.setSpi(spi_take_style, DictUtils.getDictCache(DictUtils.shooting_style)){ pos,item->
            mTakeStyle=item
        }


        btn_ok.setOnClickListener {
            if (!InputUtils.checkEmpty(edt_price)) return@setOnClickListener
            if (uploadPics.size==1 && uploadVideos.size==1) {
                ToastUtils.showShort("请上传主图,二选一")
                return@setOnClickListener
            }
            if (!chb_privacy.isChecked){
                ToastUtils.showShort("请勾选交易规则")
                return@setOnClickListener
            }
            //检查新增的平台的url是否有空
            if (!checkUrls()){
                ToastUtils.showShort("请填写完整新增平台的信息")
                return@setOnClickListener
            }
            RxHttpScope().launch {
                RxHttp.postJson("service/shortVideoPromote/save")
                    //.add("id", "")
                    .add("deleteFlag", "0")
                    .add("imageUrl", getPicsUrl())
                    .add("price", edt_price.text.toString())
                    .add("recommendFlag", "0")
                    .add("recommendTime", "")
                    .add("score", "5")
                    .add("serviceTypeLabel", Need.SHORT_VIDEO_PROMOTE.title)
                    .add("serviceTypeValue", Need.SHORT_VIDEO_PROMOTE.desc)
                    .add("shootStyleLabel",mTakeStyle!!.label)
                    .add("shootStyleValue",mTakeStyle!!.id)
                    .add("talentSkillLabel",mCaiyi!!.label) //才艺特长
                    .add("talentSkillValue",mCaiyi!!.id) //才艺特长
                    .add("userId", MyUtils.getLoginInfo()!!.userId)
                    .add("title", "发布视频推广服务-"+DateUtils.today2())
                    .add("videoUrl", getVideosUrl())
                    .add("platforms",platformList)
                    .toResponse<Any>().await()
                ToastUtils.showShort("发布短视频推广服务成功")
                mActivity.finish()
            }
        }
    }

    //图片选择结果处理
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    //主图只能选择其一
    override fun uploadCallback(url:String){
        if (uploadType == 1){ //选择了图片就清空视频
            //clear(2)
            //rv_multi_video.adapter!!.notifyDataSetChanged()

            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==2){ //选择了视频就清空图片
//            clear(1)
            rv_multi_pic.adapter!!.notifyDataSetChanged()

            uploadVideos.add(UploadPic(filepath=url))
            rv_multi_video.adapter!!.notifyDataSetChanged()
        } else { //单个的图片或者视频

        }
    }


    var viewList= mutableListOf<View>()
    fun addPaltform(){
        var view = layoutInflater.inflate(R.layout.add_video_ext,ll_container,false)
        var iv_del = view.findViewById<ImageView>(R.id.iv_del)
        var tv_name = view.findViewById<TextView>(R.id.tv_name)
        var spi_platform1 = view.findViewById<PowerSpinnerView>(R.id.spi_platform1)
        //平台
        SpiUtils.setSpi(spi_platform1, DictUtils.getDictCache(DictUtils.short_video_platform)){ pos,item->
            spi_platform1.tag=item
        }
        viewList.add(view)
        tv_name.text="平台${viewList.size}"
        ll_container.addView(view)
        iv_del.setOnClickListener {
            viewList.remove(view)
            ll_container.removeView(view)
            updateName()
        }

    }

    fun updateName(){
        viewList.forEachIndexed { index, view ->
            var tv_name = view.findViewById<TextView>(R.id.tv_name)
            tv_name.text="平台${index+1}"
        }
    }

    //获取新增平台的图片,视频的url
    fun checkUrls():Boolean{
        platformList.clear()
        viewList.forEach {
            var tv_name = it.findViewById<TextView>(R.id.tv_name)
            var spi_platform1 = it.findViewById<PowerSpinnerView>(R.id.spi_platform1)
            var edt_fans1 = it.findViewById<EditText>(R.id.edt_fans1)
            var edt_account1 = it.findViewById<EditText>(R.id.edt_account1)
            var edt_price1 = it.findViewById<EditText>(R.id.edt_price1)
            var ischeck = InputUtils.checkEmpty(edt_fans1,edt_account1,edt_price1)
            if (!ischeck) return false

            var palt=spi_platform1.tag as Dict
            platformList.add(Platform(edt_price1.text.toString(),edt_fans1.text.toString(),"",edt_account1.text.toString(),
                    palt.label,palt.id.toString(),""))
        }
        return true
    }

}