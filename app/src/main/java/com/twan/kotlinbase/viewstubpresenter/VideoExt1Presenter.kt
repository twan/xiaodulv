package com.twan.kotlinbase.viewstubpresenter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import com.blankj.utilcode.util.FileUtils
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.blankj.utilcode.util.UriUtils
import com.lxj.xpopup.XPopup
import com.skydoves.powerspinner.PowerSpinnerView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.Viewstub1VideoExtBinding
import com.twan.kotlinbase.databinding.Viewstub2VideoExtBinding
import com.twan.kotlinbase.event.TakePhotoEvent
import com.twan.kotlinbase.event.TichengEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.ui.SendActivity
import com.twan.kotlinbase.ui.TichengRateActivity
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.viewstub1_video_ext.*
import kotlinx.android.synthetic.main.viewstub1_video_ext.btn_ok
import kotlinx.android.synthetic.main.viewstub1_video_ext.chb_privacy
import kotlinx.android.synthetic.main.viewstub1_video_ext.rv_multi_pic
import kotlinx.android.synthetic.main.viewstub1_video_ext.rv_multi_video
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//1-需求,2-服务
//发需求:短视频推广
class VideoExt1Presenter: BaseDataBindingFragment<Viewstub1VideoExtBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.viewstub1_video_ext
    }
    var mPlatform:Dict?=null
    override fun initView(view: View?, savedInstanceState: Bundle?) {
        initRvMultiPicOrVideo(rv_multi_pic,1)
        initRvMultiPicOrVideo(rv_multi_video,2)
        //平台
        SpiUtils.setSpi(spi_platform, DictUtils.getDictCache(DictUtils.short_video_platform)){ pos,item->
            mPlatform=item
        }

        btn_ok.setOnClickListener {
            if (!InputUtils.checkEmpty(edt_adv_name,edt_goods_price,edt_yongjin,edt_ext_url,edt_direction_url,edt_talent_cnt,edt_desc,edt_require,edt_cooper_amt)) return@setOnClickListener
            if (uploadPics.size==1 || uploadVideos.size==1) {
                ToastUtils.showShort("请上传主图,二选一")
                return@setOnClickListener
            }
            if (!chb_privacy.isChecked){
                ToastUtils.showShort("请勾选交易规则")
                return@setOnClickListener
            }

            RxHttpScope().launch {
                RxHttp.postJson("demand/popularize/save")
                .add("adName",edt_adv_name.text.toString())
                .add("commissionRate",edt_yongjin.text.toString())
                .add("commodityPrice",edt_goods_price.text.toString())//: 0,
                .add("cooperationPrice",edt_cooper_amt.text.toString())//: 0,
                .add("deleteFlag","0")
                .add("demandTypeLabel", Need.SHORT_VIDEO_PROMOTE.title)
                .add("demandTypeValue",Need.SHORT_VIDEO_PROMOTE.desc)
                .add("directionalUrl",edt_direction_url.text.toString())
                .add("expertNum",edt_talent_cnt.text.toString())//: 0,
                //.add("id","")
                .add("imageUrl",getPicsUrl())
                .add("platformId",mPlatform!!.id)
                .add("platformName",mPlatform!!.label)
                .add("popularizeUrl",edt_ext_url.text.toString())
                .add("productSellingPoint",edt_desc.text.toString())
                .add("status","")
                .add("title","发布视频推广需求-"+DateUtils.today2())
                .add("userId",MyUtils.getLoginInfo()!!.userId)
                .add("videoClaim",edt_require.text.toString())
                .add("videoUrl",getVideosUrl())
                .toResponse<Any>().await()
                ToastUtils.showShort("发布短视频推广成功")
                mActivity.finish()
            }
        }
    }

    //图片选择结果处理
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    //主图只能选择其一
    override fun uploadCallback(url:String){
        if (uploadType == 1){ //选择了图片就清空视频
            //clear(2)
            //rv_multi_video.adapter!!.notifyDataSetChanged()

            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==2){ //选择了视频就清空图片
            //clear(1)
            //rv_multi_pic.adapter!!.notifyDataSetChanged()

            uploadVideos.add(UploadPic(filepath=url))
            rv_multi_video.adapter!!.notifyDataSetChanged()
        } else { //单个的图片或者视频

        }
    }

}