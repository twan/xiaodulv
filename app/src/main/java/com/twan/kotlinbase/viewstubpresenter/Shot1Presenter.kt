package com.twan.kotlinbase.viewstubpresenter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewStub
import android.widget.*
import butterknife.ButterKnife
import butterknife.OnClick
import com.blankj.utilcode.util.FileUtils
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.blankj.utilcode.util.UriUtils
import com.lxj.xpopup.XPopup
import com.skydoves.powerspinner.PowerSpinnerView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.Viewstub1ShotBinding
import com.twan.kotlinbase.databinding.Viewstub2ShotBinding
import com.twan.kotlinbase.event.TakePhotoEvent
import com.twan.kotlinbase.event.TichengEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.ui.SendActivity
import com.twan.kotlinbase.ui.TichengRateActivity
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.viewstub1_shot.*
import kotlinx.android.synthetic.main.viewstub1_shot.btn_ok
import kotlinx.android.synthetic.main.viewstub1_shot.chb_privacy
import kotlinx.android.synthetic.main.viewstub1_shot.edt_price
import kotlinx.android.synthetic.main.viewstub1_shot.rb_local
import kotlinx.android.synthetic.main.viewstub1_shot.rv_multi_pic
import kotlinx.android.synthetic.main.viewstub1_shot.rv_multi_video
import kotlinx.android.synthetic.main.viewstub1_shot.spi_zhibo_time
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//1-需求,2-服务
//发需求:拍摄
class Shot1Presenter: BaseDataBindingFragment<Viewstub1ShotBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.viewstub1_shot
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        spi_zhibo_time.text= DateUtils.today()
        initRvMultiPicOrVideo(rv_multi_pic,1)
        initRvMultiPicOrVideo(rv_multi_video,2)
        //时间
        spi_zhibo_time.setOnClickListener {
            SelectDateUtils.initTimePicker(mActivity,spi_zhibo_time).show()
        }

        btn_ok.setOnClickListener {
            if (uploadVideos.size==1 || uploadPics.size==1) {
                ToastUtils.showShort("请上传主图,二选一")
                return@setOnClickListener
            }
            if (!InputUtils.checkEmpty(edt_addr,edt_price,edt_take_cnt)){
                return@setOnClickListener
            }
            if (!chb_privacy.isChecked){
                ToastUtils.showShort("请勾选交易规则")
                return@setOnClickListener
            }

            RxHttpScope().launch {
                RxHttp.postJson("demand/video/save")
                .add("actorProvideLabel",if (rb_actor_self.isChecked) rb_actor_self.text.toString() else rb_actor_fuwu.text.toString())
                .add("actorProvideValue",if (rb_actor_self.isChecked) 1 else 2)
                .add("deleteFlag","0")
                .add("demandTypeLabel",Need.SHORT_VIDEO_MAKE.title)
                .add("demandTypeValue",Need.SHORT_VIDEO_MAKE.desc)
                //.add("id","")
                .add("imageUrl",getPicsUrl())
                .add("perPrice",edt_price.text.toString())//0
                .add("placeAddress",edt_addr.text.toString())
                .add("placeTypeLabel",if (rb_local.isChecked) rb_local.text.toString() else rv_yidi.text.toString())
                .add("placeTypeValue",if (rb_local.isChecked) 1 else 2)
                .add("remark",edt_mark.text.toString())
                .add("shootDate","${spi_zhibo_time.text} 00:00:00")
                .add("siteTypeLabel",if (rb_room.isChecked) rb_room.text.toString() else rv_outdoor.text.toString())
                .add("siteTypeValue",if (rb_room.isChecked) 1 else 2)
                .add("status","")
                .add("title","发布视频拍摄需求-"+DateUtils.today2())
                .add("userId",MyUtils.getLoginInfo()!!.userId)
                .add("videoNum",edt_take_cnt.text.toString())//: 0,
                .add("videoUrl", getVideosUrl())
                .toResponse<Any>().await()
                ToastUtils.showShort("发布短视频拍摄成功")
                mActivity.finish()
            }
        }
    }


    //图片选择结果处理
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    //主图只能选择其一
    override fun uploadCallback(url:String){
        if (uploadType == 1){ //选择了图片就清空视频
            //clear(2)
            //rv_multi_video.adapter!!.notifyDataSetChanged()

            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==2){ //选择了视频就清空图片
            //clear(1)
            //rv_multi_pic.adapter!!.notifyDataSetChanged()

            uploadVideos.add(UploadPic(filepath=url))
            rv_multi_video.adapter!!.notifyDataSetChanged()
        } else { //单个的图片或者视频

        }
    }



}