package com.twan.kotlinbase.viewstubpresenter

import android.os.Bundle
import android.view.View
import android.widget.*
import com.blankj.utilcode.util.ToastUtils
import com.lxj.xpopup.XPopup
import com.skydoves.powerspinner.PowerSpinnerView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.databinding.Viewstub1ModelBinding
import com.twan.kotlinbase.databinding.Viewstub2ModelBinding
import com.twan.kotlinbase.event.TakePhotoEvent
import com.twan.kotlinbase.event.TichengEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.ui.SendActivity
import com.twan.kotlinbase.ui.TichengRateActivity
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.viewstub1_model.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//1-需求,2-服务
//发需求:模特
class Model1Presenter: BaseDataBindingFragment<Viewstub1ModelBinding>() {
    var mModleType:Dict?=null
    var mSex:Dict?=null
    var mSkin:Dict?=null
    var mCountry:Dict?=null
    var mLanuageLevel:Dict?=null
    override fun getLayoutId(): Int {
        return R.layout.viewstub1_model
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        //spi_model_type
        SpiUtils.setSpi(spi_model_type, DictUtils.getDictCache(DictUtils.model_type)){ pos,item->
            mModleType=item
        }
        SpiUtils.setSpi(spi_sex, DictUtils.getDictCache(DictUtils.sex)){ pos,item->
            mSex=item
        }
        //spi_country
        SpiUtils.setSpi(spi_country, DictUtils.getDictCache(DictUtils.nationality)){ pos,item->
            mCountry=item
        }
        //spi_lanuage_level
        SpiUtils.setSpi(spi_lanuage_level, DictUtils.getDictCache(DictUtils.Mandarin_level)){ pos,item->
            mLanuageLevel=item
        }
        //spi_skin
        SpiUtils.setSpi(spi_skin, DictUtils.getDictCache(DictUtils.color)){ pos,item->
            mSkin=item
        }

        btn_ok.setOnClickListener {
            if (!InputUtils.checkEmpty(edt_title,edt_height,edt_weight,edt_age,edt_sanwei,edt_cnt,edt_price)) return@setOnClickListener
            if (!chb_privacy.isChecked){
                ToastUtils.showShort("请勾选交易规则")
                return@setOnClickListener
            }

            RxHttpScope().launch {
                RxHttp.postJson("demand/model/save")
                    .add("age",edt_age.text.toString())
                    .add("complexion", mSkin!!.id)
                    .add("country", mCountry!!.id)
                    .add("delFlag", "0")
                    .add("demandTypeLabel", Need.MODEL.title)
                    .add("demandTypeValue", Need.MODEL.desc)
                    .add("height", edt_height.text.toString())
                    //.add("id", "")
                    .add("mandarinLevel", mLanuageLevel?.id)
                    .add("modelTypeLabel", mModleType!!.label)
                    .add("modelTypeValue", mModleType!!.id)
                    .add("number", edt_cnt.text.toString())
                    .add("price", edt_price.text.toString())
                    .add("remarks", edt_mark.text.toString())
                    .add("sex", mSex?.id)
                    .add("status", "")
                    .add("title", edt_title.text.toString())
                    .add("userId", MyUtils.getLoginInfo()!!.userId)
                    .add("vitalStatistics", edt_sanwei.text.toString())
                    .add("weight", edt_weight.text.toString())
                    .toResponse<Any>().await()
                ToastUtils.showShort("发布模特需求成功")
                mActivity.finish()
            }
        }
    }



}