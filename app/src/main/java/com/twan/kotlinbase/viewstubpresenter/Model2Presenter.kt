package com.twan.kotlinbase.viewstubpresenter

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lxj.xpopup.XPopup
import com.skydoves.powerspinner.PowerSpinnerView
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.bean.MyPic
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.Viewstub1ShotBinding
import com.twan.kotlinbase.databinding.Viewstub2ModelBinding
import com.twan.kotlinbase.event.TakePhotoEvent
import com.twan.kotlinbase.event.TichengEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.ui.SendActivity
import com.twan.kotlinbase.ui.TichengRateActivity
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.viewstub2_actor.*
import kotlinx.android.synthetic.main.viewstub2_model.*
import kotlinx.android.synthetic.main.viewstub2_model.btn_ok
import kotlinx.android.synthetic.main.viewstub2_model.chb_privacy
import kotlinx.android.synthetic.main.viewstub2_model.edt_age
import kotlinx.android.synthetic.main.viewstub2_model.edt_height
import kotlinx.android.synthetic.main.viewstub2_model.edt_mark
import kotlinx.android.synthetic.main.viewstub2_model.edt_price
import kotlinx.android.synthetic.main.viewstub2_model.edt_title
import kotlinx.android.synthetic.main.viewstub2_model.edt_weight
import kotlinx.android.synthetic.main.viewstub2_model.rv_multi_pic
import kotlinx.android.synthetic.main.viewstub2_model.rv_multi_video
import kotlinx.android.synthetic.main.viewstub2_model.spi_country
import kotlinx.android.synthetic.main.viewstub2_model.spi_lanuage_level
import kotlinx.android.synthetic.main.viewstub2_model.spi_sex
import kotlinx.android.synthetic.main.viewstub2_model.spi_skin
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//1-需求,2-服务
//发需求:模特
class Model2Presenter: BaseDataBindingFragment<Viewstub2ModelBinding>() {
    var mModleType:Dict?=null
    var mSex:Dict?=null
    var mSkin:Dict?=null
    var mCountry:Dict?=null
    var mLanuageLevel:Dict?=null

    override fun getLayoutId(): Int {
        return R.layout.viewstub2_model
    }

    override fun initView(view: View?, savedInstanceState: Bundle?) {
        initRvMultiPicOrVideo(rv_multi_pic,1)
        initRvMultiPicOrVideo(rv_multi_video,2)
        //spi_model_type
        SpiUtils.setSpi(spi_model_type, DictUtils.getDictCache(DictUtils.model_type)){ pos,item->
            mModleType=item
        }
        SpiUtils.setSpi(spi_sex, DictUtils.getDictCache(DictUtils.sex)){ pos,item->
            mSex=item
        }
        //spi_skin
        SpiUtils.setSpi(spi_country, DictUtils.getDictCache(DictUtils.nationality)){ pos,item->
            mCountry=item
        }
        //spi_lanuage_level
        SpiUtils.setSpi(spi_lanuage_level, DictUtils.getDictCache(DictUtils.Mandarin_level)){ pos,item->
            mLanuageLevel=item
        }
        //spi_skin
        SpiUtils.setSpi(spi_skin, DictUtils.getDictCache(DictUtils.color)){ pos,item->
            mSkin=item
        }

        btn_ok.setOnClickListener {
            var pics= getPicsUrl()
            if (pics.split(",").size<5) {
                ToastUtils.showShort("请上传主图,至少5张")
                return@setOnClickListener
            }
            if (uploadVideos.size==0) {
                ToastUtils.showShort("请上传模卡视频")
                return@setOnClickListener
            }
            if (!InputUtils.checkEmpty(edt_title,edt_height,edt_weight,edt_age,edt_sanwei,edt_price)) return@setOnClickListener
            if (!chb_privacy.isChecked){
                ToastUtils.showShort("请勾选交易规则")
                return@setOnClickListener
            }

            RxHttpScope().launch {
                RxHttp.postJson("service/model/save")
                    .add("age",edt_age.text.toString())
                    .add("complexion", mSkin!!.label)
                    .add("country", mCountry?.label)
                    .add("deleteFlag", "0")
                    .add("dieCardImages", pics)
                    .add("dieCardVideo", getVideosUrl())
                    .add("height", edt_height.text.toString())
                    //.add("id", "")
                    .add("mandarinLevel", mLanuageLevel?.label)
                    .add("modelTypeLabel", mModleType?.label)
                    .add("modelTypeValue", mModleType?.id)
                    .add("price", edt_price.text.toString())
                    .add("remarks", edt_mark.text.toString())
                    .add("score", "5")
                    .add("serviceTypeLabel", Need.MODEL.title)
                    .add("serviceTypeValue", Need.MODEL.desc)
                    .add("sex", mSex?.label)
                    .add("title", edt_title.text.toString())
                    .add("userId", MyUtils.getLoginInfo()?.userId)
                    .add("vitalStatistics", edt_sanwei.text.toString())
                    .add("weight", edt_weight.text.toString())
                    .toResponse<Any>().await()
                ToastUtils.showShort("发布模特服务成功")
                mActivity.finish()
            }
        }
    }

    override fun uploadCallback(url:String){
        if (uploadType == 1){
            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==2){
            uploadVideos.add(UploadPic(filepath=url))
            rv_multi_video.adapter!!.notifyDataSetChanged()
        } else { //单个的图片或者视频

        }
    }
}