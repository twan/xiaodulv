package com.twan.kotlinbase.viewstubpresenter

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.FileUtils
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lxj.xpopup.XPopup
import com.twan.kotlinbase.R
import com.twan.kotlinbase.app.BaseDataBindingFragment
import com.twan.kotlinbase.app.Need
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.bean.UploadPic
import com.twan.kotlinbase.databinding.Viewstub1ZhiboBinding
import com.twan.kotlinbase.event.TichengEvent
import com.twan.kotlinbase.network.RxHttpScope
import com.twan.kotlinbase.pop.AddPicPopup
import com.twan.kotlinbase.ui.TichengRateActivity
import com.twan.kotlinbase.util.*
import com.twan.kotlinbase.widgets.router.Router
import com.zhihu.matisse.MimeType
import kotlinx.android.synthetic.main.viewstub1_zhibo.*
import kotlinx.android.synthetic.main.viewstub1_zhibo.btn_ok
import kotlinx.android.synthetic.main.viewstub1_zhibo.chb_privacy
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

//1-需求,2-服务
//发需求:直播
class Zhibo1Presenter:BaseDataBindingFragment<Viewstub1ZhiboBinding>() {
    override fun getLayoutId(): Int {
        return R.layout.viewstub1_zhibo
    }

    private lateinit var mCategory:Dict
    override fun initView(view: View?, savedInstanceState: Bundle?) {
        initRvMultiPicOrVideo(rv_multi_pic,1)
        initRvMultiPicOrVideo(rv_multi_video,2)
        spi_zhibo_time.text=DateUtils.today()
        //时间
        spi_zhibo_time.setOnClickListener {
            SelectDateUtils.initTimePicker(mActivity,spi_zhibo_time).show()
        }
        //类目
        SpiUtils.setSpi(spi_select, DictUtils.getDictCache(DictUtils.category)){pos,dict->
            mCategory=dict
        }
        //提成
        tv_ticheng.setOnClickListener {
            Router.newIntent(mActivity).to(TichengRateActivity::class.java).launch()
        }

        btn_ok.setOnClickListener {
            if (uploadPics.size==1 || uploadVideos.size==1) {
                ToastUtils.showShort("请上传主图,二选一")
                return@setOnClickListener
            }
            if (!InputUtils.checkEmpty(edt_shop_url,edt_price)){
                return@setOnClickListener
            }
            if (!InputUtils.checkEmpty(tv_ticheng)){
                return@setOnClickListener
            }
            if (!chb_privacy.isChecked){
                ToastUtils.showShort("请勾选交易规则")
                return@setOnClickListener
            }

            RxHttpScope().launch {
                RxHttp.postJson("demand/live/save")
                .add("categoryLabel",mCategory.label)
                .add("categoryValue",mCategory.id)
                .add("commissionRate",tv_ticheng.text.toString())
                .add("deleteFlag","0")
                .add("demandTypeLabel",Need.SHOP_LIVE.title)
                .add("demandTypeValue",Need.SHOP_LIVE.desc)
                .add("hourlyRate",edt_price.text.toString())
                //.add("id","")
                .add("imageUrl",getPicsUrl())
                .add("liveClaim",edt_zhibo_require.text.toString())
                .add("liveDate","${spi_zhibo_time.text} 00:00:00")
                .add("shopUrl",edt_shop_url.text.toString())
                .add("status","")
                .add("title","发布直播需求-"+DateUtils.today2())
                .add("userId",MyUtils.getLoginInfo()!!.userId)
                .add("videoUrl",getVideosUrl())
                .toResponse<Any>().await()
                ToastUtils.showShort("发布成功")
                mActivity.finish()
            }
        }
    }

    //提成选择
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun dataEvent(event: TichengEvent) {
        tv_ticheng.setTextColor(resources.getColor(R.color.text_black))
        tv_ticheng.text=event.rate
    }


    //图片选择结果处理
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    //主图只能选择其一
    override fun uploadCallback(url:String){
        if (uploadType == 1){ //选择了图片就清空视频
            //clear(2)
            //rv_multi_video.adapter!!.notifyDataSetChanged()

            uploadPics.add(UploadPic(filepath=url))
            rv_multi_pic.adapter!!.notifyDataSetChanged()
        } else if (uploadType ==2){ //选择了视频就清空图片
            //clear(1)
            //rv_multi_pic.adapter!!.notifyDataSetChanged()

            uploadVideos.add(UploadPic(filepath=url))
            rv_multi_video.adapter!!.notifyDataSetChanged()
        } else { //单个的图片或者视频

        }
    }


}