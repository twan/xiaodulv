package com.twan.kotlinbase.util

import com.blankj.utilcode.constant.TimeConstants
import com.blankj.utilcode.util.TimeUtils
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    /**
     * date:2020-11-01
     * cnt:增加或者减少的天数, 如:5或者-5
     * 返回如:2020-11-01
     */
    fun add(date: String, cnt: Long):String{
        var start = TimeUtils.string2Date(date, SimpleDateFormat("yyyy-MM-dd"))
        return TimeUtils.date2String(TimeUtils.getDate(start, cnt, TimeConstants.DAY), SimpleDateFormat("yyyy-MM-dd"))
    }

    fun getMonth():Int{
        return Calendar.getInstance().get(Calendar.MONTH)+1
    }

    fun getDay():Int{
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
    }

    /**
     * date:2020-11-01
     * 把日期转为Calendar
     */
    fun convet(date: String):Calendar{
        var date = TimeUtils.string2Date(date, SimpleDateFormat("yyyy-MM-dd"))
        var calstart = Calendar.getInstance()
        calstart.time=date
        return calstart
    }

    fun convetDate(date: String):Date{
        var date = TimeUtils.string2Date(date, SimpleDateFormat("yyyy-MM-dd"))
        return date
    }

    fun convetDate2(date: String):Date{
        var date = TimeUtils.string2Date(date, SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
        return date
    }

    /**
     * 返回今日: 2020-02-01
     */
    fun today():String{
        return TimeUtils.getNowString(SimpleDateFormat("yyyy-MM-dd"))
    }

    fun today2():String{
        return TimeUtils.getNowString(SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
    }

    fun today3():String{
        return TimeUtils.getNowString(SimpleDateFormat("yyyy-MM"))
    }

}