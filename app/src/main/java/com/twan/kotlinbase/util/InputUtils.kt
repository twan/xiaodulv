package com.twan.kotlinbase.util

import android.view.View
import android.widget.EditText
import android.widget.TextView

data class NeedCheck(val view:View,val maxLength:Int?=0,val reg:String?="")

object InputUtils {
    //只检测EditText是否为空
    fun checkEmpty(vararg edts: EditText):Boolean{
        edts.forEach {
            if (it.text.toString().isNullOrEmpty()){
                it.requestFocus()
                it.error = it.hint
                return false
            }
        }
        return true
    }

    fun checkEmpty(vararg edts: TextView):Boolean{
        edts.forEach {
            if (it.text.toString().isNullOrEmpty()){
                it.requestFocus()
                it.error = it.hint
                return false
            }
        }
        return true
    }

}