package com.twan.kotlinbase.util;

import android.content.Context;
import android.view.WindowManager;

/**
 * @anthor zhoujr
 * @time 2021/4/28 13:33
 * @describe
 */
public class WindowWH {
    public static int getWindowWidth(Context context){
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        return windowManager.getDefaultDisplay().getWidth();
    }
    public static int getWindowHeight(Context context){
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        return windowManager.getDefaultDisplay().getHeight();
    }
}
