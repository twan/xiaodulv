package com.twan.kotlinbase.util

import com.twan.kotlinbase.bean.*
import java.io.Serializable

object CacheUtils {
    //var selectPicCache = MyCache<List<PicBean>>()//选择的图片
}

class MyCache<T> : Serializable {
    private var mValue: T? = null
    constructor(value: T) {
        mValue = value
    }

    constructor() {

    }

    fun get(): T? {
        return mValue
    }

    fun set(value: T) {
        if (value !== mValue) {
            mValue = value
        }
    }

    fun clear(){
        mValue=null
    }
}