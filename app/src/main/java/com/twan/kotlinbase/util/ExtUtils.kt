package com.twan.kotlinbase.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.ImageView
import java.text.DecimalFormat


fun String?.toSafeFloat():Float{
    return if (this.isNullOrEmpty()) 0f
    else this.toFloat()
}

fun String?.toMoneyStr():String{
    return if (this.isNullOrEmpty()) "0"
    else this
}

fun String?.firstImgUrl():String{
    return if (this.isNullOrEmpty()) ""
    else {
        var urls = this.split(",")
        urls.forEach {
            if (!it.isEmpty())
                return it
        }
        ""
    }
}

fun ImageView.getUrl():String{
    if (this.tag == null) return ""
    else return this.tag as String
}

//保留两位,四舍五入
fun Float.toMoney():String{
    val format = DecimalFormat("###0.00")//0.##
    //format.roundingMode = RoundingMode.CEILING
    return format.format(this)
}

//保留两位,四舍五入
fun Float.toFen():String{
    val format = DecimalFormat("###0")//0.##
    //format.roundingMode = RoundingMode.CEILING
    return format.format(this)
}

//默认实现的TextWatcher
interface SimpleTextWatcher: TextWatcher {
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun afterTextChanged(p0: Editable?) {

    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }
}