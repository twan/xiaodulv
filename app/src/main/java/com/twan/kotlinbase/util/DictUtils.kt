package com.twan.kotlinbase.util

import com.blankj.utilcode.util.SPUtils
import com.twan.kotlinbase.bean.Dict
import com.twan.kotlinbase.network.RxHttpScope
import rxhttp.wrapper.param.RxHttp
import rxhttp.wrapper.param.toResponse

object DictUtils {

    var dicts= mutableListOf<String>()
    val user_status="user_status"//	用户状态
    val dept_status="dept_status"//	部门状态
    val job_status="job_status"//	岗位状态
    val register_type="register_type"//	注册标签
    val short_video_type="short_video_type"//	短视频类型
    val shooting_mode="shooting_mode"//	拍摄模式
    val time_length="time_length"//	时长
    val need_actor="need_actor"//	是否需要演员
    val short_video_platform="short_video_platform"//	短视频平台
    val talent_expertise="talent_expertise"//	才艺特长
    val shooting_style="shooting_style"//	拍摄风格
    val live_tag="live_tag"//	直播标签
    val sex="sex"//	性别
    val category="category"//	类目
    val talent_type="talent_type"//	达人类型
    val talented_person="talented_person"//	才艺达人
    val actor_type="actor_type"//	演员类型
    val model_type="model_type"//	模特类型
    val figure="figure"//	身材
    val Mandarin_level="Mandarin_level"//	普通话程度
    val color="color"//	肤色
    val nationality="nationality"//	国籍
    val shop_live_time_length="shop_live_time_length"//店铺直播时长
    val short_video_promote="short_video_promote"//最晚完成时长

    fun loadDict(){
        dicts.add(user_status)//	用户状态
        dicts.add(dept_status)//	部门状态
        dicts.add(job_status)//	岗位状态
        dicts.add(register_type)//	注册标签
        dicts.add(short_video_type)//	短视频类型
        dicts.add(shooting_mode)//	拍摄模式
        dicts.add(time_length)//	时长
        dicts.add(need_actor)//	是否需要演员
        dicts.add(short_video_platform)//	短视频平台
        dicts.add(talent_expertise)//	才艺特长
        dicts.add(shooting_style)//	拍摄风格
        dicts.add(live_tag)//	直播标签
        dicts.add(sex)//	性别
        dicts.add(category)//	类目
        dicts.add(talent_type)//	达人类型
        dicts.add(talented_person)//	才艺达人
        dicts.add(actor_type)//	演员类型
        dicts.add(model_type)//	模特类型
        dicts.add(figure)//	身材
        dicts.add(Mandarin_level)//	普通话程度
        dicts.add(color)//	肤色
        dicts.add(nationality)//	国籍
        dicts.add(shop_live_time_length)//	店铺直播时长
        dicts.add(short_video_promote)//	最晚完成时长


        RxHttpScope().launch {
            //短视频类型
            dicts.forEach {
                var req= RxHttp.get("/dict/findAllByCode").add("dictCode",it).toResponse<List<Dict>>().await()
                SPUtils.getInstance().put(it,JsonUtil.objectToJson(req))
            }
        }
    }

    fun getDictCache(key:String):List<Dict>? {
        var json = SPUtils.getInstance().getString(key)
        return JsonUtil.stringToArray(json,Array<Dict>::class.java)
    }
}