package com.twan.kotlinbase.util

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.bigkoo.pickerview.builder.TimePickerBuilder
import com.bigkoo.pickerview.view.TimePickerView
import com.blankj.utilcode.util.TimeUtils
import java.text.SimpleDateFormat
import java.util.*

object SelectDateUtils {
    enum class DateMode(){
       YMD,YM,HM
    }
    var selectYMD = booleanArrayOf(true, true, true, false, false, false)
    var selectYM = booleanArrayOf(true, true, false, false, false, false)
    var selectHM = booleanArrayOf(false, false, false, true, true, false)

    fun initTimePicker(context: Context,
                       dateView: TextView,
                       startDate:Calendar?= Calendar.getInstance(),
                       endDate:Calendar?= null,
                       mode:DateMode=DateMode.YMD,
                       onSelect:((selectDate:String)->Unit)?=null): TimePickerView {
        //可选的最大实际
        var realEndDate = endDate
        if (endDate == null) {
            var maxCalendar = Calendar.getInstance()
            maxCalendar.time = TimeUtils.string2Date("2099-12-31", SimpleDateFormat("yyyy-MM-dd"))
            realEndDate = maxCalendar
        }
        //选择日期还是时分
        var pvTime = TimePickerBuilder(context) { date, _ ->
            val selectDate = when (mode) {
                DateMode.YMD -> {
                    SimpleDateFormat("yyyy-MM-dd").format(date)
                }
                DateMode.YM -> {
                    SimpleDateFormat("yyyy-MM").format(date)
                }
                else -> {
                    SimpleDateFormat("HH:mm").format(date)
                }
            }
            dateView.text = selectDate
            onSelect?.run { this(selectDate) }
        }.setTimeSelectChangeListener { }
            .setType(if (mode==DateMode.YMD) {
                selectYMD
            } else if (mode==DateMode.YM) {
                selectYM
            } else{
                selectHM
            })
            .isDialog(true) //默认设置false ，内部实现将DecorView 作为它的父控件。
            .addOnCancelClickListener { }
            .setItemVisibleCount(7) //若设置偶数，实际值会加1（比如设置6，则最大可见条目为7）
            .setLineSpacingMultiplier(2.0f)
            .setRangDate(startDate, realEndDate)
            .isAlphaGradient(true)
            .setCancelText(" ") //取消按钮文字
            .setSubmitText("确定") //确认按钮文字
            //.setContentSize(18)//滚轮文字大小
            .setTitleSize(18) //标题文字大小
            .setTitleText("请选择日期") //标题文字
            .setOutSideCancelable(true) //点击屏幕，点在控件外部范围时，是否取消显示
            .isCyclic(true) //是否循环滚动
            .setTitleColor(Color.parseColor("#343434")) //标题文字颜色
            .setSubmitColor(Color.parseColor("#007EFF")) //确定按钮文字颜色
            .setTitleBgColor(Color.WHITE) //标题背景颜色 Night mode
            .setDate(Calendar.getInstance()) // 如果不设置的话，默认是系统时间*/
            .setLabel("年", "月", "日", "时", "分", "秒") //默认设置为年月日时分秒
            .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
            .setTextColorCenter(Color.parseColor("#007EFF"))
            .setDividerColor(Color.TRANSPARENT)
            .build()

        val mDialog = pvTime.dialog
        if (mDialog != null) {
            val params = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                Gravity.BOTTOM
            )
            params.leftMargin = 0
            params.rightMargin = 0
            pvTime.dialogContainerLayout.layoutParams = params
            val dialogWindow = mDialog.window
            if (dialogWindow != null) {
                dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim) //修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM)
                dialogWindow.setDimAmount(0.3f)
            }
        }
        return pvTime
    }
}