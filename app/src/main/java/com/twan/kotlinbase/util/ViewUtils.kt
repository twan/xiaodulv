package com.twan.kotlinbase.util

import android.view.ViewGroup
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.pop_select.view.*

object ViewUtils {

    fun setLLheight(ll:LinearLayout){
        var lp = ll.layoutParams
        lp.width= ViewGroup.LayoutParams.MATCH_PARENT;
        lp.height=350
        ll.layoutParams = lp
    }
}