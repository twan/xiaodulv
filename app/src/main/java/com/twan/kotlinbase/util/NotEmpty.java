package com.twan.kotlinbase.util;

/**
 * Created by paopao on 2020/3/28.
 */

public class NotEmpty {

    public static boolean isempty(String content, String msg){
        if(content.isEmpty()){
            UIUtils.showToast(msg);
            return true;
        }else{
            return  false;
        }
    }
}
