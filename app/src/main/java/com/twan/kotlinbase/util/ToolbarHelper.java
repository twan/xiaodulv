package com.twan.kotlinbase.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.appcompat.widget.Toolbar;

/**
 * @anthor zhoujr
 * @time 2020/12/29 13:36
 * @describe
 */
public class ToolbarHelper {
    public static void addMiddleTitle(Context context, CharSequence title, @ColorInt int textColor, Toolbar toolbar) {
        TextView textView = toolbar.findViewWithTag("title");
        if (textView == null) {
            textView = new TextView(context);
            textView.setTag("title");
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            textView.setMaxLines(1);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            toolbar.addView(textView, params);
        }
        textView.setTextColor(textColor);
        textView.setText(title);
    }
}
