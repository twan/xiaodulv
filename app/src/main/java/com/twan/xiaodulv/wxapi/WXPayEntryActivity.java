package com.twan.xiaodulv.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.twan.kotlinbase.R;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
	private static final String TAG = "WXPayEntryActivity";
	
    private IWXAPI api;
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
        api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {

	}

	@Override
	public void onResp(BaseResp resp) {
		Log.d(TAG, "onPayFinish, errCode = " + resp.errCode);

		if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
			if (resp.errCode==0){
				ToastUtils.showShort("微信支付成功");
				//Router.Companion.newIntent(WXPayEntryActivity.this).to(PaySuccessActivity.class).launch();
			}else if (resp.errCode==-2){
				ToastUtils.showShort("取消支付");
			}else {
				ToastUtils.showShort("微信支付失败, 错误码:"+resp.errCode+",描述:"+resp.errStr);
			}
			finish();
		}
	}

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wechat_pay_result);
		api = WXAPIFactory.createWXAPI(this, WecahtPayUtils.APP_ID);
		api.handleIntent(getIntent(), this);
	}

}