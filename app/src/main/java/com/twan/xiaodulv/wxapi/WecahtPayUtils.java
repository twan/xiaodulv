package com.twan.xiaodulv.wxapi;

import android.content.Context;

import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

public class WecahtPayUtils {
    public static final String APP_ID ="wx67c3b894c7942d9f";
    public static void wxpay(Context context, WechatBean wxPayRsp) {
        IWXAPI api= WXAPIFactory.createWXAPI(context, APP_ID);
        api.registerApp(APP_ID);
        PayReq payReq=new PayReq();
        payReq.appId=APP_ID;
        payReq.partnerId=wxPayRsp.partnerid;
        payReq.prepayId=wxPayRsp.prepayid;
        payReq.packageValue="Sign=WXPay";
        payReq.nonceStr=wxPayRsp.noncestr;
        payReq.timeStamp=wxPayRsp.timestamp;
        payReq.sign=wxPayRsp.sign;
        api.sendReq(payReq);
    }

    public static class WechatBean {
        public String appid;//": "wx26c88e0e2830c3f7",
        public String partnerid;//": "1521036471",
        public String prepayid;//": "wx21104537754499cae11915102efd540000",
        public String noncestr;//": "84117275BE999FF55A987B9381E01F96",
        public String timestamp;//": "1603248351",
        public String sign;//": "A6FAD954D1DCB513C2B5EE655FABA0DF"
    }
}

